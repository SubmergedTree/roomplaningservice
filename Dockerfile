FROM openjdk:12

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

ENV RP_SVC_API_KEY testKey
ENV RP_SVC_PORT 8080

ENTRYPOINT ["java","-jar","/app.jar"]
