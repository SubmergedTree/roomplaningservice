/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.configuration;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.util.validation.ValidationService;
import com.auasagtderfisch.roomplanningsvc.wall.WallService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;

@Profile("test")
@Configuration
public class SpringTestConfig {

    @Bean
    @Primary
    public WallService wallService() {
        return Mockito.mock(WallService.class);
    }

    @Bean
    @Primary
    public ValidationService validationService() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return new ValidationService(factory.getValidator());
    }

    @Bean
    @Primary
    public BlackboardService blackboardService() {
        return Mockito.mock(BlackboardService.class);
    }
}
