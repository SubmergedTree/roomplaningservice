/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d;

import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.CoordinateXY;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class JtsCAD2dObjectTest {

    @Test
    @DisplayName("create JtsCAD2dObject without specifying position and rotation")
    void createSimpleTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        Point pos = obj1.getPosition();

        assertEquals(0, pos.getX(), 0.01);
        assertEquals(0, pos.getY(), 0.01);
        assertEquals(0, obj1.getRotation());

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(-5, -5).isEqualTo(points[0]));
        assertTrue(new Point(5, -5).isEqualTo(points[1]));
        assertTrue(new Point(5, 5).isEqualTo(points[2]));
        assertTrue(new Point(-5, 5).isEqualTo(points[3]));
        assertTrue(new Point(-5, -5).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("local centroid is global position")
    void createWithLocalCentroidAsGlobalPosition() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
        Point isCentroid = obj1.getPosition();

        assertEquals(5, isCentroid.getX(), 0.01);
        assertEquals(5, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(0, 0).isEqualTo(points[0]));
        assertTrue(new Point(10, 0).isEqualTo(points[1]));
        assertTrue(new Point(10, 10).isEqualTo(points[2]));
        assertTrue(new Point(0, 10).isEqualTo(points[3]));
        assertTrue(new Point(0, 0).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("create JtsCAD2dObject with specifying position")
    void createWithPositionTest() {
        Point shouldCentroid = new Point(30, 25);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        Point isCentroid = obj1.getPosition();
        assertEquals(30, isCentroid.getX(), 0.01);
        assertEquals(25, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(25, 20).isEqualTo(points[0]));
        assertTrue(new Point(35, 20).isEqualTo(points[1]));
        assertTrue(new Point(35, 30).isEqualTo(points[2]));
        assertTrue(new Point(25, 30).isEqualTo(points[3]));
        assertTrue(new Point(25, 20).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("create JtsCAD2dObject with specifying position and angle")
    void createWithPositionAndRotationTest() {
        Point shouldCentroid = new Point(5, 5);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 42);
        Point isCentroid = obj1.getPosition();
        assertEquals(5, isCentroid.getX(), 0.01);
        assertEquals(5, isCentroid.getY(), 0.01);
        assertEquals(42, obj1.getRotation(), 0.01);
        /*Point[] points = obj1.getVertices();
        assertTrue(new Point(4.62, -2.06).isEqualTo(points[0]));
        assertTrue(new Point(12.06, 4.63).isEqualTo(points[1]));
        assertTrue(new Point(5.37, 12.06).isEqualTo(points[2]));
        assertTrue(new Point(-2.06, 5.37).isEqualTo(points[3]));
        assertTrue(new Point(4.63, -2.06).isEqualTo(points[4]));*/
    }

    @Test
    @DisplayName("set position from (30, 25) to (0,0)")
    void setPositionToZeroTest() {
        Point shouldCentroid = new Point(30, 25);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        obj1.setPosition(new Point(0, 0));

        Point isCentroid = obj1.getPosition();
        assertEquals(0, isCentroid.getX(), 0.01);
        assertEquals(0, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(-5, -5).isEqualTo(points[0]));
        assertTrue(new Point(5, -5).isEqualTo(points[1]));
        assertTrue(new Point(5, 5).isEqualTo(points[2]));
        assertTrue(new Point(-5, 5).isEqualTo(points[3]));
        assertTrue(new Point(-5, -5).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("set position from (30, 25) to (180,-45)")
    void setPositionToTest() {
        Point shouldCentroid = new Point(30, 25);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        Point isCentroid = obj1.getPosition();
        assertEquals(30, isCentroid.getX(), 0.01);
        assertEquals(25, isCentroid.getY(), 0.01);

        obj1.setPosition(new Point(180, -45));

        isCentroid = obj1.getPosition();
        assertEquals(180, isCentroid.getX(), 0.01);
        assertEquals(-45, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        // TODO
      /*  Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(-5, -5).isEqualTo(points[0]));
        assertTrue(new Point(5, -5).isEqualTo(points[1]));
        assertTrue(new Point(5, 5).isEqualTo(points[2]));
        assertTrue(new Point(-5, 5).isEqualTo(points[3]));
        assertTrue(new Point(-5, -5).isEqualTo(points[4]));*/
    }

    @Test
    @DisplayName("set position from (0, 0) to (180,-45)")
    void setPositionToTest2() {
        Point shouldCentroid = new Point(0, 0);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, -63);
        obj1.setPosition(new Point(180, -45));

        Point isCentroid = obj1.getPosition();
        assertEquals(180, isCentroid.getX(), 0.01);
        assertEquals(-45, isCentroid.getY(), 0.01);
        assertEquals(-63, obj1.getRotation(), 0.01);

        // TODO
      /*  Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(-5, -5).isEqualTo(points[0]));
        assertTrue(new Point(5, -5).isEqualTo(points[1]));
        assertTrue(new Point(5, 5).isEqualTo(points[2]));
        assertTrue(new Point(-5, 5).isEqualTo(points[3]));
        assertTrue(new Point(-5, -5).isEqualTo(points[4]));*/
    }

    @Test
    @DisplayName("move (0,0)")
    void moveZeroTest() {
        Point shouldCentroid = new Point(10, -10);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        obj1.move(new Point(0, 0));
        Point isCentroid = obj1.getPosition();
        assertEquals(10, isCentroid.getX(), 0.01);
        assertEquals(-10, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(5, -15).isEqualTo(points[0]));
        assertTrue(new Point(15, -15).isEqualTo(points[1]));
        assertTrue(new Point(15, -5).isEqualTo(points[2]));
        assertTrue(new Point(5, -5).isEqualTo(points[3]));
        assertTrue(new Point(5, -15).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("move (80, 60)")
    void moveTest() {
        Point shouldCentroid = new Point(10, -10);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        obj1.move(new Point(80, 60));
        Point isCentroid = obj1.getPosition();
        assertEquals(90, isCentroid.getX(), 0.01);
        assertEquals(50, isCentroid.getY(), 0.01);
        assertEquals(0, obj1.getRotation(), 0.01);

        Point[] points = obj1.getVertices();
        assertEquals(5, points.length);
        assertTrue(new Point(85, 45).isEqualTo(points[0]));
        assertTrue(new Point(95, 45).isEqualTo(points[1]));
        assertTrue(new Point(95, 55).isEqualTo(points[2]));
        assertTrue(new Point(85, 55).isEqualTo(points[3]));
        assertTrue(new Point(85, 45).isEqualTo(points[4]));
    }

    @Test
    @DisplayName("rotate 0 degree")
    void rotateZeroTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        assertEquals(0, obj1.getRotation(), 0.01);
        obj1.rotate(0);
        assertEquals(0, obj1.getRotation(), 0.01);
        Point isCentroid = obj1.getPosition();
        assertEquals(0, isCentroid.getX(), 0.01);
        assertEquals(0, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("rotate 42 degree")
    void rotate42Test() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        assertEquals(0, obj1.getRotation(), 0.01);
        obj1.rotate(42);
        assertEquals(42, obj1.getRotation(), 0.01);
        Point isCentroid = obj1.getPosition();
        assertEquals(0, isCentroid.getX(), 0.01);
        assertEquals(0, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("rotate 370 degree")
    void rotate370Test() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        assertEquals(0, obj1.getRotation(), 0.01);
        obj1.rotate(42);
        assertEquals(42, obj1.getRotation(), 0.01);
        Point isCentroid = obj1.getPosition();
        assertEquals(0, isCentroid.getX(), 0.01);
        assertEquals(0, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("rotate -60 degree")
    void rotateMinus60Test() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        assertEquals(0, obj1.getRotation(), 0.01);
        obj1.rotate(-60);
        assertEquals(-60, obj1.getRotation(), 0.01);
        Point isCentroid = obj1.getPosition();
        assertEquals(0, isCentroid.getX(), 0.01);
        assertEquals(0, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("get position")
    void getPositionTest() {
        Point shouldCentroid = new Point(30, 25);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 0);
        Point isCentroid = obj1.getPosition();
        assertEquals(30, isCentroid.getX(), 0.01);
        assertEquals(25, isCentroid.getY(), 0.01);

    }

    @Test
    @DisplayName("get rotation")
    void getRotationTest() {
        Point shouldCentroid = new Point(30, 25);
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), shouldCentroid, 180);
        assertEquals(180, obj1.getRotation(), 0.01);
    }

    @Test
    @DisplayName("intersection")
    void intersectWithTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        JtsCAD2dObject obj2 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj2.setPosition(new Point(3, 3));
        obj2.rotate(400);
        assertTrue(obj1.intersectWith(obj2));
    }

    @Test
    @DisplayName("no intersection")
    void noIntersectionTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        JtsCAD2dObject obj2 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj2.setPosition(new Point(30, 3));
        obj2.rotate(400);
        assertFalse(obj1.intersectWith(obj2));
    }

    @Test
    @DisplayName("inside")
    void isInsideTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        JtsCAD2dObject obj2 = new JtsCAD2dObject(createExampleCoordinates2(), JtsCAD2dObject.InitialPosition.ZERO);
        obj2.setPosition(new Point(1, 1));
        assertTrue(obj1.isInside(obj2));
    }

    @Test
    @DisplayName("not inside")
    void isNotInsideTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        JtsCAD2dObject obj2 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj2.setPosition(new Point(3, 3));
        assertFalse(obj1.isInside(obj2));
    }

    @Test
    @DisplayName("distance to another polygon")
    void distanceToTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        JtsCAD2dObject obj2 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj2.setPosition(new Point(30, 3));
        obj2.rotate(400);
        assertEquals(17.95, obj1.distanceTo(obj2), 0.01);
    }

    @Test
    @DisplayName("Get dimension")
    void getDimensionTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj1.move(new Point(80, 60));
        obj1.rotate(400);

        Point dimension = obj1.getDimension();
        assertEquals(10, dimension.getX(), 0.01);
        assertEquals(10, dimension.getY(), 0.01);
    }

    @Test
    @DisplayName("Get size")
    void getSizeTest() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.ZERO);
        obj1.move(new Point(80, 60));
        obj1.rotate(400);
        double size = obj1.getSize();
        assertEquals(100, size, 0.01);
    }

    @Test
    @DisplayName("Get dimension")
    void getDimensionTest2() {
        JtsCAD2dObject obj1 = new JtsCAD2dObject(createExampleCoordinates3(), JtsCAD2dObject.InitialPosition.ZERO);
        obj1.move(new Point(4,-2));
        obj1.rotate(30);

        Point dimension = obj1.getDimension();
        assertEquals(2, dimension.getX(), 0.01);
        assertEquals(20, dimension.getY(), 0.01);
    }

    @Test
    @DisplayName("rotate around centroid")
    void rotateAroundCentroid() {
        JtsCAD2dObject obj = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
        obj.rotate(90);

        assertEquals(90, obj.getRotation());
        Point[] points = obj.getVertices();
        assertEquals(10, points[0].getX(), 0.01);
        assertEquals(0, points[0].getY(), 0.01);
        assertEquals(10, points[1].getX(), 0.01);
        assertEquals(10, points[1].getY(), 0.01);
        assertEquals(0, points[2].getX(), 0.01);
        assertEquals(10, points[2].getY(), 0.01);
        assertEquals(0, points[3].getX(), 0.01);
        assertEquals(0, points[3].getY(), 0.01);
        assertEquals(10, points[4].getX(), 0.01);
        assertEquals(0, points[4].getY(), 0.01);

        Point isCentroid = obj.getPosition();
        assertEquals(5, isCentroid.getX(), 0.01);
        assertEquals(5, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("rotate around zero")
    void rotateAroundZero() {
        Point zero = new Point(0, 0);
        JtsCAD2dObject obj = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
        obj.rotateAround(90, zero);

        assertEquals(90, obj.getRotation());
        Point[] points = obj.getVertices();
        assertEquals(0, points[0].getX(), 0.01);
        assertEquals(0, points[0].getY(), 0.01);
        assertEquals(0, points[1].getX(), 0.01);
        assertEquals(10, points[1].getY(), 0.01);
        assertEquals(-10, points[2].getX(), 0.01);
        assertEquals(10, points[2].getY(), 0.01);
        assertEquals(-10, points[3].getX(), 0.01);
        assertEquals(0, points[3].getY(), 0.01);
        assertEquals(0, points[4].getX(), 0.01);
        assertEquals(0, points[4].getY(), 0.01);

        Point isCentroid = obj.getPosition();
        assertEquals(-5, isCentroid.getX(), 0.01);
        assertEquals(5, isCentroid.getY(), 0.01);
    }

    @Test
    @DisplayName("Rotate around centroid")
    void rotateAroundForeignCentroid() {
        JtsCAD2dObject foreigner = new JtsCAD2dObject(createExampleCoordinates(), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
        JtsCAD2dObject main = new JtsCAD2dObject(createExampleCoordinatesUnder1(), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
        foreigner.rotate(90+90+45);
        main.rotateAround(90+90+45, foreigner.getPosition());
        Point centroidForeigner = foreigner.getPosition();
        Point centroidMain = main.getPosition();

        assertEquals(5, centroidForeigner.getX(), 0.01);
        assertEquals(5, centroidForeigner.getY(), 0.01);

        assertEquals(-2.07, centroidMain.getX(), 0.01);
        assertEquals(12.07, centroidMain.getY(), 0.01);
    }

    private CoordinateSequence createExampleCoordinates() {
        Coordinate ol = new CoordinateXY(0, 10);
        Coordinate or = new CoordinateXY(10, 10);
        Coordinate ul = new CoordinateXY(0, 0);
        Coordinate ur = new CoordinateXY(10, 0);

        Coordinate[] coords = new Coordinate[]{ul, ur, or, ol, ul};
        return new CoordinateArraySequence(coords);
    }

    private CoordinateSequence createExampleCoordinatesUnder1() {
        Coordinate ol = new CoordinateXY(0, 0);
        Coordinate or = new CoordinateXY(10, 0);
        Coordinate ul = new CoordinateXY(0, -10);
        Coordinate ur = new CoordinateXY(10, -10);

        Coordinate[] coords = new Coordinate[]{ul, ur, or, ol, ul};
        return new CoordinateArraySequence(coords);
    }

    private CoordinateSequence createExampleCoordinates2() {
        Coordinate ol = new CoordinateXY(0, 2);
        Coordinate or = new CoordinateXY(2, 2);
        Coordinate ul = new CoordinateXY(0, 0);
        Coordinate ur = new CoordinateXY(2, 0);

        Coordinate[] coords = new Coordinate[]{ul, ur, or, ol, ul};
        return new CoordinateArraySequence(coords);
    }

    private CoordinateSequence createExampleCoordinates3() {
        Coordinate ol = new CoordinateXY(0, 20);
        Coordinate or = new CoordinateXY(2, 20);
        Coordinate ul = new CoordinateXY(0, 0);
        Coordinate ur = new CoordinateXY(2, 0);

        Coordinate[] coords = new Coordinate[]{ul, ur, or, ol, ul};
        return new CoordinateArraySequence(coords);
    }

    private Point[] createExamplePoints() {
        Point ol = new Point(0, 10);
        Point or = new Point(10, 0);
        Point ul = new Point(0, 0);
        Point ur = new Point(10, 10);
        return new Point[]{ol, or, ul, ur};
    }

    private boolean containsPoint(Point[] points, Point toCheck) {
        return Arrays.stream(points).anyMatch(p -> p.isEqualTo(toCheck));
    }

}
