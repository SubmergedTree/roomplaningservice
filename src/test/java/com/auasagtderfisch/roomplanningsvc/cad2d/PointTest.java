/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PointTest {

    @Test
    @DisplayName("two points are not equal")
    public void equalFalseTest() {
        Point p1 = new Point(2, 5);
        Point p2 = new Point(6, 1);
        assertFalse(p1.isEqualTo(p2));
    }

    @Test
    @DisplayName("two points are equal")
    public void equalTrueTest() {
        Point p1 = new Point(2, 5);
        Point p2 = new Point(2, 5);
        assertTrue(p1.isEqualTo(p2));
    }

    @Test
    @DisplayName("add two points")
    public void addTest() {
        Point p1 = new Point(2, 5);
        Point p2 = new Point(6, 1);

        Point result = p1.add(p2);

        assertTrue(p1.isEqualTo(new Point(2,5)));
        assertTrue(p2.isEqualTo(new Point(6,1)));
        assertTrue(result.isEqualTo(new Point(8,6)));
    }

    @Test
    @DisplayName("subtract two points")
    public void subtractTest() {
        Point p1 = new Point(2, 5);
        Point p2 = new Point(6, 1);

        Point result = p1.subtract(p2);

        assertTrue(p1.isEqualTo(new Point(2,5)));
        assertTrue(p2.isEqualTo(new Point(6,1)));
        assertTrue(result.isEqualTo(new Point(-4,4)));
    }

    @Test
    @DisplayName("calculate distance to (0,0)")
    public void distanceToZeroTest() {
        Point p1 = new Point(2, 5);
        double distance = p1.distanceToZero();
        assertEquals(5.385164807134504, distance);
    }

    @Test
    @DisplayName("calculate distance of two points")
    public void distanceToTest() {
        Point p1 = new Point(2, 5);
        Point p2 = new Point(6, 1);
        double distance = p1.distanceTo(p2);
        assertEquals(5.656854249492381, distance);
    }

}
