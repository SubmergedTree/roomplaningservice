/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObjectFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.PointListFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Area;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.ConferenceDesk;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Desk;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DeskTest {

    @Test
    @DisplayName("Check if a Desk is used standingNotUpright and lowestDepth is updated accordingly")
    @Disabled
    void updateStandingNotUprightTest() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();

        List<Point> points = pointsFactory.createRectangle(new Point(0, 600), new Point(600, 600), new Point(0, 0), new Point(600, 0) );
        JtsCAD2dObject dimension = dimensionFactory.create(points);
        Point globalPosition = new Point(0,0);

        Desk testDesk = new Desk(points, globalPosition, 0.0, dimensionFactory, pointsFactory);
        testDesk.setStandingNotUpright(true);
        double result = 0.0f;
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.MovementArea) {
                result = a.getLowestDepth();
            }
        }
        assertEquals(120.0f, result);
    }

    @Test
    @DisplayName("Make a Desk WheelChairCompatible and check if movementArea has the right size")
    @Disabled
    void updateMakeAWheelChairCompatibleTest() {
   /*     CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();

        List<Point> points = pointsFactory.createRectangle(new Point(0, 5), new Point(5, 5), new Point(0, 0), new Point(5, 0) );
        //JtsCAD2dObject dimension = dimensionFactory.create(points);
        Point globalPosition = new Point(0,0);

        Desk testDesk = new Desk(points, globalPosition, 90.0, dimensionFactory, pointsFactory);
        testDesk.setWheelchairAccessible(false);
        testDesk.setWheelchairCompatible(true);
        //List<Point> zero_points = pointsFactory.createRectangle(new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0) );
        //JtsCAD2dObject result = dimensionFactory.create(zero_points);
        List<Area> testAreas = testDesk.getAreas();
        double testAngle = testDesk.getRotation();
        double areaAngle = 0.0;
        Point[] resultPoints = null;//testAreas.get(1).getAreaObject().getVertices();
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                resultPoints = a.getAreaObject().getVertices();
                areaAngle = a.getAngle();
            }
        }

        assertEquals(0.0, resultPoints[0].getY());
        //assertEquals(0.0f, resultPoints[0].getX());
        //assertEquals(1500.0f, result.getVertices()[0].getY());
       // assertEquals(1500.0f, result.getVertices()[1].getX());
        //assertEquals(1500.0f, result.getVertices()[1].getY());
      //  assertEquals(0.0f, result.getVertices()[2].getX());
      //  assertEquals(0.0f, result.getVertices()[2].getY());
     //   assertEquals(1500.0f, result.getVertices()[3].getX());
     //   assertEquals(0.0f, result.getVertices()[3].getY());*/
    }

    @Test
    @DisplayName("Make a Desk WheelChairAccessible and Compatible and check if movementArea has the right lowestDepth")
    @Disabled
    void updateMakeAWheelChairAccessibleAndCompatibleTest() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();

        List<Point> points = pointsFactory.createRectangle(new Point(0, 600), new Point(600, 600), new Point(0, 0), new Point(600, 0) );
        JtsCAD2dObject dimension = dimensionFactory.create(points);
        Point globalPosition = new Point(0,0);

        Desk testDesk = new Desk(points, globalPosition, 0.0, dimensionFactory, pointsFactory);
        testDesk.setWheelchairCompatible(true);
        testDesk.setWheelchairAccessible(true);
        double result = 0.0;
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.MovementArea) {
                result = a.getLowestDepth();
            }
        }

        assertEquals(1200.0f, result);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 0° rotation")
    void checkAreaWithNoRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 0.0, dimensionFactory, pointsFactory);
        Point test_dimension = new Point();
        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
                test_dimension = a.getAreaObject().getDimension();
            }
            test_dimension = testDesk.getCAD2dObject().getDimension();
        }
        assertEquals(-1.0, result[0].getX(), 0.01);
        assertEquals(-101.0, result[0].getY(), 0.01);
        assertEquals(1.0, result[1].getX(), 0.01);
        assertEquals(-101.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(-1.0, result[2].getY(), 0.01);
        assertEquals(-1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 90° rotation")
    void checkAreaWithRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 90.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(101.0, result[0].getX(), 0.01);
        assertEquals(-1.0, result[0].getY(), 0.01);
        assertEquals(101.0, result[1].getX(), 0.01);
        assertEquals(1.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 45° rotation")
    void checkAreaWith45Rotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);
        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 45.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(70.71, result[0].getX(), 0.01);
        assertEquals(-72.12, result[0].getY(), 0.01);
        assertEquals(72.12, result[1].getX(), 0.01);
        assertEquals(-70.71, result[1].getY(), 0.01);
        assertEquals(1.41, result[2].getX(), 0.01);
        assertEquals(0, result[2].getY(), 0.01);
        assertEquals(0, result[3].getX(), 0.01);
        assertEquals(-1.41, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 90° rotation")
    void checkAreaWithRotationAndStandingNotUpright() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 90.0, dimensionFactory, pointsFactory);
        testDesk.setStandingNotUpright(true);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.MovementArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(121.0, result[0].getX(), 0.01);
        assertEquals(-1.0, result[0].getY(), 0.01);
        assertEquals(121.0, result[1].getX(), 0.01);
        assertEquals(1.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }
}
