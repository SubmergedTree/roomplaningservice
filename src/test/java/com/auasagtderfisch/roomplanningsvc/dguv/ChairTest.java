/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObjectFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.PointListFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Area;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Chair;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Desk;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChairTest {

/*
    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 0° rotation")
    void checkAreaWithNoRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Chair testDesk = new Chair(dimension, globalPosition, 0.0, dimensionFactory, pointsFactory);
        Point test_dimension = new Point();
        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
                test_dimension = a.getAreaObject().getDimension();
            }
        }
        test_dimension = testDesk.getCAD2dObject().getDimension();
        assertEquals(-1.0, result[0].getX(), 0.01);
        assertEquals(-101.0, result[0].getY(), 0.01);
        assertEquals(1.0, result[1].getX(), 0.01);
        assertEquals(-101.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(-1.0, result[2].getY(), 0.01);
        assertEquals(-1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 90° rotation")
    void checkAreaWithRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 90.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(101.0, result[0].getX(), 0.01);
        assertEquals(-1.0, result[0].getY(), 0.01);
        assertEquals(101.0, result[1].getX(), 0.01);
        assertEquals(1.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 45° rotation")
    void checkAreaWith45Rotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);
        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 45.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(70.71, result[0].getX(), 0.01);
        assertEquals(-72.12, result[0].getY(), 0.01);
        assertEquals(72.12, result[1].getX(), 0.01);
        assertEquals(-70.71, result[1].getY(), 0.01);
        assertEquals(1.41, result[2].getX(), 0.01);
        assertEquals(0, result[2].getY(), 0.01);
        assertEquals(0, result[3].getX(), 0.01);
        assertEquals(-1.41, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 90° rotation")
    void checkAreaWithRotationAndStandingNotUpright() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        Desk testDesk = new Desk(points, globalPosition, 90.0, dimensionFactory, pointsFactory);
        testDesk.setStandingNotUpright(true);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.MovementArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(121.0, result[0].getX(), 0.01);
        assertEquals(-1.0, result[0].getY(), 0.01);
        assertEquals(121.0, result[1].getX(), 0.01);
        assertEquals(1.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }*/
}
