package com.auasagtderfisch.roomplanningsvc.dguv;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObjectFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.PointListFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Area;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.ConferenceDesk;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Desk;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConferenceDeskTest {
    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly")
    void checkAreaWithNoRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        ConferenceDesk testDesk = new ConferenceDesk(false, dimension, globalPosition, 0.0, dimensionFactory, pointsFactory);
        Point test_dimension = new Point();
        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        test_dimension = testDesk.getCAD2dObject().getDimension();
        assertEquals(-1.0, result[0].getX());
        assertEquals(-81.0, result[0].getY());
        assertEquals(1.0, result[1].getX());
        assertEquals(-81.0, result[1].getY());
        assertEquals(1.0, result[2].getX());
        assertEquals(-1.0, result[2].getY());
        assertEquals(-1.0, result[3].getX());
        assertEquals(-1.0, result[3].getY());
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 90 ° rotation")
    void checkAreaWithRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        ConferenceDesk testDesk = new ConferenceDesk(false, dimension, globalPosition, 90.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(81.0, result[0].getX(), 0.01);
        assertEquals(-1.0, result[0].getY(), 0.01);
        assertEquals(81.0, result[1].getX(), 0.01);
        assertEquals(1.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 180 ° rotation")
    void checkAreaWith180Rotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        ConferenceDesk testDesk = new ConferenceDesk(false, dimension, globalPosition, 180.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }
        assertEquals(1.0, result[0].getX(), 0.01);
        assertEquals(81.0, result[0].getY(), 0.01);
        assertEquals(-1.0, result[1].getX(), 0.01);
        assertEquals(81.0, result[1].getY(), 0.01);
        assertEquals(-1.0, result[2].getX(), 0.01);
        assertEquals(1.0, result[2].getY(), 0.01);
        assertEquals(1.0, result[3].getX(), 0.01);
        assertEquals(1.0, result[3].getY(), 0.01);
    }

    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 270 ° rotation")
    void checkAreaWith270Rotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        ConferenceDesk testDesk = new ConferenceDesk(false, dimension, globalPosition, 270.0, dimensionFactory, pointsFactory);

        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
            }
        }

        assertEquals(-81.0, result[0].getX(), 0.01);
        assertEquals(1.0, result[0].getY(), 0.01);
        assertEquals(-81.0, result[1].getX(), 0.01);
        assertEquals(-1.0, result[1].getY(), 0.01);
        assertEquals(-1.0, result[2].getX(), 0.01);
        assertEquals(-1.0, result[2].getY(), 0.01);
        assertEquals(-1.0, result[3].getX(), 0.01);
        assertEquals(1.0, result[3].getY(), 0.01);
    }
}
