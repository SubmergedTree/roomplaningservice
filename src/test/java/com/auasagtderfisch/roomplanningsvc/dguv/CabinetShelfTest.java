package com.auasagtderfisch.roomplanningsvc.dguv;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObjectFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.PointListFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Area;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.CabinetShelf;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Desk;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType.*;

public class CabinetShelfTest {
 /*    @Test
    @DisplayName("Check if the areas of a ConferenceDesk is calcultated accordingly with 0° rotation")
    void checkAreaWithNoRotation() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();
        Point globalPosition = new Point(0,0);

        Point dimension = new Point(2, 2);
        List<Point> points = pointsFactory.createRectangleFromDimension(dimension);

        CabinetShelf testDesk = new CabinetShelf(false, false, dimension, globalPosition, 0.0, dimensionFactory, pointsFactory, 0.0);
        Point test_dimension = new Point();
        Point[] result = new Point[5];
        for (Area a : testDesk.getAreas()) {
            if (a.getAreaType() == AreaType.UserArea) {
                result = a.getAreaObject().getVertices();
                test_dimension = a.getAreaObject().getDimension();
            }
        }
        test_dimension = testDesk.getCAD2dObject().getDimension();
       assertEquals(-1.0, result[0].getX(), 0.01);
        assertEquals(-101.0, result[0].getY(), 0.01);
        assertEquals(1.0, result[1].getX(), 0.01);
        assertEquals(-101.0, result[1].getY(), 0.01);
        assertEquals(1.0, result[2].getX(), 0.01);
        assertEquals(-1.0, result[2].getY(), 0.01);
        assertEquals(-1.0, result[3].getX(), 0.01);
        assertEquals(-1.0, result[3].getY(), 0.01);*/
    }

/*
    @Test
    @DisplayName("Check if a CabinetShelf is open and area is updated accordingly")
    void CabinetShelfIsOpenTest() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();

        List<Point> points = pointsFactory.createRectangle(new Point(0, 600), new Point(600, 600), new Point(0, 0), new Point(600, 0) );
        Point globalPosition = new Point(0,0);
        Point dimension = new Point(100,100);
        CabinetShelf testShelf = new CabinetShelf(dimension , globalPosition, 0.0,  dimensionFactory, pointsFactory);
        testShelf.setLongestDrawer(500.0f);
        testShelf.setOpen(true);

        double result = 0.0f;
        for (Area a : testShelf.getAreas()) {
            if (a.getAreaType() == AreaType.FunctionalArea) {
                result = a.getLowestDepth();
            }
        }
        assertEquals(1000.0f, result);
    }

    @Test
    @DisplayName("Check if a CabinetShelf is open and area is updated accordingly")
    void AreaTest() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();


        Point dimension = new Point(10,10);
        Point globalPosition = new Point(0,0);
        CabinetShelf testShelf = new CabinetShelf(dimension , globalPosition, 0.0,  dimensionFactory, pointsFactory);

        Area area1 = new Area();
        area1.setAreaType(AreaType.UserArea);
        Point[] resultList = testShelf.getCAD2dObject().getVertices();
        JtsCAD2dObject areaDimension = dimensionFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(new Point(-5,-5), new Point(5,-5),dimension));
        area1.setAreaObject(areaDimension);

        Point[] resultList_1 = area1.getAreaObject().getVertices();

        assertEquals(resultList_1[3], resultList[0]);
        assertEquals(resultList_1[1], resultList[1]);
        assertEquals(resultList_1[2], resultList[2]);
        assertEquals(resultList_1[3], resultList[3]);
        assertEquals(resultList_1[4], resultList[4]);
    }

    @Test
    @DisplayName("Test if longestDrawer is set correctly")
    void AreaLongestDrawerTest() {
        CAD2DObjectFactory dimensionFactory = new JtsCAD2dObjectFactoryImpl();
        PointListFactory pointsFactory = new PointListFactoryImpl();

        Point dimension = new Point(10,10);
        Point globalPosition = new Point(0,0);
        CabinetShelf testShelf = new CabinetShelf(dimension , globalPosition, 0.0,  dimensionFactory, pointsFactory);

        Area area1 = new Area();
        area1.setAreaType(AreaType.FunctionalArea);
        area1.setAreaObject(testShelf.getCAD2dObject());

        Point[] cabinetShelfVertices = testShelf.getCAD2dObject().getVertices();
        area1.setLowestDepth(1000.0, cabinetShelfVertices[2], cabinetShelfVertices[3]);

        testShelf.setLongestDrawer(500.0);
        testShelf.setOpen(true);

        Point[] resultList_1 = area1.getAreaObject().getVertices();
        List<Area> areaList = testShelf.getAreas();
        Area result = new Area();
        for (Area a : areaList) {
            if (a.getAreaType() == FunctionalArea) {
                result = a;
            }
        }
        Point[] resultList_area = result.getAreaObject().getVertices();

        assertEquals(resultList_1[0], resultList_area[0]);
        assertEquals(resultList_1[1], resultList_area[1]);
        assertEquals(resultList_1[2], resultList_area[2]);
        assertEquals(resultList_1[3], resultList_area[3]);
        assertEquals(resultList_1[4], resultList_area[4]);
    }
*/
//}
