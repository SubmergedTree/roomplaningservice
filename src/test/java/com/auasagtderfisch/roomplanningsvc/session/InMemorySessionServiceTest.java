/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardFactory;
import com.auasagtderfisch.roomplanningsvc.blackboard.impl.BlackboardVertxService;
import com.auasagtderfisch.roomplanningsvc.persistence.LoaderService;
import com.auasagtderfisch.roomplanningsvc.session.impl.InMemorySessionService;
import com.auasagtderfisch.roomplanningsvc.session.impl.InMemorySessionServicePojo;
import com.auasagtderfisch.roomplanningsvc.session.impl.InMemorySessionServiceVerticle;
import com.auasagtderfisch.roomplanningsvc.util.vertx.codec.GenericCodec;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(VertxExtension.class)
public class InMemorySessionServiceTest {

    LoaderService loaderService1 = mock(LoaderService.class);
    InMemorySessionService inMemorySessionService;

    private BlackboardFactory blackboardFactory = mock(BlackboardFactory.class);

    @BeforeEach
    void prepare(Vertx vertx, VertxTestContext testContext) {
        when(loaderService1.forWhat()).thenReturn("testSet");
        when(loaderService1.loadNew(anyString())).thenReturn(Future.succeededFuture());
        when(blackboardFactory.createBlackboardService(any(), any())).thenReturn(Future.succeededFuture(null));
        List<LoaderService> loaderServices = new LinkedList<>();
        loaderServices.add(loaderService1);
        vertx.deployVerticle(new InMemorySessionServiceVerticle(loaderServices, blackboardFactory, null), h -> {
            GenericCodec.registerDefault(vertx, InMemorySessionServicePojo.class);
            GenericCodec.registerDefault(vertx, UUID.class);
            GenericCodec.registerDefault(vertx, BlackboardVertxService.class);
            inMemorySessionService = new InMemorySessionService(vertx, null);
            testContext.completeNow();
        });
    }

    @AfterEach
    void cleanup(Vertx vertx, VertxTestContext testContext) {
        vertx.close(h -> {
            testContext.completeNow();
        });
    }

    @Test
    @DisplayName("Unknown role set")
    public void createProjectUnknownRoleSetTest(Vertx vertx, VertxTestContext testContext) {
        inMemorySessionService.createProject("testProject", "unknown", "testUser")
                .onComplete(h -> {
                    testContext.verify(() -> {
                        assertTrue(h.failed());
                        testContext.completeNow();
                    });
                });
    }

    /*
    @Test
    @DisplayName("create project is successful")
    public void createProjectSuccessTest(Vertx vertx, VertxTestContext testContext) {
        inMemorySessionService.createProject("testProject", "testSet", "testUser")
                .onComplete(h -> {
            testContext.verify(() -> {
                assertTrue(h.succeeded());
                testContext.completeNow();
            });
        });

    }*/
}
