/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.mock;

import io.vertx.core.AbstractVerticle;

public class MockCollisionVerticle extends AbstractVerticle {
//    private MessageConsumer<Object> mc;
//
//    @Override
//    public void start(Promise<Void> startPromise) {
//        listen();
//        startPromise.complete();
//    }
//
//    @Override
//    public void stop(Promise<Void> stopPromise) {
//        mc.unregister(h -> stopPromise.complete());
//    }
//
//    private void listen() {
//        mc = vertx.eventBus().consumer("collision").handler(msg -> {
//
//            WorkerTasks wt = (WorkerTasks)msg.body();
//            System.out.println("CALLED C: " + wt.getTasks().getTodo());
//
//          /*  System.out.println("-------------------------collisison");
//            wt.getTasks().stream().map(Task::getTodo).forEach(System.out::println);
//            System.out.println("-------------------------");*/
//         /*   String allTasks = wt.getTasks()
//                    .stream()
//                    .map(Task::getTodo)
//                    .reduce("", String::concat);
//
//            modifyLocalMapEntry(wt.getModelUUID(), allTasks);*/
//            modifyLocalMapEntry(wt.getModelUUID(), wt.getTasks().getTodo());
//
//            msg.reply("ACK");
//        });
//    }
//
//    private void modifyLocalMapEntry(String modelUUID, String tasks) {
//
//        try {
//            Thread.sleep(200);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        LocalMap<String, String> sharedMap;
//        synchronized (sharedMap = vertx.sharedData().getLocalMap("testMap")) {
//            String old = sharedMap.get(modelUUID + "#collision");
//            sharedMap.replace(modelUUID + "#collision", old + tasks);
//        }
//    }
}

