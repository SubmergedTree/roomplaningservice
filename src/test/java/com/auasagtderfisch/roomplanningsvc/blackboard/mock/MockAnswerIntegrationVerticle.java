/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.mock;

import io.vertx.core.AbstractVerticle;

public class MockAnswerIntegrationVerticle extends AbstractVerticle {
//    private MessageConsumer<Object> mc;
//
//    @Override
//    public void start(Promise<Void> startPromise) {
//        listen();
//        startPromise.complete();
//    }
//
//    @Override
//    public void stop(Promise<Void> stopPromise) {
//        mc.unregister(h -> stopPromise.complete());
//    }
//
//    private void listen() {
//        mc = vertx.eventBus().consumer("integration").handler(msg -> {
//            String modelUUID = (String)msg.body();
//            msg.reply(createAnswer(modelUUID));
//        });
//    }
//
//    private Answer createAnswer(String modelUUID) {
//        LocalMap<String, String> sharedMap = vertx.sharedData().getLocalMap("testMap");
//        String fp = sharedMap.get(modelUUID + "#fp");
//        String collision = sharedMap.get(modelUUID + "#collision");
//        String escape = sharedMap.get(modelUUID + "#escape");
//        return new Answer(fp  + " | "  + collision + " | " +escape);
//    }
}
