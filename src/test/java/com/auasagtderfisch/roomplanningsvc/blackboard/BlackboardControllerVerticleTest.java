/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard;

import io.vertx.junit5.VertxExtension;
import org.junit.jupiter.api.extension.ExtendWith;


import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(VertxExtension.class)
public class BlackboardControllerVerticleTest {

//    @BeforeEach
//    void prepare(Vertx vertx, VertxTestContext testContext) {
//        createLocalMap(vertx);
//        vertx.deployVerticle(new BlackboardControllerVerticle(),createTestDeploymentOptions(), h -> {
//            GenericCodec.registerDefault(vertx, Task.class);
//            GenericCodec.registerDefault(vertx, Answer.class);
//            GenericCodec.registerDefault(vertx, WorkerTasks.class);
//            testContext.completeNow();
//            vertx.deploymentIDs().forEach(System.out::println);
//         //   System.out.println("set up");
//        });
//    }
//
//    @AfterEach
//    void cleanup(Vertx vertx, VertxTestContext testContext) {
//        System.out.println("clean up");
//        vertx.close(h -> {
//            //System.out.println("____________");
//            //vertx.deploymentIDs().forEach(System.out::println);
//           // System.out.println("all closed");
//            vertx.sharedData().getLocalMap("testMap").close();
//            testContext.completeNow();
//        });
//    }
//
//    @Test
//    @DisplayName("One Request")
//    void sendOneRequestTest(Vertx vertx, VertxTestContext testContext) {
//        Task task = new Task("taskId", "modelId", "1");
//        vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task, testContext.succeeding(response -> {
//            Answer res = (Answer)response.body();
//            testContext.verify(() -> {
//                assertEquals("1 | 1 | 1", res.getTodo());
//                testContext.completeNow();
//            });
//        }));
//    }
//
//    @Test
//    @DisplayName("Send multiple requests with pause")
//    void sendMultipleRequestWithPauseTest(Vertx vertx, VertxTestContext testContext) {
//        Checkpoint deploymentCheckpoint = testContext.checkpoint();
//        Checkpoint messageReceived1 = testContext.checkpoint();
//        Checkpoint messageReceived2 = testContext.checkpoint();
//        Checkpoint messageReceived3 = testContext.checkpoint();
//
//        vertx.deployVerticle(new BlackboardControllerVerticle(),createTestDeploymentOptions(), testContext.succeeding(id -> {
//            deploymentCheckpoint.flag();
//            Task task1 = new Task("taskId1", "modelId", "1");
//            Task task2 = new Task("taskId2", "modelId", "2");
//            Task task3 = new Task("taskId3", "modelId", "3");
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task1, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//
//                testContext.verify(() -> {
//                    assertEquals("1 | 1 | 1", res.getTodo());
//                });
//                messageReceived1.flag();
//            }));
//
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task2, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//                testContext.verify(() -> {
//                    assertEquals("12 | 12 | 12", res.getTodo());
//                });
//                messageReceived2.flag();
//            }));
//
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task3, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//
//                testContext.verify(() -> {
//                    assertEquals("123 | 123 | 123", res.getTodo());
//                });
//                messageReceived3.flag();
//            }));
//        }));
//    }
//
//    @Test
//    void sendMultipleRequestTogetherTest(Vertx vertx, VertxTestContext testContext) {
//        Checkpoint deploymentCheckpoint = testContext.checkpoint();
//        Checkpoint messageReceived1 = testContext.checkpoint();
//        Checkpoint messageReceived2 = testContext.checkpoint();
//        Checkpoint messageReceived3 = testContext.checkpoint();
//
//        vertx.deployVerticle(new BlackboardControllerVerticle(),createTestDeploymentOptions(), testContext.succeeding(id -> {
//            deploymentCheckpoint.flag();
//            Task task1 = new Task("taskId1", "modelId", "1");
//            Task task2 = new Task("taskId2", "modelId", "2");
//            Task task3 = new Task("taskId3", "modelId", "3");
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task1, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//                messageReceived1.flag();
//            }));
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task2, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//                messageReceived2.flag();
//            }));
//
//            vertx.eventBus().request(BlackboardControllerVerticle.getListenerId("modelUUID"), task3, testContext.succeeding(response -> {
//                Answer res = (Answer)response.body();
//                System.out.println(res.getTodo());
//                // permutations possible
//              /*  testContext.verify(() -> {
//                    assertEquals("123 | 123 | 123", res.getTodo());
//                });*/
//                messageReceived3.flag();
//            }));
//        }));
//    }
//
//    // TODO test undeploy
//
//    // TODO error in worker test case
//
//    // TODO error in all worker test case
//
//    private DeploymentOptions createTestDeploymentOptions() {
//        JsonObject config = new JsonObject()
//                .put(BlackboardControllerVerticle.CONFIG_CALC_COLLISION_CMD,  "collision")
//                .put(BlackboardControllerVerticle.CONFIG_CALC_ESCAPE_ROUTE_CMD, "escape")
//                .put(BlackboardControllerVerticle.CONFIG_CALC_FURNITURE_PLACEMENT_CMD, "furniture")
//                .put(BlackboardControllerVerticle.CONFIG_CALC_INTEGRATION_CMD, "integration")
//                .put(BlackboardControllerVerticle.CONFIG_COLLISION_VERTICLE_NAME, MockCollisionVerticle.class.getName())
//                .put(BlackboardControllerVerticle.CONFIG_ESCAPE_VERTICLE_NAME, MockEscapeRouteVerticle.class.getName())
//                .put(BlackboardControllerVerticle.CONFIG_FURNITURE_PLACEMENT_VERTICLE_NAME, MockFurniturePlacementVerticle.class.getName())
//                .put(BlackboardControllerVerticle.CONFIG_INTEGRATION_VERTICLE_NAME, MockAnswerIntegrationVerticle.class.getName())
//                .put(BlackboardControllerVerticle.MODEL_LOCAL_MAP_NAME, "testMap")
//                .put(BlackboardControllerVerticle.MODEL_UUID, "modelUUID");
//        return new DeploymentOptions()
//                .setConfig(config);
//    }
//
//    private void createLocalMap(Vertx vertx) {
//        LocalMap<String, String> sharedMap = vertx.sharedData().getLocalMap("testMap");
//        sharedMap.put("modelUUID#fp", "");
//        sharedMap.put("modelUUID#collision", "");
//        sharedMap.put("modelUUID#escape", "");
//
//    }
}
