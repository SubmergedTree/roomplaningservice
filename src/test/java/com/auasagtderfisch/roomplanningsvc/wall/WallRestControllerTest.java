/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall;

import com.auasagtderfisch.roomplanningsvc.configuration.SpringTestConfig;
import com.auasagtderfisch.roomplanningsvc.util.validation.ValidationService;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.impl.StdRestVerticle;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import com.auasagtderfisch.roomplanningsvc.wall.impl.WallRestController;
import com.auasagtderfisch.roomplanningsvc.wall.pojo.ModifyWallPojo;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SpringTestConfig.class })
@ExtendWith(VertxExtension.class)
public class WallRestControllerTest {

    @Autowired
    private WallService wallService;
    @Autowired
    private ValidationService validationService;

    @BeforeEach
    void prepare(Vertx vertx, VertxTestContext testContext) {
        VertxRestController wallRestController = new WallRestController(wallService, validationService);
        List<VertxRestController> vertxRestControllers = new ArrayList<>();
        vertxRestControllers.add(wallRestController);
        StdRestVerticle stdRestVerticle = new StdRestVerticle(vertxRestControllers);
        vertx.deployVerticle(stdRestVerticle, h-> {
            testContext.completeNow();
        });
    }

    @AfterEach
    void cleanup(Vertx vertx, VertxTestContext testContext) {
        vertx.close(h -> {
            testContext.completeNow();
        });
    }

    // TODO check responses when implemented
    @Test
    @DisplayName("Add wall rest server test")
    void modifyWallTest(Vertx vertx, VertxTestContext testContext) {
        ModifyWallPojo modifyWallPojo = WallRestData.createAddWallPojo();
        Mockito.when(wallService.modifyWall(any())).thenReturn(Future.succeededFuture());
        JsonObject jsonObject = JsonObject.mapFrom(modifyWallPojo);
        WebClient client = WebClient.create(vertx);
        client.post(8080, "localhost", "/wall/modifyWall?sessionId=testId")
                .sendJsonObject(jsonObject,
                        testContext.succeeding(response -> {
                            testContext.verify( () -> {
                                assertTrue(response.statusCode() == 200);
                            });
                            testContext.completeNow();
                        }));
    }

    @Test
    @DisplayName("Add wall but parameter is missing")
    void addWallMissingParameterTest(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @Test
    @DisplayName("Add wall but service returns error")
    void modifyWallServiceErrorTest(Vertx vertx, VertxTestContext testContext) {
        ModifyWallPojo modifyWallPojo = WallRestData.createAddWallPojo();
        Mockito.when(wallService.modifyWall(any())).thenReturn(Future.failedFuture(new Exception("hilfe")));
        JsonObject jsonObject = JsonObject.mapFrom(modifyWallPojo);
        WebClient client = WebClient.create(vertx);
        client.post(8080, "localhost", "/wall/modifyWall?sessionId=testId")
                .sendJsonObject(jsonObject,
                        testContext.succeeding(response -> {
                            testContext.verify( () -> {
                                assertTrue(response.statusCode() == 400);
                            });
                            testContext.completeNow();
                        }));
    }

    @Test
    @DisplayName("get wall test")
    void getWall(Vertx vertx, VertxTestContext testContext) {
        Mockito.when(wallService.getWall(any())).thenReturn(Future.succeededFuture());
        WebClient client = WebClient.create(vertx);
        client.get(8080, "localhost", "/wall/getWall?sessionId=testId")
                .send(testContext.succeeding(response -> {
                            testContext.verify( () -> {
                                assertTrue(response.statusCode() == 200);
                            });
                            testContext.completeNow();
                        }));
    }
}
