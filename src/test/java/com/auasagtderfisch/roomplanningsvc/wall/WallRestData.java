/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.wall.pojo.ModifyWallPojo;

import java.util.ArrayList;
import java.util.List;

public class WallRestData {
    public static ModifyWallPojo createAddWallPojo() {
        Point pointA = new Point(200, 300);
        Point pointB = new Point(10, 20);
        List<Point> points = new ArrayList<>();
        points.add(pointA);
        points.add(pointB);
        return new ModifyWallPojo(points,"name", "sessionId");
    }
}
