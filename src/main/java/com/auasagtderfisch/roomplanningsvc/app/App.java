/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.app;

import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.impl.BlackboardVertxService;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.session.impl.InMemorySessionServicePojo;
import com.auasagtderfisch.roomplanningsvc.session.impl.InMemorySessionServiceVerticle;
import com.auasagtderfisch.roomplanningsvc.util.vertx.codec.GenericCodec;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.impl.RestVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.shareddata.SharedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.UUID;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "com.auasagtderfisch")
public class App {
    private final Vertx vertx = Vertx.vertx();

    @Autowired
    private InMemorySessionServiceVerticle inMemorySessionServiceVerticle;

    @Autowired
    @Qualifier("APIKeyRestVerticle")
    private RestVerticle stdRestVerticle;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        /*GeometryFactory factory = new GeometryFactory();

        Coordinate ol = new CoordinateXY(0, 10);
        Coordinate or = new CoordinateXY(10, 10);
        Coordinate ul = new CoordinateXY(0, 0);
        Coordinate ur = new CoordinateXY(10, 0);

        Coordinate ul1 = new CoordinateXY(5, 5);
        Coordinate ur1 = new CoordinateXY(15, 5);
        Coordinate or1 = new CoordinateXY(15, 15);
        Coordinate ol1 = new CoordinateXY(5, 15);


        Coordinate coords[] = new Coordinate[]{ul, ur, or, ol, ul};
        Coordinate coords1[] = new Coordinate[]{ul1, ur1, or1, ol1, ul1};

        CoordinateSequence coordinateSequence = new CoordinateArraySequence(coords);
        Geometry geometry = factory.createPolygon(coordinateSequence);

        CoordinateSequence coordinateSequence1 = new CoordinateArraySequence(coords1);
        Geometry geometry1 = factory.createPolygon(coordinateSequence1);

        System.out.println(geometry.getArea());
        System.out.println(geometry.intersects(geometry1));
*/
    }

    @Bean
    public Vertx vertx() {
        return vertx;
    }

    @Bean
    public SharedData sharedData() {
        return vertx.sharedData();
    }

    @PostConstruct
    public void deployVerticle() {
        vertx.deployVerticle(stdRestVerticle);
        vertx.deployVerticle(inMemorySessionServiceVerticle);
        registerCodecs();
    }

    private void registerCodecs() {
        GenericCodec.registerDefault(vertx, Task.class);
        GenericCodec.registerDefault(vertx, Answer.class);
        GenericCodec.registerDefault(vertx, BlackboardVertxService.class);
        GenericCodec.registerDefault(vertx, InMemorySessionServicePojo.class);
        GenericCodec.registerDefault(vertx, UUID.class);
    }
}