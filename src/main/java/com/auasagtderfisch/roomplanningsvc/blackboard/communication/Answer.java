/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.communication;

import io.vertx.core.Future;

public class Answer {
    private final Object answer;
    private final Throwable error;

    public Answer(Object answer) {
        this.answer = answer;
        error = null;
    }

    public Answer(Throwable error) {
        this.answer = null;
        this.error = error;
    }

    public Object getAnswer() {
        return answer;
    }

    public Future<Object> unpack() {
        if (this.answer != null)
            return Future.succeededFuture(answer);
        return Future.failedFuture(error);
    }
}
