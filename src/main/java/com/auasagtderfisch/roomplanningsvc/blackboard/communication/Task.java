/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.communication;

public class Task {
    private final TaskType taskType; // TODO use polymorphism -> class with type attribute
    private final Object Task;

    public Task(TaskType taskType, Object task) {
        this.taskType = taskType;
        Task = task;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public Object getTask() {
        return Task;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskType=" + taskType +
                ", Task=" + Task +
                '}';
    }
}
