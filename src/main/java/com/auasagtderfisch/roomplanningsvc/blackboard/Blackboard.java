/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard;


public interface Blackboard {
   // M get();
   // void set(M m);

    <T extends AbstractData> T get();
    <T extends AbstractData> void set(T t);

}
