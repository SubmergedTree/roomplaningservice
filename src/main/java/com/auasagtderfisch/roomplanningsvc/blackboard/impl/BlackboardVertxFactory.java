/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardFactory;
import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Room;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.placer.FurniturePlacer;
import com.auasagtderfisch.roomplanningsvc.util.function.Function2;
import com.auasagtderfisch.roomplanningsvc.wall.impl.worker.WallWorker;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class BlackboardVertxFactory implements BlackboardFactory {

    private final Vertx vertx;
    private final Function2<String, Blackboard, WallWorker> wallWorkerFactory;
    private final Function2<String, Blackboard, FurniturePlacer> furniturePlacerFactory;

    @Autowired
    public BlackboardVertxFactory(Vertx vertx,
                                  Function2<String, Blackboard, WallWorker> wallWorkerFactory, // can be a list of all matching factories. create blackbiod gets an key which decides which factories to create
                                  Function2<String, Blackboard, FurniturePlacer> furniturePlacerFactory) {
        this.vertx = vertx;
        this.wallWorkerFactory = wallWorkerFactory;
        this.furniturePlacerFactory = furniturePlacerFactory;
    }

    @Override
    public Future<BlackboardService> createBlackboardService(UUID modelUUID, Object model) {
        String modelUUIDStr = modelUUID.toString();
        String listenerId = BlackboardControllerVerticle.getListenerId(modelUUIDStr);
        BlackboardControllerVerticle blackboardControllerVerticle = createBlackboard(modelUUIDStr, model);
        DeploymentOptions deploymentOptions = createDeploymentOptions();
        Future<String> escapeRoute = Future.future(p -> vertx.deployVerticle(blackboardControllerVerticle, deploymentOptions, p));
        return escapeRoute.map(deploymentId -> new BlackboardVertxService(listenerId, deploymentId, vertx));
    }

    private BlackboardControllerVerticle createBlackboard(String modelUUIDStr, Object model) {
        var blackboard = new RoomBlackboard(); // TODO move into factory
        blackboard.set((Room)model);
        var wallWorker = wallWorkerFactory.apply(modelUUIDStr, blackboard);
        var furniturePlacer = furniturePlacerFactory.apply(modelUUIDStr, blackboard);
        return new BlackboardControllerVerticle(wallWorker, furniturePlacer, modelUUIDStr);
    }

    private DeploymentOptions createDeploymentOptions() {
        return new DeploymentOptions()
                .setWorker(true);
    }
}

