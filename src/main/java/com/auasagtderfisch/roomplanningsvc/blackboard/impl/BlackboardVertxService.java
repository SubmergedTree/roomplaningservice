/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class BlackboardVertxService implements BlackboardService {

    private final String listenerId;
    private final String deploymentId;
    private final Vertx vertx;

    public BlackboardVertxService(String listenerId, String deploymentId, Vertx vertx) {
        this.listenerId = listenerId;
        this.deploymentId = deploymentId;
        this.vertx = vertx;
    }

    @Override
    public Future<Answer> addTask(Task task) {
        return Future.future(h -> vertx.eventBus().request(listenerId, task, cb -> {
            if (cb.succeeded())
                h.complete((Answer)cb.result().body());
            else
                h.fail(cb.cause());
        }));
    }

    @Override
    public Future<Void> kill() {
        return Future.future(p -> vertx.undeploy(deploymentId, p));
    }
}
