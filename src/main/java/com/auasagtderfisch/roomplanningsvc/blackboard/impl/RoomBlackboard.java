/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.impl;


import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractData;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Room;

public class RoomBlackboard implements Blackboard {

    private Room room;

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractData> T get() {
        return (T)room;
    }

    @Override
    public <T extends AbstractData> void set(T t) {
        this.room = (Room)t;
    }
}
