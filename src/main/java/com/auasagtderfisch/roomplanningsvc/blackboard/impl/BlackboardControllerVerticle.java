/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.placer.FurniturePlacer;
import com.auasagtderfisch.roomplanningsvc.wall.impl.worker.WallWorker;
import io.vertx.core.*;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BlackboardControllerVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(BlackboardControllerVerticle.class);

    private static final String LISTENER_ID_PREFIX = "blackboard";

    private final String modelUUID;
    private final String listenerId;

    private MessageConsumer<Object> mc;

    public static String getListenerId(String modelUUID) {
        return LISTENER_ID_PREFIX + modelUUID;
    }

    private final WallWorker wallWorker;
    private String wallWorkerDeploymentId;

    private final FurniturePlacer furniturePlacerWorker;
    private String furniturePlacerDeploymentId;


    public BlackboardControllerVerticle(WallWorker wallWorker, FurniturePlacer furniturePlacer, String modelUUID) {
        this.wallWorker = wallWorker;
        this.furniturePlacerWorker = furniturePlacer;

        this.modelUUID = modelUUID;

        this.listenerId = getListenerId(modelUUID);
    }

    @Override
    public void start(Promise<Void> onStart) {
        this.setupWorker().onComplete(h -> {
            if (h.succeeded()) {
                listen();
                logger.info("Blackboard started. listens to: " + listenerId);
                onStart.complete();
            } else {
                onStart.fail(h.cause());
            }
        });
    }

    @Override
    public void stop(Promise<Void> onStop) {
        mc.unregister(h -> {
            Future<Void> wallWorkerDestroyed = Future.future(p -> vertx.undeploy(wallWorkerDeploymentId, p));
            Future<Void> furniturePlacerWorkerDestroyed = Future.future(p -> vertx.undeploy(furniturePlacerDeploymentId, p));

            CompositeFuture.all(wallWorkerDestroyed, furniturePlacerWorkerDestroyed)
                    .onComplete(handler -> {
                if(handler.succeeded()) {
                    logger.info("shutdown blackboard");
                    onStop.complete();
                } else {
                    // Edge case for unit tests: vertx.close
                    if (!vertx.deploymentIDs().contains(wallWorkerDeploymentId) &&
                        !vertx.deploymentIDs().contains(furniturePlacerDeploymentId)) {
                        logger.info("shutdown blackboard");
                        onStop.complete();
                    } else {
                        logger.warn("error on shutting down blackboard");
                        onStop.fail(handler.cause());
                    }
                }

            });
        });
    }

    private CompositeFuture setupWorker() {
        DeploymentOptions options = new DeploymentOptions().setWorker(true);
        Future<String> wall = Future.future(p -> vertx.deployVerticle(this.wallWorker, options, p));
        Future<String> furniturePlacer = Future.future(p -> vertx.deployVerticle(this.furniturePlacerWorker, options, p));

        return CompositeFuture.all(wall, furniturePlacer)
                .onSuccess(h -> {
                    this.wallWorkerDeploymentId = wall.result();
                    this.furniturePlacerDeploymentId = furniturePlacer.result();
                })
                .onFailure(f -> {
                    System.out.println(f.getCause().getMessage());
                });
    }

    private void listen() {
        mc = vertx.eventBus().consumer(listenerId).handler(msg -> {
            Task task = (Task) msg.body();
            selectWorker(task, msg);
        });
    }

    private void  selectWorker(Task task, Message<Object> msg) {
        switch (task.getTaskType()) {
            case WallTask:
                runWallWorker(task, msg);
                break;
            case FurnitureTask:
                runFurnitureWorker(task, msg);
                break;
        }
    }

    private void runWallWorker(Task task, Message<Object> msg) {
        calcWall(task).onComplete(h -> {
            if (h.succeeded()) {
                msg.reply(h.result().body());
            } else {
                msg.fail(-1, h.cause().getMessage());
            }
        });
    }

    private void runFurnitureWorker(Task task, Message<Object> msg) {
        calcFurniture(task).onComplete(h -> {
            if (h.succeeded()) {
                msg.reply(h.result().body());
            } else {
                msg.fail(-1, h.cause().getMessage());
            }
        });
    }

    private Future<Message<Object>> calcWall(Task task) {
        return Future.future(cb -> vertx.eventBus().request(modelUUID+"#WallWorker", task, cb));
    }

    private Future<Message<Object>> calcFurniture(Task task) {
        return Future.future(cb -> vertx.eventBus().request(modelUUID+"#FurnitureWorker", task, cb));
    }
}
