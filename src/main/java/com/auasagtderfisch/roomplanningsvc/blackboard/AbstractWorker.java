/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.blackboard;

import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.Optional;

public abstract class AbstractWorker extends AbstractVerticle {

    protected final Blackboard store;
    protected final CAD2DObjectFactory cad2dObjectFactory;
    protected final LocalizationService localizationService;

    private MessageConsumer<Object> mc;
    private final String listenTo;

    public static String getListenTo(String modelUUID, String callCmd) {
        return modelUUID.concat(callCmd);
    }

    public AbstractWorker(Blackboard store,
                          CAD2DObjectFactory cad2dObjectFactory,
                          LocalizationService localizationService,
                          String listenTo) {
        this.store = store;
        this.cad2dObjectFactory = cad2dObjectFactory;
        this.localizationService = localizationService;
        this.listenTo = listenTo;
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        listen();
        startPromise.complete();
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        mc.unregister(h -> stopPromise.complete());
    }

    private void listen() {
        mc = vertx.eventBus().consumer(listenTo).handler(msg -> {
            Task task = (Task) msg.body();
            Optional<Answer>  answer = onTaskReceived(task);
            answer.ifPresentOrElse(msg::reply, () -> msg.reply(new Answer(null)));
        });
    }

    protected abstract Optional<Answer> onTaskReceived(Task task);
}
