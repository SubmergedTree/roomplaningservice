/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.persistence;

import io.vertx.core.Future;

import java.util.Set;

public interface LoaderService {
    Future<Object> loadNew(String name);
    Future<Object> load(String name);
    Future<Void> safe(String name);
    Future<Void> unload(String name);
    Future<Set<String>> getStoredProjectNames();
    String forWhat();
}
