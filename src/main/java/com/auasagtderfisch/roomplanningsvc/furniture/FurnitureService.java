/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyCabinetShelfPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyChairPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyConferenceDeskPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyDeskPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.DeleteFurnitureResponsePojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.FurnitureResponsePojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.GetAllResponsePojo;
import io.vertx.core.Future;

public interface FurnitureService {
    Future<GetAllResponsePojo> getAll(String sessionId);
    Future<DeleteFurnitureResponsePojo> delete(String sessionId, String furnitureId);

    Future<FurnitureResponsePojo> addDesk(String sessionId, DeskPojo deskPojo);
    Future<FurnitureResponsePojo> addContainer(String sessionId, ContainerPojo containerPojo);
    Future<FurnitureResponsePojo> addConferenceDesk(String sessionId, ConferenceDeskPojo conferenceDesk);
    Future<FurnitureResponsePojo> addCabinetShelf(String sessionId, CabinetShelfPojo cabinetShelf);
    Future<FurnitureResponsePojo> addChair(String sessionId, ChairPojo chair);

    Future<FurnitureResponsePojo> modifyDesk(String sessionId, String furnitureId, ModifyDeskPojo deskPojo);
    Future<FurnitureResponsePojo> modifyContainer(String sessionId, String furnitureId, ContainerPojo containerPojo);
    Future<FurnitureResponsePojo> modifyConferenceDesk(String sessionId, String furnitureId, ModifyConferenceDeskPojo conferenceDesk);
    Future<FurnitureResponsePojo> modifyCabinetShelf(String sessionId, String furnitureId, ModifyCabinetShelfPojo cabinetShelf);
    Future<FurnitureResponsePojo> modifyChair(String sessionId, String furnitureId, ModifyChairPojo chair);


}
