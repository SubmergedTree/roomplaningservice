/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.BaseFurniture;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties.DeskProperties;

import javax.validation.constraints.NotNull;

public class ModifyDeskPojo extends BaseFurniture {
    @NotNull
    public DeskProperties properties;
}
