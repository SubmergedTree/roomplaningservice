package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class ModifyChairTaskBuilder {
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public ModifyChairTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public ModifyChairTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public ModifyChairTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public ModifyChairTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public ModifyChairTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ModifyChairTask createModifyChairTask() {
        return new ModifyChairTask(position, dimension, furnitureID, rotation, name);
    }
}