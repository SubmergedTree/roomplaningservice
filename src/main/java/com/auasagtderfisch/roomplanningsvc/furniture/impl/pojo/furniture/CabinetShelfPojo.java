/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties.CabinetShelfProperties;

import javax.validation.constraints.NotNull;

public class CabinetShelfPojo extends BaseFurnitureWithDimensionPojo {
    @NotNull
    public CabinetShelfProperties properties;

    public CabinetShelfPojo() {
        super();
    }
}
