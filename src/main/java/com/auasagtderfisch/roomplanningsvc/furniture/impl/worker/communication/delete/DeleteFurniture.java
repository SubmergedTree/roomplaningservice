package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class DeleteFurniture extends FurnitureTask {
    private final String furnitureID;

    public DeleteFurniture (String furnitureID) {
        super(FurnitureTaskType.DELETE_FURNITURE);
        this.furnitureID = furnitureID;
    }

    public String getFurnitureID() {
        return furnitureID;
    }
}
