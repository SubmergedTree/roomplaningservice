package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class AddConferenceDeskTaskBuilder {
    private boolean backsideIsEmpty;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public AddConferenceDeskTaskBuilder setBacksideIsEmpty(boolean backsideIsEmpty) {
        this.backsideIsEmpty = backsideIsEmpty;
        return this;
    }

    public AddConferenceDeskTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public AddConferenceDeskTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public AddConferenceDeskTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public AddConferenceDeskTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public AddConferenceDeskTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AddConferenceDeskTask createAddConferenceDeskTask() {
        return new AddConferenceDeskTask(backsideIsEmpty, position, dimension, furnitureID, rotation, name);
    }
}