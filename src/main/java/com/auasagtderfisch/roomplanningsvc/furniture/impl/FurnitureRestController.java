/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl;


import com.auasagtderfisch.roomplanningsvc.furniture.FurnitureService;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyCabinetShelfPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyChairPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyConferenceDeskPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyDeskPojo;
import com.auasagtderfisch.roomplanningsvc.util.validation.ValidationService;
import com.auasagtderfisch.roomplanningsvc.util.vertx.json.JsonUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.RestUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FurnitureRestController implements VertxRestController {

    private final static String SESSION_ID_KEY = "sessionId";
    private final static String FURNITURE_ID_KEY = "furnitureId";

    private final FurnitureService furnitureService;
    private final ValidationService validationService;

    @Autowired
    public FurnitureRestController(FurnitureService furnitureService, ValidationService validationService) {
        this.furnitureService = furnitureService;
        this.validationService = validationService;
    }

    @Override
    public Router setupRoutes(Vertx vertx) {
        Router subRouter = Router.router(vertx);
        setUpAddFurnitureRoutes(subRouter);
        setUpModifyFurnitureRoutes(subRouter);
        setUpDeleteFurnitureRoutes(subRouter);
        setUpGetFurnitureRoutes(subRouter);
        return subRouter;
    }

    @Override
    public String getSubRoute() {
        return "/furniture";
    }

    private void setUpAddFurnitureRoutes(Router subRouter) {
        subRouter.route("/addDesk*").handler(BodyHandler.create());
        subRouter.post("/addDesk").handler(this::addDesk);

        // subRouter.route("/addContainer*").handler(BodyHandler.create());
        // subRouter.post("/addContainer").handler(this::addContainer);

        subRouter.route("/addConferenceDesk*").handler(BodyHandler.create());
        subRouter.post("/addConferenceDesk").handler(this::addConferenceDesk);

        subRouter.route("/addCabinetShelf*").handler(BodyHandler.create());
        subRouter.post("/addCabinetShelf").handler(this::addCabinetShelf);

        subRouter.route("/addChair*").handler(BodyHandler.create());
        subRouter.post("/addChair").handler(this::addChair);
    }

    private void setUpModifyFurnitureRoutes(Router subRouter) {
        subRouter.route("/modifyDesk*").handler(BodyHandler.create());
        subRouter.put("/modifyDesk").handler(this::modifyDesk);

        subRouter.route("/modifyConferenceDesk*").handler(BodyHandler.create());
        subRouter.put("/modifyConferenceDesk").handler(this::modifyConferenceDesk);

        subRouter.route("/modifyCabinetShelf*").handler(BodyHandler.create());
        subRouter.put("/modifyCabinetShelf").handler(this::modifyCabinetShelf);

        subRouter.route("/modifyChair*").handler(BodyHandler.create());
        subRouter.put("/modifyChair").handler(this::modifyChair);

    }

    private void setUpDeleteFurnitureRoutes(Router subRouter) {
        subRouter.route("/delete*").handler(BodyHandler.create());
        subRouter.delete("/delete").handler(this::delete);
    }

    private void setUpGetFurnitureRoutes(Router subRouter) {
        subRouter.route("/getAll*").handler(BodyHandler.create());
        subRouter.get("/getAll").handler(this::getAll);
    }

    private void addDesk(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var deskPojo = JsonUtil.serializeRequest(routingContext, DeskPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, deskPojo)
                .flatMap(c -> furnitureService.addDesk(sessionId.result(), deskPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void modifyDesk(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        var deskPojo = JsonUtil.serializeRequest(routingContext, ModifyDeskPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, furnitureId, deskPojo)
                .flatMap(c -> furnitureService.modifyDesk(sessionId.result(), furnitureId.result(),deskPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void addContainer(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var containerPojo = JsonUtil.serializeRequest(routingContext, ContainerPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, containerPojo)
                .flatMap(c -> furnitureService.addContainer(sessionId.result(), containerPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void addConferenceDesk(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var conferenceDeskPojo = JsonUtil.serializeRequest(routingContext, ConferenceDeskPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, conferenceDeskPojo)
                .flatMap(c -> furnitureService.addConferenceDesk(sessionId.result(), conferenceDeskPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void modifyConferenceDesk(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        var conferenceDeskPojo = JsonUtil.serializeRequest(routingContext, ModifyConferenceDeskPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, furnitureId, conferenceDeskPojo)
                .flatMap(c -> furnitureService.modifyConferenceDesk(sessionId.result(),
                        furnitureId.result(),conferenceDeskPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void addCabinetShelf(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var cabinetShelfPojo = JsonUtil.serializeRequest(routingContext, CabinetShelfPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, cabinetShelfPojo)
                .flatMap(c -> furnitureService.addCabinetShelf(sessionId.result(), cabinetShelfPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void modifyCabinetShelf(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        var cabinetShelfPojo = JsonUtil.serializeRequest(routingContext, ModifyCabinetShelfPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, furnitureId, cabinetShelfPojo)
                .flatMap(c -> furnitureService.modifyCabinetShelf(sessionId.result(),
                        furnitureId.result(),cabinetShelfPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void addChair(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var chairPojo = JsonUtil.serializeRequest(routingContext, ChairPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, chairPojo)
                .flatMap(c -> furnitureService.addChair(sessionId.result(), chairPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void modifyChair(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        var chairPojo = JsonUtil.serializeRequest(routingContext, ModifyChairPojo.class)
                .flatMap(validationService::validate);
        CompositeFuture.all(sessionId, furnitureId, chairPojo)
                .flatMap(c -> furnitureService.modifyChair(sessionId.result(),
                        furnitureId.result(),chairPojo.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void delete(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        CompositeFuture.all(sessionId, furnitureId)
                .flatMap(c -> furnitureService.delete(sessionId.result(), furnitureId.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void getAll(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        sessionId.flatMap(furnitureService::getAll)
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

}
