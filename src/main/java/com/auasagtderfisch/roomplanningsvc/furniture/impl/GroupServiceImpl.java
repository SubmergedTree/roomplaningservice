/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.TaskType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Group;
import com.auasagtderfisch.roomplanningsvc.furniture.GroupService;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group.*;
import com.auasagtderfisch.roomplanningsvc.session.SessionService;
import io.vertx.core.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupServiceImpl implements GroupService {

    private final SessionService sessionService;

    @Autowired
    public GroupServiceImpl(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    public Future<NewGroupPojo> createGroup(String sessionId, String groupName) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new CreateGroupTask(groupName));
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (CreateGroupAnswer)a)
                            .map(a -> new NewGroupPojo(a.getGroupId()));
                });
    }

    @Override
    public Future<GroupDeletedPojo> deleteGroup(String sessionId, String groupId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new DeleteGroupTask(groupId));
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (DeleteGroupAnswer)a)
                            .map(a -> new GroupDeletedPojo(a.getGroupId()));
                });
    }

    @Override
    public Future<AddToGroupPojo> addToGroup(String sessionId, String groupId, String furnitureId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new AddToGroupTask(furnitureId, groupId));
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (AddToGroupAnswer)a)
                            .map(a -> new AddToGroupPojo(a.getGroupId(), a.getFurnitureId()));
                });
    }

    @Override
    public Future<DeletedFromGroupPojo> deleteFromGroup(String sessionId, String groupId, String furnitureId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new DeleteFromGroupTask(furnitureId, groupId));
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (DeleteFromGroupAnswer)a)
                            .map(a -> new DeletedFromGroupPojo(a.getGroupId(), a.getDeletedFurnitureId()));
                });
    }

    @Override
    public Future<GroupsPojo> getGroups(String sessionId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new GetGroupsTask());
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (GetGroupsAnswer)a)
                            .map(a -> new GroupsPojo(toPojo(a.getGroups())));
                });
    }

    private List<GroupPojo> toPojo(Map<String, Group> groups) {
        List<GroupPojo> pojo = new ArrayList<>();
        for (Map.Entry<String, Group> entry : groups.entrySet()) {
            String groupId = entry.getKey();
            Group group = entry.getValue();
            GroupPojo pojoGroup = new GroupPojo(groupId, group.getName(), group.getFurnitureIds());
            pojo.add(pojoGroup);
        }
        return pojo;
    }
}
