/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.TaskType;
import com.auasagtderfisch.roomplanningsvc.furniture.FurnitureService;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.FurniturePojoFactory;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyCabinetShelfPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyChairPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyConferenceDeskPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify.ModifyDeskPojo;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.GetFurnitureTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.ModifyFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.GetFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete.DeleteFurniture;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete.DeleteFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify.*;
import com.auasagtderfisch.roomplanningsvc.session.SessionService;
import io.vertx.core.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FurnitureServiceImpl implements FurnitureService {

    private final SessionService sessionService;
    private final FurniturePojoFactory furniturePojoFactory;

    @Autowired
    public FurnitureServiceImpl(SessionService sessionService, FurniturePojoFactory furniturePojoFactory) {
        this.sessionService = sessionService;
        this.furniturePojoFactory = furniturePojoFactory;
    }

    @Override
    public Future<GetAllResponsePojo> getAll(String sessionId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    Task task = new Task(TaskType.FurnitureTask, new GetFurnitureTask());
                    return blackboardService.addTask(task)
                            .map(a -> (GetFurnitureAnswer)a.getAnswer())
                            .map(this::buildFurnitureResponsePojo);
                });
    }

    @Override
    public Future<DeleteFurnitureResponsePojo> delete(String sessionId, String furnitureId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    DeleteFurniture deleteFurnitureTask = new DeleteFurniture(furnitureId);
                    return blackboardService.addTask(new Task(TaskType.FurnitureTask, deleteFurnitureTask))
                            .flatMap(Answer::unpack)
                            .map(a -> (DeleteFurnitureAnswer)a)
                            .map(a -> new DeleteFurnitureResponsePojoBuilder()
                                    .setDeletedId(a.getDeletedId())
                                    .setErrorMessages(a.getErrorMessages())
                                    .setDeletedName(a.getDeletedName())
                                    .createDeleteFurnitureResponsePojo());
                });
    }

    @Override
    public Future<FurnitureResponsePojo> addDesk(String sessionId, DeskPojo deskPojo) {
        var furnitureId = generateFurnitureID();
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    AddDeskTask addDeskTask = new AddDeskTaskBuilder()
                            .setDimension(deskPojo.dimension)
                            .setPosition(deskPojo.position)
                            .setName(deskPojo.getName())
                            .setRotation(deskPojo.getAngle())
                            .setWheelchairAccessible(deskPojo.properties.wheelchairAccessible)
                            .setStandingNotUpright(deskPojo.properties.standingNotUpright)
                            .setWheelchairCompatible(deskPojo.properties.wheelchairCompatible)
                            .setFurnitureID(furnitureId)
                            .createAddDeskTask();
                    return this.addTask(blackboardService, addDeskTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> addContainer(String sessionId, ContainerPojo containerPojo) {
        return null;
    }

    @Override
    public Future<FurnitureResponsePojo> addConferenceDesk(String sessionId, ConferenceDeskPojo conferenceDesk) {
        var furnitureId = generateFurnitureID();
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    AddConferenceDeskTask conferenceDeskTask = new AddConferenceDeskTaskBuilder()
                            .setDimension(conferenceDesk.dimension)
                            .setPosition(conferenceDesk.position)
                            .setBacksideIsEmpty(conferenceDesk.properties.backsideIsEmpty)
                            .setName(conferenceDesk.name)
                            .setRotation(conferenceDesk.angle)
                            .setFurnitureID(furnitureId)
                            .createAddConferenceDeskTask();
                    return this.addTask(blackboardService, conferenceDeskTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> addCabinetShelf(String sessionId, CabinetShelfPojo cabinetShelf) {
        var furnitureId = generateFurnitureID();
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    AddCabinetShelfTask addCabinetShelfTask = new AddCabinetShelfTaskBuilder()
                            .setDimension(cabinetShelf.dimension)
                            .setPosition(cabinetShelf.position)
                            .setIsOpen(cabinetShelf.properties.isOpen)
                            .setIsSeldomInUse(cabinetShelf.properties.isSeldomInUse)
                            .setLongestDrawer(cabinetShelf.properties.longestDrawer)
                            .setName(cabinetShelf.name)
                            .setRotation(cabinetShelf.angle)
                            .setFurnitureID(furnitureId)
                            .createAddCabinetShelfTask();
                    return this.addTask(blackboardService, addCabinetShelfTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> addChair(String sessionId, ChairPojo chair) {
        var furnitureId = generateFurnitureID();
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    AddChairTask addChairTask = new AddChairTaskBuilder()
                            .setDimension(chair.dimension)
                            .setPosition(chair.position)
                            .setName(chair.name)
                            .setRotation(chair.angle)
                            .setFurnitureID(furnitureId)
                            .createAddChairTask();
                    return this.addTask(blackboardService, addChairTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> modifyDesk(String sessionId, String furnitureId, ModifyDeskPojo deskPojo) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    ModifyDeskTask modifyDeskTask = new ModifyDeskTaskBuilder()
                          //  .setDimension(deskPojo.getDimension())
                            .setPosition(deskPojo.getPosition())
                            .setName(deskPojo.getName())
                            .setRotation(deskPojo.getAngle())
                            .setWheelchairAccessible(deskPojo.properties.wheelchairAccessible)
                            .setStandingNotUpright(deskPojo.properties.standingNotUpright)
                            .setWheelchairCompatible(deskPojo.properties.wheelchairCompatible)
                            .setFurnitureID(furnitureId)
                            .createModifyDeskTask();
                    return this.addTask(blackboardService, modifyDeskTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> modifyContainer(String sessionId, String furnitureId, ContainerPojo containerPojo) {
        return null;
    }

    @Override
    public Future<FurnitureResponsePojo> modifyConferenceDesk(String sessionId, String furnitureId, ModifyConferenceDeskPojo conferenceDesk) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    ModifyConferenceDeskTask modifyConferenceDeskTask = new ModifyConferenceDeskTaskBuilder()
                            //.setDimension(conferenceDesk.dimension)
                            .setPosition(conferenceDesk.position)
                            .setBacksideIsEmpty(conferenceDesk.properties.backsideIsEmpty)
                            .setName(conferenceDesk.name)
                            .setRotation(conferenceDesk.angle)
                            .setFurnitureID(furnitureId)
                            .createModifyConferenceDeskTask();
                    return this.addTask(blackboardService, modifyConferenceDeskTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> modifyCabinetShelf(String sessionId, String furnitureId, ModifyCabinetShelfPojo cabinetShelf) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    ModifyCabinetShelfTask modifyCabinetShelfTask = new ModifyCabinetShelfTaskBuilder()
                            //.setDimension(cabinetShelf.dimension)
                            .setPosition(cabinetShelf.position)
                            .setIsOpen(cabinetShelf.properties.isOpen)
                            .setIsSeldomInUse(cabinetShelf.properties.isSeldomInUse)
                            .setLongestDrawer(cabinetShelf.properties.longestDrawer)
                            .setName(cabinetShelf.name)
                            .setRotation(cabinetShelf.angle)
                            .setFurnitureID(furnitureId)
                            .createModifyCabinetShelfTask();
                    return this.addTask(blackboardService, modifyCabinetShelfTask, furnitureId);
                });
    }

    @Override
    public Future<FurnitureResponsePojo> modifyChair(String sessionId, String furnitureId, ModifyChairPojo chair) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    ModifyChairTask modifyChairTask = new ModifyChairTaskBuilder()
                            //.setDimension(chair.dimension)
                            .setPosition(chair.position)
                            .setName(chair.name)
                            .setRotation(chair.angle)
                            .setFurnitureID(furnitureId)
                            .createModifyChairTask();
                    return this.addTask(blackboardService, modifyChairTask, furnitureId);
                });
    }

    private GetAllResponsePojo buildFurnitureResponsePojo(GetFurnitureAnswer getFurnitureAnswer) {
        return new GetAllResponsePojoBuilder()
                .setDesks(furniturePojoFactory.toDeskPojos(getFurnitureAnswer.getDesks()))
                .setCabinetShelfs(furniturePojoFactory.toCabinetShelfPojos(getFurnitureAnswer.getCabinetShelfs()))
                .setChairs(furniturePojoFactory.toChairPojos(getFurnitureAnswer.getChairs()))
                .setConferenceDesks(furniturePojoFactory.toConferenceDeskPojos(getFurnitureAnswer.getConferenceDesks()))
                .setErrors(getFurnitureAnswer.getErrors())
                .createGetAllResponsePojo();
    }

    private String generateFurnitureID() {
        return UUID.randomUUID().toString();
    }

    private Future<FurnitureResponsePojo> addTask(BlackboardService blackboardService, Object task, String furnitureId) {
        return blackboardService.addTask(new Task(TaskType.FurnitureTask, task))
                .flatMap(Answer::unpack)
                .map(a -> (ModifyFurnitureAnswer)a)
                .map(a -> new FurnitureResponsePojo(a.getError(),
                        furnitureId,
                        furniturePojoFactory.toAreaPojos(a.getAreas())));
    }
}
