/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group;

import java.util.List;

public class GroupPojo {
    private final String groupId;
    private final List<String> furnitureIds;
    private final String name;

    public GroupPojo(String groupId, String name, List<String> furnitureIds) {
        this.groupId = groupId;
        this.furnitureIds = furnitureIds;
        this.name = name;
    }

    public List<String> getFurnitureIds() {
        return furnitureIds;
    }

    public String getName() {
        return name;
    }

    public String getGroupId() {
        return groupId;
    }
}
