/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.*;

import java.util.List;

public class GetFurnitureAnswer {
    private final List<Chair> chairs;
    private final List<CabinetShelf> cabinetShelfs;
    private final List<Desk> desks;
    private final List<ConferenceDesk> conferenceDesks;
    private final List<Container> containers;
    private final List<RuleEvalError> errors;

    public GetFurnitureAnswer(List<Chair> chairs, List<CabinetShelf> cabinetShelfs, List<Desk> desks, List<ConferenceDesk> conferenceDesks, List<Container> containers, List<RuleEvalError> errors) {
        this.chairs = chairs;
        this.cabinetShelfs = cabinetShelfs;
        this.desks = desks;
        this.conferenceDesks = conferenceDesks;
        this.containers = containers;
        this.errors = errors;
    }

    public List<Chair> getChairs() {
        return chairs;
    }

    public List<CabinetShelf> getCabinetShelfs() {
        return cabinetShelfs;
    }

    public List<Desk> getDesks() {
        return desks;
    }

    public List<ConferenceDesk> getConferenceDesks() {
        return conferenceDesks;
    }

    public List<Container> getContainers() {
        return containers;
    }

    public List<RuleEvalError> getErrors() {
        return errors;
    }
}
