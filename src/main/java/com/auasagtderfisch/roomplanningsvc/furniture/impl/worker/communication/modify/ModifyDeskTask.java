/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class ModifyDeskTask extends FurnitureTask {

    private final boolean wheelchairCompatible;
    private final boolean wheelchairAccessible;
    private final boolean standingNotUpright;
    private final double rotation;
    private final Point position;
    private final Point dimension;
    private final String furnitureID;
    private final String name;

    public ModifyDeskTask(boolean wheelchairCompatible, boolean wheelchairAccessible, boolean standingNotUpright, Point position, Point dimension, String furnitureID, double rotation, String name) {
        super(FurnitureTaskType.MODIFY_DESK);
        this.wheelchairCompatible = wheelchairCompatible;
        this.wheelchairAccessible = wheelchairAccessible;
        this.standingNotUpright = standingNotUpright;
        this.position = position;
        this.dimension = dimension;
        this.furnitureID = furnitureID;
        this.rotation = rotation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isWheelchairCompatible() {
        return wheelchairCompatible;
    }

    public boolean isWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public boolean isStandingNotUpright() {
        return standingNotUpright;
    }

    public Point getPosition() {
        return position;
    }

    public Point getDimension() {
        return dimension;
    }

    public String getFurnitureID() {
        return furnitureID;
    }

    public double getRotation() {
        return rotation;
    }
}
