package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class ModifyDeskTaskBuilder {
    private boolean wheelchairCompatible;
    private boolean wheelchairAccessible;
    private boolean standingNotUpright;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public ModifyDeskTaskBuilder setWheelchairCompatible(boolean wheelchairCompatible) {
        this.wheelchairCompatible = wheelchairCompatible;
        return this;
    }

    public ModifyDeskTaskBuilder setWheelchairAccessible(boolean wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
        return this;
    }

    public ModifyDeskTaskBuilder setStandingNotUpright(boolean standingNotUpright) {
        this.standingNotUpright = standingNotUpright;
        return this;
    }

    public ModifyDeskTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public ModifyDeskTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public ModifyDeskTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public ModifyDeskTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public ModifyDeskTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ModifyDeskTask createModifyDeskTask() {
        return new ModifyDeskTask(wheelchairCompatible, wheelchairAccessible, standingNotUpright, position, dimension, furnitureID, rotation, name);
    }
}