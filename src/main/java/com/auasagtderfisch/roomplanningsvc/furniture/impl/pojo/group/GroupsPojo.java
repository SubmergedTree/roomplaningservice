/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group;

import java.util.List;
import java.util.Map;

public class GroupsPojo {
    private final List<GroupPojo> groups;

    public GroupsPojo(List<GroupPojo> groups) {
        this.groups = groups;
    }

    public List<GroupPojo> getGroups() {
        return groups;
    }
}
