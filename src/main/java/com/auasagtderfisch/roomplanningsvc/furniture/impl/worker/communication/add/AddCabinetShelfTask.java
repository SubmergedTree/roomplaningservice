package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class AddCabinetShelfTask extends FurnitureTask {
    private double longestDrawer = 0.0;
    private boolean isSeldomInUse;
    private boolean isOpen;
    private final Point position;
    private final Point dimension;
    private final String furnitureID;
    private final String name;
    private final double rotation;

    public AddCabinetShelfTask(boolean isSeldomInUse, boolean isOpen, Point position, Point dimension, String furnitureID, double rotation, String name, double longestDrawer) {
        super(FurnitureTaskType.ADD_CABINETSHELF);
        this.isSeldomInUse = isSeldomInUse;
        this.longestDrawer = longestDrawer;
        this.isOpen = isOpen;
        this.position = position;
        this.dimension = dimension;
        this.furnitureID = furnitureID;
        this.rotation = rotation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getRotation() {
        return rotation;
    }

    public double getLongestDrawer() {
        return longestDrawer;
    }

    public boolean isSeldomInUse() {
        return isSeldomInUse;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public Point getPosition() {
        return position;
    }

    public Point getDimension() {
        return dimension;
    }

    public String getFurnitureID() {
        return furnitureID;
    }
}
