/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.furniture;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.AreaPojo;

import java.util.List;

public class ChairPojo extends com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.ChairPojo {

    public String furnitureId;

    public String getFurnitureId() {
        return furnitureId;
    }

    public List<AreaPojo> areas;

    public List<AreaPojo> getAreas() {
        return areas;
    }

    public ChairPojo() {

    }
}
