/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.*;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties.CabinetShelfProperties;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties.ConferenceDeskProperties;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties.DeskProperties;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.furniture.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FurniturePojoFactory {

    private DeskPojo toDeskPojo(Desk desk) {
        DeskPojo deskPojo = new DeskPojo();
        deskPojo.properties = new DeskProperties();
        deskPojo.angle = desk.getRotation();
        deskPojo.dimension = desk.getCAD2dObject().getDimension();
        deskPojo.position = desk.getPosition();
        deskPojo.name = desk.getName();
        deskPojo.properties.standingNotUpright = desk.isStandingNotUpright();
        deskPojo.properties.wheelchairCompatible = desk.isWheelchairCompatible();
        deskPojo.properties.wheelchairAccessible = desk.isWheelchairAccessible();
        deskPojo.furnitureId = desk.getId().toString();
        deskPojo.areas = toAreaPojos(desk.getAreas());
        return deskPojo;
    }

    private ChairPojo toChairPojo(Chair chair) {
        ChairPojo chairPojo = new ChairPojo();
        chairPojo.properties = new HashMap<>();
        chairPojo.angle = chair.getRotation();
        chairPojo.dimension = chair.getCAD2dObject().getDimension();
        chairPojo.position = chair.getPosition();
        chairPojo.name = chair.getName();
        chairPojo.furnitureId = chair.getId().toString();
        chairPojo.areas = toAreaPojos(chair.getAreas());
        return chairPojo;
    }

    private CabinetShelfPojo toCabinetShelfPojo(CabinetShelf cabinetShelf) {
        CabinetShelfPojo cabinetShelfPojo = new CabinetShelfPojo();
        cabinetShelfPojo.properties = new CabinetShelfProperties();
        cabinetShelfPojo.angle = cabinetShelf.getRotation();
        cabinetShelfPojo.dimension = cabinetShelf.getCAD2dObject().getDimension();
        cabinetShelfPojo.position = cabinetShelf.getPosition();
        cabinetShelfPojo.name = cabinetShelf.getName();
        cabinetShelfPojo.furnitureId = cabinetShelf.getId().toString();
        cabinetShelfPojo.properties.longestDrawer = cabinetShelf.getLongestDrawer();
        cabinetShelfPojo.properties.isSeldomInUse = cabinetShelf.getIsSeldomInUse();
        cabinetShelfPojo.properties.isOpen = cabinetShelf.isOpen();
        cabinetShelfPojo.areas = toAreaPojos(cabinetShelf.getAreas());
        return cabinetShelfPojo;
    }
/*
    private DeskPojo toContainerPojo(Desk desk) {
        DeskPojo deskPojo = new DeskPojo();
        deskPojo.angle = desk.getRotation();
        deskPojo.dimension = desk.getCAD2dObject().getDimension();
        deskPojo.position = desk.getPosition();
        deskPojo.name = desk.getName();
        deskPojo.properties.standingNotUpright = desk.isStandingNotUpright();
        deskPojo.properties.wheelchairCompatible = desk.isWheelchairCompatible();
        deskPojo.properties.wheelchairAccessible = desk.isWheelchairAccessible();
        deskPojo.areas = toAreaPojos(desk.getAreas());
        return deskPojo;
    }
*/
    private ConferenceDeskPojo toConferenceDeskPojo(ConferenceDesk conferenceDesk) {
        ConferenceDeskPojo conferenceDeskPojo = new ConferenceDeskPojo();
        conferenceDeskPojo.properties = new ConferenceDeskProperties();
        conferenceDeskPojo.angle = conferenceDesk.getRotation();
        conferenceDeskPojo.dimension = conferenceDesk.getCAD2dObject().getDimension();
        conferenceDeskPojo.position = conferenceDesk.getPosition();
        conferenceDeskPojo.name = conferenceDesk.getName();
        conferenceDeskPojo.furnitureId = conferenceDesk.getId().toString();
        conferenceDeskPojo.properties.backsideIsEmpty = conferenceDesk.isBacksideIsEmpty();
        conferenceDeskPojo.areas = toAreaPojos(conferenceDesk.getAreas());
        return conferenceDeskPojo;
    }

    public List<DeskPojo> toDeskPojos(List<Desk> desks) {
        return desks
                .stream()
                .map(this::toDeskPojo)
                .collect(Collectors.toList());
    }

    public List<ConferenceDeskPojo> toConferenceDeskPojos(List<ConferenceDesk> conferenceDesks) {
        return conferenceDesks
                .stream()
                .map(this::toConferenceDeskPojo)
                .collect(Collectors.toList());
    }

    public List<CabinetShelfPojo> toCabinetShelfPojos(List<CabinetShelf> cabinetShelves) {
        return cabinetShelves
                .stream()
                .map(this::toCabinetShelfPojo)
                .collect(Collectors.toList());
    }

    public List<ChairPojo> toChairPojos(List<Chair> chairs) {
        return chairs
                .stream()
                .map(this::toChairPojo)
                .collect(Collectors.toList());
    }

    public List<AreaPojo> toAreaPojos(List<Area> areas) {
        return areas
                .stream()
                .map(area -> {
                    var position = area.getPosition();
                    var areaType = area.getAreaType().name();
                    var dimension = area.getAreaObject().getDimension();
                    return new AreaPojo(position, dimension, areaType);
                }).collect(Collectors.toList());
    }
}
