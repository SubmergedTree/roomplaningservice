/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

public class DeleteFromGroupAnswer {
    private final String groupId;
    private final String deletedFurnitureId;

    public DeleteFromGroupAnswer(String groupId, String furnitureId) {
        this.groupId = groupId;
        this.deletedFurnitureId = furnitureId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getDeletedFurnitureId() {
        return deletedFurnitureId;
    }
}
