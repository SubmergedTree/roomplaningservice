package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class AddDeskTaskBuilder {
    private boolean wheelchairCompatible;
    private boolean wheelchairAccessible;
    private boolean standingNotUpright;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public AddDeskTaskBuilder setWheelchairCompatible(boolean wheelchairCompatible) {
        this.wheelchairCompatible = wheelchairCompatible;
        return this;
    }

    public AddDeskTaskBuilder setWheelchairAccessible(boolean wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
        return this;
    }

    public AddDeskTaskBuilder setStandingNotUpright(boolean standingNotUpright) {
        this.standingNotUpright = standingNotUpright;
        return this;
    }

    public AddDeskTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public AddDeskTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public AddDeskTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public AddDeskTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public AddDeskTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AddDeskTask createAddDeskTask() {
        return new AddDeskTask(wheelchairCompatible, wheelchairAccessible, standingNotUpright, position, dimension, furnitureID, rotation, name);
    }
}