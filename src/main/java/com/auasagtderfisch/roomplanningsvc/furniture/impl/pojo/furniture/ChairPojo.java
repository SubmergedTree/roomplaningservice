/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture;


import javax.validation.constraints.NotNull;
import java.util.Map;

public class ChairPojo extends BaseFurnitureWithDimensionPojo {

    @NotNull
    public Map<String, String> properties;

    public ChairPojo() {
        super();
    }
}
