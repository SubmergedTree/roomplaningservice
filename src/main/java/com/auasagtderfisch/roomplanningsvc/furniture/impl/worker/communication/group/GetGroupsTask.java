/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class GetGroupsTask extends FurnitureTask {

    public GetGroupsTask() {
        super(FurnitureTaskType.GET_GROUPS);
    }

}
