package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class ModifyCabinetShelfTaskBuilder {
    private boolean isSeldomInUse;
    private boolean isOpen;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;
    private double longestDrawer;

    public ModifyCabinetShelfTaskBuilder setIsSeldomInUse(boolean isSeldomInUse) {
        this.isSeldomInUse = isSeldomInUse;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ModifyCabinetShelfTaskBuilder setLongestDrawer(double longestDrawer) {
        this.longestDrawer = longestDrawer;
        return this;
    }

    public ModifyCabinetShelfTask createModifyCabinetShelfTask() {
        return new ModifyCabinetShelfTask(isSeldomInUse, isOpen, position, dimension, furnitureID, rotation, name, longestDrawer);
    }
}