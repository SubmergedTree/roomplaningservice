/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.furniture.*;

import java.util.List;

public class GetAllResponsePojo {
    public final List<DeskPojo> desks;
    public final List<ChairPojo> chairs;
    public final List<ConferenceDeskPojo> conferenceDesks;
  //  public final List<ContainerPojo> containers;
    public final List<CabinetShelfPojo> cabinetShelfs;
    private final List<RuleEvalError> errors;

    public GetAllResponsePojo(List<DeskPojo> desks,
                              List<ChairPojo> chairs,
                              List<ConferenceDeskPojo> conferenceDesks,
                           //   List<ContainerPojo> containers,
                              List<CabinetShelfPojo> cabinetShelfs,
                              List<RuleEvalError> errors) {
        this.desks = desks;
        this.chairs = chairs;
        this.conferenceDesks = conferenceDesks;
     //   this.containers = containers;
        this.cabinetShelfs = cabinetShelfs;
        this.errors = errors;
    }

    public List<DeskPojo> getDesks() {
        return desks;
    }

    public List<ChairPojo> getChairs() {
        return chairs;
    }

    public List<ConferenceDeskPojo> getConferenceDesks() {
        return conferenceDesks;
    }

    /*public List<ContainerPojo> getContainers() {
        return containers;
    }*/

    public List<CabinetShelfPojo> getCabinetShelfs() {
        return cabinetShelfs;
    }

    public List<RuleEvalError> getErrors() {
        return errors;
    }
}
