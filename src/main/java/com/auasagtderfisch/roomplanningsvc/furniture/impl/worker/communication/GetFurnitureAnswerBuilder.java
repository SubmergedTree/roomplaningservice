/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.*;

import java.util.List;

public class GetFurnitureAnswerBuilder {
    private List<Chair> chairs;
    private List<CabinetShelf> cabinetShelfs;
    private List<Desk> desks;
    private List<ConferenceDesk> conferenceDesks;
    private List<Container> containers;
    private List<RuleEvalError> errors;

    public GetFurnitureAnswerBuilder setChairs(List<Chair> chairs) {
        this.chairs = chairs;
        return this;
    }

    public GetFurnitureAnswerBuilder setCabinetShelfs(List<CabinetShelf> cabinetShelfs) {
        this.cabinetShelfs = cabinetShelfs;
        return this;
    }

    public GetFurnitureAnswerBuilder setDesks(List<Desk> desks) {
        this.desks = desks;
        return this;
    }

    public GetFurnitureAnswerBuilder setConferenceDesks(List<ConferenceDesk> conferenceDesks) {
        this.conferenceDesks = conferenceDesks;
        return this;
    }

    public GetFurnitureAnswerBuilder setContainers(List<Container> containers) {
        this.containers = containers;
        return this;
    }

    public GetFurnitureAnswerBuilder setError(List<RuleEvalError> errors) {
        this.errors = errors;
        return this;
    }

    public GetFurnitureAnswer createGetFurnitureAnswer() {
        return new GetFurnitureAnswer(chairs, cabinetShelfs, desks, conferenceDesks, containers, errors);
    }
}