/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response.furniture.*;

import java.util.List;

public class GetAllResponsePojoBuilder {
    private List<DeskPojo> desks;
    private List<ChairPojo> chairs;
    private List<ConferenceDeskPojo> conferenceDesks;
    private List<ContainerPojo> containers;
    private List<CabinetShelfPojo> cabinetShelfs;
    private List<RuleEvalError> errors;

    public GetAllResponsePojoBuilder setDesks(List<DeskPojo> desks) {
        this.desks = desks;
        return this;
    }

    public GetAllResponsePojoBuilder setChairs(List<ChairPojo> chairs) {
        this.chairs = chairs;
        return this;
    }

    public GetAllResponsePojoBuilder setConferenceDesks(List<ConferenceDeskPojo> conferenceDesks) {
        this.conferenceDesks = conferenceDesks;
        return this;
    }

    public GetAllResponsePojoBuilder setContainers(List<ContainerPojo> containers) {
        this.containers = containers;
        return this;
    }

    public GetAllResponsePojoBuilder setCabinetShelfs(List<CabinetShelfPojo> cabinetShelfs) {
        this.cabinetShelfs = cabinetShelfs;
        return this;
    }

    public GetAllResponsePojoBuilder setErrors(List<RuleEvalError> errors) {
        this.errors = errors;
        return this;
    }

    public GetAllResponsePojo createGetAllResponsePojo() {
        return new GetAllResponsePojo(desks, chairs, conferenceDesks, cabinetShelfs, errors);
    }
}