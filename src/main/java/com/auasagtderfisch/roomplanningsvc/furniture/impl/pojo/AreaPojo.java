/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class AreaPojo {
    private final Point position;
    private final Point dimension;
    private final String areaType;

    public AreaPojo(Point position, Point dimension, String areaType) {
        this.position = position;
        this.dimension = dimension;
        this.areaType = areaType;
    }

    public Point getPosition() {
        return position;
    }

    public Point getDimension() {
        return dimension;
    }

    public String getAreaType() {
        return areaType;
    }
}
