/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class DeleteFurnitureResponsePojoBuilder {
    private String deletedName;
    private String deletedId;
    private List<RuleEvalError> errorMessages;

    public DeleteFurnitureResponsePojoBuilder setDeletedName(String deletedName) {
        this.deletedName = deletedName;
        return this;
    }

    public DeleteFurnitureResponsePojoBuilder setDeletedId(String deletedId) {
        this.deletedId = deletedId;
        return this;
    }

    public DeleteFurnitureResponsePojoBuilder setErrorMessages(List<RuleEvalError> errorMessages) {
        this.errorMessages = errorMessages;
        return this;
    }

    public DeleteFurnitureResponsePojo createDeleteFurnitureResponsePojo() {
        return new DeleteFurnitureResponsePojo(deletedName, deletedId, errorMessages);
    }
}