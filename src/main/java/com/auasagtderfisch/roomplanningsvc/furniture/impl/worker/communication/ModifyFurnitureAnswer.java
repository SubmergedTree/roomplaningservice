/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Area;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class ModifyFurnitureAnswer {
    private final List<RuleEvalError> error;
    private final List<Area> areas;

    public ModifyFurnitureAnswer(List<RuleEvalError> error, List<Area> areas) {
        this.error = error;
        this.areas = areas;
    }

    public List<RuleEvalError> getError() {
        return error;
    }

    public List<Area> getAreas() {
        return areas;
    }
}
