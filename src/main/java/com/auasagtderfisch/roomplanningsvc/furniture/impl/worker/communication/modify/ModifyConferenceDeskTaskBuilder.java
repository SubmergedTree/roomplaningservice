package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class ModifyConferenceDeskTaskBuilder {
    private boolean backsideIsEmpty;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public ModifyConferenceDeskTaskBuilder setBacksideIsEmpty(boolean backsideIsEmpty) {
        this.backsideIsEmpty = backsideIsEmpty;
        return this;
    }

    public ModifyConferenceDeskTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public ModifyConferenceDeskTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public ModifyConferenceDeskTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public ModifyConferenceDeskTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public ModifyConferenceDeskTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ModifyConferenceDeskTask createModifyConferenceDeskTask() {
        return new ModifyConferenceDeskTask(backsideIsEmpty, position, dimension, furnitureID, rotation, name);
    }
}