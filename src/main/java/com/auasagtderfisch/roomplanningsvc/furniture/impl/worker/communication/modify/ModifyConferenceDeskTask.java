package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class ModifyConferenceDeskTask extends FurnitureTask {
    private boolean backsideIsEmpty;
    private final Point position;
    private final Point dimension;
    private final String furnitureID;
    private final String name;
    private final double rotation;

    public ModifyConferenceDeskTask(boolean backsideIsEmpty, Point position, Point dimension, String furnitureID, double rotation, String name) {
        super(FurnitureTaskType.MODIFY_CONFERENCEDESK);
        this.backsideIsEmpty = backsideIsEmpty;
        this.position = position;
        this.dimension = dimension;
        this.furnitureID = furnitureID;
        this.rotation = rotation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getRotation() {
        return rotation;
    }

    public boolean isBacksideIsEmpty() {
        return backsideIsEmpty;
    }

    public Point getPosition() {
        return position;
    }

    public Point getDimension() {
        return dimension;
    }

    public String getFurnitureID() {
        return furnitureID;
    }
}
