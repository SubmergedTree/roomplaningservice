/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

public class DeleteGroupAnswer {
    private final String groupId;

    public DeleteGroupAnswer(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

}
