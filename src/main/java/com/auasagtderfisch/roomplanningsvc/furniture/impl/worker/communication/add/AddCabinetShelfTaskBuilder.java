package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class AddCabinetShelfTaskBuilder {
    private boolean isSeldomInUse;
    private boolean isOpen;
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;
    private double longestDrawer;

    public AddCabinetShelfTaskBuilder setIsSeldomInUse(boolean isSeldomInUse) {
        this.isSeldomInUse = isSeldomInUse;
        return this;
    }

    public AddCabinetShelfTaskBuilder setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
        return this;
    }

    public AddCabinetShelfTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public AddCabinetShelfTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public AddCabinetShelfTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public AddCabinetShelfTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public AddCabinetShelfTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AddCabinetShelfTaskBuilder setLongestDrawer(double longestDrawer) {
        this.longestDrawer = longestDrawer;
        return this;
    }

    public AddCabinetShelfTask createAddCabinetShelfTask() {
        return new AddCabinetShelfTask(isSeldomInUse, isOpen, position, dimension, furnitureID, rotation, name, longestDrawer);
    }
}