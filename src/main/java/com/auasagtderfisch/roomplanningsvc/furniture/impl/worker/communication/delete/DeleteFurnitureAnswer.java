/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class DeleteFurnitureAnswer {
    private final String deletedName;
    private final String deletedId;
    private final List<RuleEvalError> errorMessages;


    public DeleteFurnitureAnswer(String deletedName, String deletedId, List<RuleEvalError> errorMessages) {
        this.deletedName = deletedName;
        this.deletedId = deletedId;
        this.errorMessages = errorMessages;
    }

    public String getDeletedName() {
        return deletedName;
    }

    public String getDeletedId() {
        return deletedId;
    }

    public List<RuleEvalError> getErrorMessages() {
        return errorMessages;
    }

}
