/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class DeleteFurnitureResponsePojo {
    public String deletedName;
    public String deletedId;
    public List<RuleEvalError> errorMessages;

    public DeleteFurnitureResponsePojo(String deletedName, String deletedId, List<RuleEvalError> errorMessages) {
        this.deletedName = deletedName;
        this.deletedId = deletedId;
        this.errorMessages = errorMessages;
    }

    public String getDeletedName() {
        return deletedName;
    }

    public String getDeletedId() {
        return deletedId;
    }

    public List<RuleEvalError> getErrorMessages() {
        return errorMessages;
    }
}
