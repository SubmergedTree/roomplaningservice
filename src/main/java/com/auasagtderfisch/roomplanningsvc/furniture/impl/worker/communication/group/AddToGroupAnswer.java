/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

public class AddToGroupAnswer {
    private final String groupId;
    private final String furnitureId;

    public AddToGroupAnswer(String groupId, String furnitureId) {
        this.groupId = groupId;
        this.furnitureId = furnitureId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getFurnitureId() {
        return furnitureId;
    }
}
