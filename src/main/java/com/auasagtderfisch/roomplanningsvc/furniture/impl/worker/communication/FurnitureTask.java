/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication;

public class FurnitureTask {
    public enum FurnitureTaskType {
        ADD_DESK,
        MODIFY_DESK,
        DELETE_FURNITURE,
        GET_FURNITURE,
        ADD_CABINETSHELF,
        ADD_CONFERENCEDESK,
        ADD_CHAIR,
        MODIFY_CABINETSHELF,
        MODIFY_CHAIR,
        MODIFY_CONFERENCEDESK,

        CREATE_GROUP,
        DELETE_GROUP,
        ADD_TO_GROUP,
        DELETE_FROM_GROUP,
        GET_GROUPS
    }

    private final FurnitureTaskType furnitureTaskType;

    public FurnitureTask(FurnitureTaskType furnitureTaskType) {
        this.furnitureTaskType = furnitureTaskType;
    }

    @SuppressWarnings("unchecked")
    public <T extends FurnitureTask> T get() {
        return (T) this;
    }

    public FurnitureTaskType getType() {
        return furnitureTaskType;
    }
}
