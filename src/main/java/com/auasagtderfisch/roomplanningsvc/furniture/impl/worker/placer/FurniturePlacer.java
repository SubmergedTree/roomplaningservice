/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.placer;

import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractWorker;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.*;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.FurnitureFactory;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.GetFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.GetFurnitureAnswerBuilder;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.ModifyFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add.AddCabinetShelfTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add.AddChairTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add.AddConferenceDeskTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add.AddDeskTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete.DeleteFurniture;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.delete.DeleteFurnitureAnswer;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify.ModifyCabinetShelfTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify.ModifyChairTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify.ModifyConferenceDeskTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.modify.ModifyDeskTask;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group.*;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class FurniturePlacer extends AbstractWorker {

    private final FurnitureFactory furnitureFactory;

    public FurniturePlacer(String listenTo, Blackboard store, CAD2DObjectFactory cad2dObjectFactory,
                           FurnitureFactory furnitureFactory, LocalizationService localizationService) {
        super(store, cad2dObjectFactory,localizationService, listenTo);
        this.furnitureFactory = furnitureFactory;
    }

    @Override
    protected Optional<Answer> onTaskReceived(Task task) {
        var furnitureTask = (FurnitureTask)task.getTask();
        switch (furnitureTask.getType()) {
            case ADD_DESK:
                AddDeskTask addDeskTask =  furnitureTask.get();
                ModifyFurnitureAnswer modifyFurnitureAnswer = addDesk(addDeskTask);
                return Optional.of(new Answer(modifyFurnitureAnswer));
            case MODIFY_DESK:
                ModifyDeskTask modifyDeskTask = furnitureTask.get();
                return Optional.of(modifyDesk(modifyDeskTask));
            case ADD_CABINETSHELF:
                AddCabinetShelfTask addCabinetShelfTask = furnitureTask.get();
                ModifyFurnitureAnswer modifyCabinetShelfAnswer = addCabinetShelf(addCabinetShelfTask);
                return Optional.of(new Answer(modifyCabinetShelfAnswer));
            case MODIFY_CABINETSHELF:
                ModifyCabinetShelfTask modifyCabinetShelfTask  = furnitureTask.get();
                return Optional.of(modifyCabinetShelf(modifyCabinetShelfTask));
            case ADD_CHAIR:
                AddChairTask addChairTask = furnitureTask.get();
                ModifyFurnitureAnswer modifyChairAnswer = addChair(addChairTask);
                return Optional.of(new Answer(modifyChairAnswer));
            case MODIFY_CHAIR:
                ModifyChairTask modifyChairTask  = furnitureTask.get();
                return Optional.of(modifyChair(modifyChairTask));
            case ADD_CONFERENCEDESK:
                AddConferenceDeskTask addConferenceDeskTask = furnitureTask.get();
                ModifyFurnitureAnswer modifyConferenceDeskAnswer = addConferenceDesk(addConferenceDeskTask);
                return Optional.of(new Answer(modifyConferenceDeskAnswer));
            case MODIFY_CONFERENCEDESK:
                ModifyConferenceDeskTask modifyConferenceDeskTask  = furnitureTask.get();
                return Optional.of(modifyConferenceDesk(modifyConferenceDeskTask));
            case GET_FURNITURE:
                return Optional.of(getFurniture());
            case DELETE_FURNITURE:
                DeleteFurniture deleteFurnitureTask = furnitureTask.get();
                return Optional.of(deleteFurniture(deleteFurnitureTask));
            case GET_GROUPS:
                return Optional.of(getGroups(furnitureTask.get()));
            case ADD_TO_GROUP:
                return Optional.of(addToGroup(furnitureTask.get()));
            case CREATE_GROUP:
                return Optional.of(createGroup(furnitureTask.get()));
            case DELETE_FROM_GROUP:
                return Optional.of(deleteFromGroup(furnitureTask.get()));
            case DELETE_GROUP:
                return Optional.of(deleteGroup(furnitureTask.get()));
            default:
                return Optional.empty();
        }
    }

    private Answer getFurniture() {
        Room room = store.get();
        List<Chair> chairs = room.getAllByType(Chair.class);
        List<CabinetShelf> cabinetShelfs = room.getAllByType(CabinetShelf.class);
        List<Desk> desks = room.getAllByType(Desk.class);
        List<ConferenceDesk> conferenceDesks = room.getAllByType(ConferenceDesk.class);
        List<Container> containers = room.getAllByType(Container.class);

        GetFurnitureAnswer furnitureAnswer =  new GetFurnitureAnswerBuilder()
                .setCabinetShelfs(cabinetShelfs)
                .setConferenceDesks(conferenceDesks)
                .setChairs(chairs)
                .setContainers(containers)
                .setDesks(desks)
                .setError(room.checkErrors())
                .createGetFurnitureAnswer();
        return new Answer(furnitureAnswer);
    }

    private ModifyFurnitureAnswer addDesk(AddDeskTask addDeskTask) {
        Room room = store.get();
        Desk desk = furnitureFactory.createDesk(addDeskTask.getDimension(),
                addDeskTask.getPosition(),
                addDeskTask.getRotation());
        desk.setName(addDeskTask.getName());
        desk.setId(UUID.fromString(addDeskTask.getFurnitureID()));
        desk.setStandingNotUpright(addDeskTask.isStandingNotUpright());
        desk.setWheelchairAccessible(addDeskTask.isWheelchairAccessible());
        desk.setWheelchairCompatible(addDeskTask.isWheelchairCompatible());
        room.addFurniture(desk);

        List<RuleEvalError> errors = room.checkErrors();
        return new ModifyFurnitureAnswer(errors, desk.getAreas());
    }

    private Answer modifyDesk(ModifyDeskTask modifyDeskTask) {
        Room room = store.get();
        UUID id = UUID.fromString(modifyDeskTask.getFurnitureID());
        Optional<Desk> deskOptional = room.getFurnitureByType(id, Desk.class);
        if (deskOptional.isPresent()) {
            Desk desk = deskOptional.get();
            updateDesk(desk, modifyDeskTask);
            List<RuleEvalError> errors = room.checkErrors();
            return new Answer(new ModifyFurnitureAnswer(errors, desk.getAreas()));
        }
        return new Answer(new FurnitureNotFoundException(localizationService.getForKey("deskNotFound")));
    }

    private void updateDesk(Desk desk, ModifyDeskTask modifyDeskTask) {
        desk.setPosition(modifyDeskTask.getPosition());
        // dimension is not editable
        desk.rotate(modifyDeskTask.getRotation());
        desk.setStandingNotUpright(modifyDeskTask.isStandingNotUpright());
        desk.setWheelchairAccessible(modifyDeskTask.isWheelchairAccessible());
        desk.setWheelchairCompatible(modifyDeskTask.isWheelchairCompatible());
        desk.setName(modifyDeskTask.getName());
    }

    private ModifyFurnitureAnswer addCabinetShelf(AddCabinetShelfTask addCabinetShelfTask) {
        Room room = store.get();
        CabinetShelf cabinetShelf = furnitureFactory.createCabinetShelf(addCabinetShelfTask.isSeldomInUse(),
                addCabinetShelfTask.isOpen(),
                addCabinetShelfTask.getDimension(),
                addCabinetShelfTask.getPosition(),
                addCabinetShelfTask.getRotation(),
                addCabinetShelfTask.getLongestDrawer());
        cabinetShelf.setName(addCabinetShelfTask.getName());
        cabinetShelf.setId(UUID.fromString(addCabinetShelfTask.getFurnitureID()));
        room.addFurniture(cabinetShelf);

        List<RuleEvalError> errors = room.checkErrors();
        return new ModifyFurnitureAnswer(errors, cabinetShelf.getAreas());
    }

    private Answer modifyCabinetShelf(ModifyCabinetShelfTask modifyCabinetShelfTask) {
        Room room = store.get();
        UUID id = UUID.fromString(modifyCabinetShelfTask.getFurnitureID());
        Optional<CabinetShelf> cabinetShelfOptional = room.getFurnitureByType(id, CabinetShelf.class);
        if (cabinetShelfOptional.isPresent()) {
            CabinetShelf cabinetShelf = cabinetShelfOptional.get();
            updateCabinetShelf(cabinetShelf, modifyCabinetShelfTask);
            List<RuleEvalError> errors = room.checkErrors();
            return new Answer(new ModifyFurnitureAnswer(errors, cabinetShelf.getAreas()));
        }
        return new Answer(new FurnitureNotFoundException(localizationService.getForKey("cabinetShelfNotFound")));
    }

    private void updateCabinetShelf(CabinetShelf cabinetShelf, ModifyCabinetShelfTask modifyCabinetShelfTask) {
        cabinetShelf.setPosition(modifyCabinetShelfTask.getPosition());
        // dimension is not editable
        cabinetShelf.rotate(modifyCabinetShelfTask.getRotation());
        cabinetShelf.setIsSeldomInUse(modifyCabinetShelfTask.isSeldomInUse());
        cabinetShelf.setOpen(modifyCabinetShelfTask.isOpen());
        cabinetShelf.setLongestDrawer(modifyCabinetShelfTask.getLongestDrawer());
        cabinetShelf.setName(modifyCabinetShelfTask.getName());
    }

    private ModifyFurnitureAnswer addChair(AddChairTask addChairTask) {
        Room room = store.get();
        Chair chair = furnitureFactory.createChair(addChairTask.getDimension(),
                addChairTask.getPosition(),
                addChairTask.getRotation());
        chair.setName(addChairTask.getName());
        chair.setId(UUID.fromString(addChairTask.getFurnitureID()));
        room.addFurniture(chair);

        List<RuleEvalError> errors = room.checkErrors();
        return new ModifyFurnitureAnswer(errors, chair.getAreas());
    }

    private Answer modifyChair(ModifyChairTask modifyChairTask) {
        Room room = store.get();
        UUID id = UUID.fromString(modifyChairTask.getFurnitureID());
        Optional<Chair> chairOptional = room.getFurnitureByType(id, Chair.class);
        if (chairOptional.isPresent()) {
            Chair chair = chairOptional.get();
            updateChair(chair, modifyChairTask);
            List<RuleEvalError> errors = room.checkErrors();
            return new Answer(new ModifyFurnitureAnswer(errors, chair.getAreas()));
        }
        return new Answer(new FurnitureNotFoundException(localizationService.getForKey("chairNotFound")));
    }

    private void updateChair(Chair chair, ModifyChairTask modifyChairTask) {
        chair.setPosition(modifyChairTask.getPosition());
        // dimension is not editable
        chair.rotate(modifyChairTask.getRotation());
        chair.setName(modifyChairTask.getName());
    }

    private ModifyFurnitureAnswer addConferenceDesk(AddConferenceDeskTask addConferenceDeskTask) {
        Room room = store.get();
        ConferenceDesk conferenceDesk = furnitureFactory.createConferenceDesk(addConferenceDeskTask.isBacksideIsEmpty(),
                addConferenceDeskTask.getDimension(),
                addConferenceDeskTask.getPosition(),
                addConferenceDeskTask.getRotation());
        conferenceDesk.setName(addConferenceDeskTask.getName());
        conferenceDesk.setId(UUID.fromString(addConferenceDeskTask.getFurnitureID()));
        room.addFurniture(conferenceDesk);

        List<RuleEvalError> errors = room.checkErrors();
        return new ModifyFurnitureAnswer(errors, conferenceDesk.getAreas());
    }

    private Answer modifyConferenceDesk(ModifyConferenceDeskTask modifyConferenceDeskTask) {
        Room room = store.get();
        UUID id = UUID.fromString(modifyConferenceDeskTask.getFurnitureID());
        Optional<ConferenceDesk> conferenceDeskOptional = room.getFurnitureByType(id, ConferenceDesk.class);
        if (conferenceDeskOptional.isPresent()) {
            ConferenceDesk conferenceDesk = conferenceDeskOptional.get();
            updateConferenceDesk(conferenceDesk, modifyConferenceDeskTask);
            List<RuleEvalError> errors = room.checkErrors();
            return new Answer(new ModifyFurnitureAnswer(errors, conferenceDesk.getAreas()));
        }
        return new Answer(new FurnitureNotFoundException(localizationService.getForKey("conferenceDeskNotFound")));
    }

    private void updateConferenceDesk(ConferenceDesk conferenceDesk, ModifyConferenceDeskTask modifyConferenceDeskTask) {
        conferenceDesk.setPosition(modifyConferenceDeskTask.getPosition());
        // dimension is not editable
        conferenceDesk.rotate(modifyConferenceDeskTask.getRotation());
        conferenceDesk.setName(modifyConferenceDeskTask.getName());
        conferenceDesk.setBacksideIsEmpty(modifyConferenceDeskTask.isBacksideIsEmpty());
    }

    private Answer deleteFurniture(DeleteFurniture deleteFurnitureTask) {
        Room room = store.get();
        if (room.deleteFurniture(UUID.fromString(deleteFurnitureTask.getFurnitureID()))) {
            var errors = room.checkErrors();
            var deleteFurnitureAnswer = new DeleteFurnitureAnswer("NA",
                    deleteFurnitureTask.getFurnitureID(),
                    errors);
            return new Answer(deleteFurnitureAnswer);
        }
        return new Answer(new FurnitureNotFoundException(localizationService.getForKey("unknownFurniture")));
    }

    private Answer getGroups(GetGroupsTask getGroupsTask) {
        Room room = store.get();
        var groupMap = room.getGroupIdFurnitureIdMap();
        return new Answer(new GetGroupsAnswer(groupMap));
    }

    private Answer createGroup(CreateGroupTask createGroupTask) {
        Room room = store.get();
        String groupId =  room.createGroup(createGroupTask.getGroupName());
        return new Answer(new CreateGroupAnswer(groupId));
    }

    private Answer deleteGroup(DeleteGroupTask deleteGroupTask) {
        Room room = store.get();
        if (room.deleteGroup(deleteGroupTask.getGroupId())) {
            return new Answer(new DeleteGroupAnswer(deleteGroupTask.getGroupId()));
        }
        return new Answer(new GroupNotFoundException(localizationService.getForKey("unknownGroup")));

    }

    private Answer addToGroup(AddToGroupTask addToGroupTask) {
        Room room = store.get();
        var err = room.addToGroup(addToGroupTask.getGroupId(), addToGroupTask.getFurnitureId());
        if (err != null) {
            return new Answer(err);
        }
        return new Answer(new AddToGroupAnswer(addToGroupTask.getGroupId(), addToGroupTask.getFurnitureId()));
    }

    private Answer deleteFromGroup(DeleteFromGroupTask deleteFromGroupTask) {
        Room room = store.get();
        var err = room.deleteFromGroup(deleteFromGroupTask.getGroupId(), deleteFromGroupTask.getFurnitureId());
        if (err != null) {
            return new Answer(err);
        }
        return new Answer(new DeleteFromGroupAnswer(deleteFromGroupTask.getGroupId(),
                deleteFromGroupTask.getFurnitureId()));
    }
}
