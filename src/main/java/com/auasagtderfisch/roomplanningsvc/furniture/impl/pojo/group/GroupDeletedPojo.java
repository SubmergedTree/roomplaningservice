/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group;

public class GroupDeletedPojo {
    private final String deletedGroup;

    public GroupDeletedPojo(String groupId) {
        this.deletedGroup = groupId;
    }

    public String getDeletedGroup() {
        return deletedGroup;
    }
}
