/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.FurnitureTask;

public class DeleteFromGroupTask extends FurnitureTask{
    private final String furnitureId;
    private final String groupId;

    public DeleteFromGroupTask(String furnitureId, String groupId) {
        super(FurnitureTaskType.DELETE_FROM_GROUP);
        this.furnitureId = furnitureId;
        this.groupId = groupId;
    }

    public String getFurnitureId() {
        return furnitureId;
    }

    public String getGroupId() {
        return groupId;
    }
}
