/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication;

public class GetFurnitureTask extends FurnitureTask {
    public GetFurnitureTask() {
        super(FurnitureTaskType.GET_FURNITURE);
    }
}
