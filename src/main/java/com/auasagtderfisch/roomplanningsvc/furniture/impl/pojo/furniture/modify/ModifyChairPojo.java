/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.modify;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.BaseFurniture;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class ModifyChairPojo extends BaseFurniture {
    @NotNull
    public Map<String, String> properties;

}
