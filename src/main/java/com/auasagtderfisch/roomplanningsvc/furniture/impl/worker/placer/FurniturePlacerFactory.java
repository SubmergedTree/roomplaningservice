/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.placer;

import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractWorker;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.FurnitureFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.WallFactory;
import com.auasagtderfisch.roomplanningsvc.util.function.Function2;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FurniturePlacerFactory {
    private final String callCmd = "#FurnitureWorker";

    private final CAD2DObjectFactory cad2dObjectFactory;
    private final FurnitureFactory furnitureFactory;
    private final LocalizationService localizationService;

    @Autowired
    public FurniturePlacerFactory(CAD2DObjectFactory cad2dObjectFactory,
                                  FurnitureFactory furnitureFactory, LocalizationService localizationService) {
        this.cad2dObjectFactory = cad2dObjectFactory;
        this.furnitureFactory = furnitureFactory;
        this.localizationService = localizationService;
    }

    @Bean
    public Function2<String, Blackboard, FurniturePlacer> furniturePlacerWorker() {
        return (String modelUUID, Blackboard store) ->
                new FurniturePlacer(AbstractWorker.getListenTo(modelUUID, callCmd), store,
                        cad2dObjectFactory, furnitureFactory, localizationService);
    }

}
