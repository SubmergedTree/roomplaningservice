/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class BaseFurnitureWithDimensionPojo extends BaseFurniture{

    @NotNull
    public Point dimension;

    public BaseFurnitureWithDimensionPojo() {
    }

    public Point getDimension() {
        return dimension;
    }
}
