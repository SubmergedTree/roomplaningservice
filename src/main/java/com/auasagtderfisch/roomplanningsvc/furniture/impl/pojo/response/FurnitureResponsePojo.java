/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.response;


import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.AreaPojo;

import java.util.List;

public class FurnitureResponsePojo {
    private List<RuleEvalError> error;
    private String furnitureId;
    public List<AreaPojo> areas;

    public FurnitureResponsePojo(List<RuleEvalError> errorMessages, String furnitureId, List<AreaPojo> areas) {
        this.error = errorMessages;
        this.furnitureId = furnitureId;
        this.areas = areas;
    }

    public List<RuleEvalError> getErrorMessages() {
        return error;
    }

    public String getFurnitureId() {
        return furnitureId;
    }

    public List<AreaPojo> getAreas() {
        return areas;
    }
}
