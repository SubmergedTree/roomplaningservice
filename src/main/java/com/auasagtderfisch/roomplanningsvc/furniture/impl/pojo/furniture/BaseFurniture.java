/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class BaseFurniture {

    @NotNull
    public Point position;

    @NotNull
    @Range(min = 0, max = 360)
    public double angle;

    @NotNull
    @NotBlank
    public String name;

    public BaseFurniture() {
    }

    public Point getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }

    public String getName() {
        return name;
    }

}
