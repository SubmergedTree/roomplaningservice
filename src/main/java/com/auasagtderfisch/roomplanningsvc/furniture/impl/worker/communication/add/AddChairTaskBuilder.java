package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.add;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

public class AddChairTaskBuilder {
    private Point position;
    private Point dimension;
    private String furnitureID;
    private double rotation;
    private String name;

    public AddChairTaskBuilder setPosition(Point position) {
        this.position = position;
        return this;
    }

    public AddChairTaskBuilder setDimension(Point dimension) {
        this.dimension = dimension;
        return this;
    }

    public AddChairTaskBuilder setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
        return this;
    }

    public AddChairTaskBuilder setRotation(double rotation) {
        this.rotation = rotation;
        return this;
    }

    public AddChairTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AddChairTask createAddChairTask() {
        return new AddChairTask(position, dimension, furnitureID, rotation, name);
    }
}