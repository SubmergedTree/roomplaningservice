/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.furniture.properties;

import javax.validation.constraints.NotNull;

public class CabinetShelfProperties {
    @NotNull
    public double longestDrawer;
    @NotNull
    public boolean isSeldomInUse;
    @NotNull
    public boolean isOpen;
}
