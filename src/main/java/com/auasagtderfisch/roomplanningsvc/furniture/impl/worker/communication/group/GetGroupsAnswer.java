/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl.worker.communication.group;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Group;
import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group.GroupPojo;

import java.util.List;
import java.util.Map;

public class GetGroupsAnswer {
    private final Map<String, Group> groups;

    public GetGroupsAnswer(Map<String, Group> groups) {
        this.groups = groups;
    }

    public Map<String, Group> getGroups() {
        return groups;
    }
}
