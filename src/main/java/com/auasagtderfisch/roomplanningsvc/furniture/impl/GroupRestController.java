/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture.impl;

import com.auasagtderfisch.roomplanningsvc.furniture.GroupService;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.RestUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GroupRestController implements VertxRestController {

    private final static String SESSION_ID_KEY = "sessionId";
    private final static String FURNITURE_ID_KEY = "furnitureId";
    private final static String GROUP_ID_KEY = "groupId";
    private final static String GROUP_NAME = "name";

    private final GroupService groupService;

    @Autowired
    public GroupRestController(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public Router setupRoutes(Vertx vertx) {
        Router subRouter = Router.router(vertx);
        subRouter.route("/createGroup*").handler(BodyHandler.create());
        subRouter.post("/createGroup").handler(this::createGroup);

        subRouter.route("/deleteGroup*").handler(BodyHandler.create());
        subRouter.delete("/deleteGroup").handler(this::deleteGroup);

        subRouter.route("/getAllGroups*").handler(BodyHandler.create());
        subRouter.get("/getAllGroups").handler(this::getAllGroups);

        subRouter.route("/addToGroup*").handler(BodyHandler.create());
        subRouter.post("/addToGroup").handler(this::addToGroup);

        subRouter.route("/deleteFromGroup*").handler(BodyHandler.create());
        subRouter.delete("/deleteFromGroup").handler(this::deleteFromGroup);

        return subRouter;
    }

    @Override
    public String getSubRoute() {
        return "/furniture/group";
    }

    private void createGroup(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var name = RestUtil.extractParamString(routingContext.request(), GROUP_NAME);
        sessionId.flatMap(h -> groupService.createGroup(sessionId.result(), name.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void deleteGroup(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var groupId = RestUtil.extractParamString(routingContext.request(), GROUP_ID_KEY);
        CompositeFuture.all(sessionId, groupId)
                .flatMap(h -> groupService.deleteGroup(sessionId.result(), groupId.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void getAllGroups(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        sessionId.flatMap(groupService::getGroups)
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void addToGroup(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var groupId = RestUtil.extractParamString(routingContext.request(), GROUP_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        CompositeFuture.all(sessionId, groupId, furnitureId)
                .flatMap(h -> groupService.addToGroup(sessionId.result(),
                        groupId.result(),
                        furnitureId.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void deleteFromGroup(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var groupId = RestUtil.extractParamString(routingContext.request(), GROUP_ID_KEY);
        var furnitureId = RestUtil.extractParamString(routingContext.request(), FURNITURE_ID_KEY);
        CompositeFuture.all(sessionId, groupId, furnitureId)
                .flatMap(h -> groupService.deleteFromGroup(sessionId.result(),
                        groupId.result(),
                        furnitureId.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

}
