/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.furniture;

import com.auasagtderfisch.roomplanningsvc.furniture.impl.pojo.group.*;
import io.vertx.core.Future;

public interface GroupService {
    Future<NewGroupPojo> createGroup(String sessionId, String groupName);
    Future<GroupDeletedPojo> deleteGroup(String sessionId, String groupId);

    Future<AddToGroupPojo> addToGroup(String sessionId, String groupId, String furnitureId);
    Future<DeletedFromGroupPojo> deleteFromGroup(String sessionId, String groupId, String furnitureId);

    Future<GroupsPojo> getGroups(String sessionId);
}
