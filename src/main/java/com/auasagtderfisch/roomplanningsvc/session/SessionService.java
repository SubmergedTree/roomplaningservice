/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.session.impl.AllProjectsPojo;
import io.vertx.core.Future;

import java.util.UUID;

public interface SessionService {

    Future<UUID> createProject(String projectName, String ruleSet, String userName);
    Future<UUID> loadProject(String projectName, String userName);

    // because of prototype we don't implement delete sessions

    Future<BlackboardService> getBlackboardForSession(String sessionId);

    Future<AllProjectsPojo> getAllProjects();
}
