/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

public class CreateProjectResultPojo {
    public String sessionId;

    public CreateProjectResultPojo(String sessionId) {
        this.sessionId = sessionId;
    }

    public CreateProjectResultPojo() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
