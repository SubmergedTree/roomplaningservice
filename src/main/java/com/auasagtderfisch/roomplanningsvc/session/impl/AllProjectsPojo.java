/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

import java.util.List;

public class AllProjectsPojo {
    private final List<String> projectNames;

    public AllProjectsPojo(List<String> projectNames) {
        this.projectNames = projectNames;
    }

    public List<String> getProjectNames() {
        return projectNames;
    }
}
