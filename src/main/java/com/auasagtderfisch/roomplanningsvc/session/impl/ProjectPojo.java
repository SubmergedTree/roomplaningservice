/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProjectPojo {

    @NotNull
    @NotBlank
    public String projectName;

    @NotNull
    @NotBlank
    public String userName;

    public ProjectPojo() {
    }

    public ProjectPojo(@NotNull @NotBlank String projectName, @NotNull @NotBlank String userName) {
        this.projectName = projectName;
        this.userName = userName;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getUserName() {
        return userName;
    }
}
