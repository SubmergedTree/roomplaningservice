/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardFactory;
import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.persistence.LoaderService;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import io.vertx.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class InMemorySessionServiceVerticle extends AbstractVerticle {
    public static final String CREATE_PROJECT = "CREATE_PROJECT";
    public static final String LOAD_PROJECT = "LOAD_PROJECT";
    public static final String GET_BLACKBOARD_SERVICE = "GET_BLACKBOARD_SERVICE";
    public static final Integer CREATE_PROJECT_ERROR_CODE = -1;
    public static final Integer GET_BLACKBOARD_SERVICE_ERROR_CODE = -2;
    public static final Integer LOAD_PROJECT_ERROR_CODE = -1;

    private final Logger logger = LoggerFactory.getLogger(InMemorySessionServiceVerticle.class);

    // rule set -> Loader service
    private final Map<String, LoaderService> loaderServices;
    // session -> blackboard service
    private final Map<String, BlackboardService> blackboardServiceMap;
    // project name -> blackboad service
    private final Map<String, BlackboardService> projectNameBlackboardMap;
    // projectName -> last time session id requested
    private final Map<String, Long> lastRequestMap;
    // session -> projectName
    private final Map<String, String> sessionToProject;

    private final BlackboardFactory blackboardFactory;
    private final LocalizationService localizationService;

    @Autowired
    public InMemorySessionServiceVerticle(List<LoaderService> loaderServices,
                                          BlackboardFactory blackboardFactory,
                                          LocalizationService localizationService) {
        this.loaderServices = new LinkedHashMap<>();
        this.blackboardServiceMap = new LinkedHashMap<>();
        this.projectNameBlackboardMap = new LinkedHashMap<>();
        this.lastRequestMap = new LinkedHashMap<>();
        this.sessionToProject = new LinkedHashMap<>();
        this.blackboardFactory = blackboardFactory;
        this.localizationService = localizationService;
        insertLoaderServices(loaderServices);
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        listen();
        deleteBlackboardJob();
        logger.info("InMemorySessionService started");
        startPromise.complete();
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        logger.info("InMemorySessionService closed");
        stopPromise.complete();
    }

    private void listen() {
        vertx.eventBus().consumer(CREATE_PROJECT).handler(h -> {
            InMemorySessionServicePojo pojo = (InMemorySessionServicePojo)h.body();
            createProject(pojo.projectName, pojo.ruleSet, pojo.userName).onComplete(cph -> {
                if (cph.succeeded())
                    h.reply(cph.result());
                else
                    h.fail(CREATE_PROJECT_ERROR_CODE, cph.cause().getMessage());
            });
        });

        vertx.eventBus().consumer(LOAD_PROJECT).handler(h -> {
            InMemorySessionServicePojo pojo = (InMemorySessionServicePojo)h.body();
            loadProject(pojo.projectName).onComplete(cph -> {
                if (cph.succeeded())
                    h.reply(cph.result());
                else
                    h.fail(LOAD_PROJECT_ERROR_CODE, cph.cause().getMessage());
            });
        });

        vertx.eventBus().consumer(GET_BLACKBOARD_SERVICE).handler(h -> {
            String sessionId = (String)h.body();
            getBlackboardServiceForSessionId(sessionId)
                    .onSuccess(h::reply)
                    .onFailure(failureHandler -> {
                        h.fail(GET_BLACKBOARD_SERVICE_ERROR_CODE, localizationService.getForKey("blackboardNotFound"));
                    });
        });
    }

    private void deleteBlackboardJob() {
        // each 20 minutes
        // is this thread safe or should we use events ?
        vertx.setPeriodic(1000 * 60 * 20, l -> {
            logger.info("run delete abandoned sessions");
            Date date = new Date();
            long currentTimestamp = date.getTime();

            Iterator<Map.Entry<String,Long>> iter = lastRequestMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String,Long> entry = iter.next();
                if (entry.getValue() < currentTimestamp) {
                    deleteBlackboard(entry.getKey());
                    iter.remove();
                }
            }
        });
    }

    private void deleteBlackboard(String projectName) {
        List<String> sessions = getSessionsForProjectName(projectName);
        sessions.forEach(s -> logger.info("delete session: " + s));
        sessions.forEach(blackboardServiceMap::remove);
        BlackboardService toKill = projectNameBlackboardMap.remove(projectName);
        toKill.kill(); // safe model first
        logger.info("Blackboard killed for project: " + projectName);
    }

    private List<String> getSessionsForProjectName(String projectName) {
        List<String> sessions = new ArrayList<>();

        Iterator<Map.Entry<String,String>> iter = sessionToProject.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String,String> entry = iter.next();
            if (entry.getValue().equals(projectName)) {
                sessions.add(entry.getKey());
                iter.remove();
            }
        }
        return sessions;
    }

    private Future<UUID> createProject(String projectName, String ruleSet, String userName) {
        if (this.loaderServices.get(ruleSet) == null) {
            logger.info("Unknown ruleset: " + ruleSet);
            return Future.failedFuture("No fitting rule set found");
        } else {
            LoaderService loaderService = loaderServices.get(ruleSet);
            return loaderService.getStoredProjectNames()
                    .flatMap(projectNames -> {
                        if (projectNames.contains(projectName)) {
                            logger.info("project with name : " + projectName + " does already exists");
                            return Future.failedFuture(localizationService.getForKey("projectNameAlreadyAllocated", projectName));
                        }
                        UUID sessionId = UUID.randomUUID();
                        UUID modelUUID = UUID.randomUUID();
                        return loaderService.loadNew(projectName)
                                .flatMap(model ->  createBlackboard(modelUUID, sessionId, projectName, model))
                                .map(h -> {
                                    sessionToProject.put(sessionId.toString(), projectName);
                                    logger.info("created blackboard, sessionId: " + sessionId + " for project: " + projectName);
                                    updateLastRequest(projectName);
                                    return sessionId;
                                });
                    });
        }
    }

    private Future<UUID> loadProject(String projectName) {
        if (this.loaderServices.get("DGUV16") == null) {
            logger.info("Unknown ruleset");
            return Future.failedFuture("No fitting rule set found");
        } else {
            UUID sessionId = UUID.randomUUID();
            if(projectNameBlackboardMap.containsKey(projectName)) {
                logger.info("blackboard found, sessionId: " + sessionId + " for project: " + projectName);
                sessionToProject.put(sessionId.toString(), projectName);
                blackboardServiceMap.put(sessionId.toString(), projectNameBlackboardMap.get(projectName));
                updateLastRequest(projectName);
                return Future.succeededFuture(sessionId);
            }

            UUID modelUUID = UUID.randomUUID();
            return loaderServices.get("DGUV16").load(projectName)
                    .flatMap(model ->  createBlackboard(modelUUID, sessionId, projectName, model))
                    .map(h -> {
                        sessionToProject.put(sessionId.toString(), projectName);
                        logger.info("created blackboard, sessionId: " + sessionId + " for project: " + projectName);
                        updateLastRequest(projectName);
                        return sessionId;
                    });
        }
    }

    private Future<Void> createBlackboard(UUID modelUUID, UUID sessionUUID, String projectName, Object model) {
        return blackboardFactory.createBlackboardService(modelUUID, model)
                .flatMap(blackboardService -> {
                    blackboardServiceMap.put(sessionUUID.toString(), blackboardService);
                    projectNameBlackboardMap.put(projectName, blackboardService);
                    return Future.succeededFuture();
                });
    }

    private Future<BlackboardService> getBlackboardServiceForSessionId(String sessionID) {
        if (blackboardServiceMap.containsKey(sessionID)) {
            String projectName = sessionToProject.get(sessionID);
            updateLastRequest(projectName);
            return Future.succeededFuture(blackboardServiceMap.get(sessionID));
        }
        return Future.failedFuture(localizationService.getForKey("blackboardNotFound"));
    }

    private void insertLoaderServices(List<LoaderService> loaderServices) {
        for(LoaderService ls : loaderServices) {
            this.loaderServices.put(ls.forWhat(), ls);
        }
    }

    private void updateLastRequest(String projectName) {
        Date date = new Date();
        long timeToDelete = date.getTime() + 1000*60*20;
        lastRequestMap.put(projectName, timeToDelete);
    }

}
