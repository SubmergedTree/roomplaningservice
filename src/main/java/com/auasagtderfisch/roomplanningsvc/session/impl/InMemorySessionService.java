/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.BlackboardService;
import com.auasagtderfisch.roomplanningsvc.blackboard.impl.BlackboardVertxService;
import com.auasagtderfisch.roomplanningsvc.persistence.LoaderService;
import com.auasagtderfisch.roomplanningsvc.session.SessionService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class InMemorySessionService implements SessionService {

    private final EventBus eventBus;
    private final List<LoaderService> loaderServices;

    @Autowired
    public InMemorySessionService(Vertx vertx, List<LoaderService> loaderServices) {
        this.eventBus = vertx.eventBus();
        this.loaderServices  = loaderServices;
    }

    @Override
    public Future<UUID> createProject(String projectName, String ruleSet, String userName) {
        InMemorySessionServicePojo inMemorySessionServicePojo = new InMemorySessionServicePojo(projectName, ruleSet, userName);
        return Future.future(futureHandler -> {
            eventBus.request(InMemorySessionServiceVerticle.CREATE_PROJECT, inMemorySessionServicePojo, eventHandler -> {
                        if (eventHandler.succeeded())
                            futureHandler.handle(Future.succeededFuture((UUID) eventHandler.result().body()));
                        else
                            futureHandler.handle(Future.failedFuture(eventHandler.cause()));
                    });
        });
    }

    @Override
    public Future<UUID> loadProject(String projectName, String userName) {
        InMemorySessionServicePojo inMemorySessionServicePojo = new InMemorySessionServicePojo(projectName, "DGUV16", userName);
        return Future.future(futureHandler -> {
            eventBus.request(InMemorySessionServiceVerticle.LOAD_PROJECT, inMemorySessionServicePojo, eventHandler -> {
                if (eventHandler.succeeded())
                    futureHandler.handle(Future.succeededFuture((UUID) eventHandler.result().body()));
                else
                    futureHandler.handle(Future.failedFuture(eventHandler.cause()));
            });
        });
    }

    @Override
    public Future<BlackboardService> getBlackboardForSession(String sessionId) {
        return Future.future(futureHandler -> {
            eventBus.request(InMemorySessionServiceVerticle.GET_BLACKBOARD_SERVICE, sessionId, eventHandler -> {
                if(eventHandler.succeeded()) {
                    BlackboardService blackboardService = (BlackboardVertxService)eventHandler.result().body();
                    futureHandler.handle(Future.succeededFuture(blackboardService));
                } else {
                    futureHandler.handle(Future.failedFuture(eventHandler.cause()));
                }
            });
        });
    }

    @Override
    public Future<AllProjectsPojo> getAllProjects() {
        LoaderService loaderService = loaderServices.get(0);
       return loaderService.getStoredProjectNames()
               .map(nameSet -> {
                   List<String> nameList = new ArrayList<String>(nameSet);
                   return new AllProjectsPojo(nameList);
                });
    }
}
