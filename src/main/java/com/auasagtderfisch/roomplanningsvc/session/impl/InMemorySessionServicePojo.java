/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

public class InMemorySessionServicePojo {
    String projectName;
    String ruleSet;
    String userName;

    public InMemorySessionServicePojo(String projectName, String ruleSet, String userName) {
        this.projectName = projectName;
        this.ruleSet = ruleSet;
        this.userName = userName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setRuleSet(String ruleSet) {
        this.ruleSet = ruleSet;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
