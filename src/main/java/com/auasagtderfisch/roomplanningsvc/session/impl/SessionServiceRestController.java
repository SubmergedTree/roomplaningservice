/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.session.impl;

import com.auasagtderfisch.roomplanningsvc.session.SessionService;
import com.auasagtderfisch.roomplanningsvc.util.validation.ValidationService;
import com.auasagtderfisch.roomplanningsvc.util.vertx.json.JsonUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.RestUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SessionServiceRestController implements VertxRestController {

    private Logger logger = LoggerFactory.getLogger(SessionServiceRestController.class);

    private final SessionService sessionService;
    private final ValidationService validationService;

    @Autowired
    public SessionServiceRestController(SessionService sessionService, ValidationService validationService) {
        this.sessionService = sessionService;
        this.validationService = validationService;
    }

    public Router setupRoutes(Vertx vertx) {
        Router subRouter = Router.router(vertx);
        subRouter.route("/create*").handler(BodyHandler.create());
        subRouter.post("/create").handler(this::createProject);

        subRouter.route("/load*").handler(BodyHandler.create());
        subRouter.post("/load").handler(this::loadProject);

        subRouter.route("/getAll*").handler(BodyHandler.create());
        subRouter.get("/getAll").handler(this::getAll);

        return subRouter;
    }

    @Override
    public String getSubRoute() {
        return "/session";
    }

    private void createProject(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        JsonUtil.serializeRequest(routingContext, ProjectPojo.class)
                .flatMap(validationService::validate)
                .flatMap(projectPojo -> sessionService
                        .createProject(projectPojo.projectName, "DGUV16", projectPojo.userName))
                .onSuccess(sessionId -> {
                    response.setStatusCode(200)
                            .end(JsonUtil.encodeObject(new CreateProjectResultPojo(sessionId.toString())));
                })
                .onFailure(err -> {
                    response.setStatusCode(400).end(JsonUtil.encodeExceptionToBuffer(err, 400));
                });
    }

    private void loadProject(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        JsonUtil.serializeRequest(routingContext, ProjectPojo.class)
                .flatMap(validationService::validate)
                .flatMap(projectPojo -> sessionService
                        .loadProject(projectPojo.projectName, projectPojo.userName))
                .onSuccess(sessionId -> {
                    response.setStatusCode(200)
                            .end(JsonUtil.encodeObject(new CreateProjectResultPojo(sessionId.toString())));
                })
                .onFailure(err -> {
                    response.setStatusCode(400).end(JsonUtil.encodeExceptionToBuffer(err, 400));
                });
    }

    private void getAll(RoutingContext routingContext) {
        var response = routingContext.response();
        sessionService.getAllProjects()
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }
}
