/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall;

import com.auasagtderfisch.roomplanningsvc.wall.pojo.*;
import io.vertx.core.Future;

public interface WallService {
    Future<GetWallResponsePojo> getWall(String sessionId);
    Future<ModifyWallResponsePojo> modifyWall(ModifyWallPojo wall);
    Future<Void> addDoor();
    Future<Void> deleteDoor();
    Future<NumOfPeople> changeNumOfPeople(String sessionId, int amount);
    Future<NumOfPeople> getNumOfPeople(String sessionId);
}
