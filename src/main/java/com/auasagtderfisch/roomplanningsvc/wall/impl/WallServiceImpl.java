/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.TaskType;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.session.SessionService;
import com.auasagtderfisch.roomplanningsvc.wall.WallService;
import com.auasagtderfisch.roomplanningsvc.wall.impl.worker.*;
import com.auasagtderfisch.roomplanningsvc.wall.pojo.*;
import io.vertx.core.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WallServiceImpl implements WallService {

    private final SessionService sessionService;

    @Autowired
    public WallServiceImpl(SessionService sessionService) {
        this.sessionService = sessionService;
    }


    @Override
    public Future<GetWallResponsePojo> getWall(String sessionId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    GetWallTask getWallTask = new GetWallTask();
                    Task task = new Task(TaskType.WallTask, getWallTask);
                    return blackboardService.addTask(task)
                            .map(a -> (GetWallAnswer)a.getAnswer())
                            .map(a -> new GetWallResponsePojo(a.getPoints()));
                });
    }

    @Override
    public Future<ModifyWallResponsePojo> modifyWall(ModifyWallPojo wall) {
        return sessionService.getBlackboardForSession(wall.sessionId)
                .flatMap(blackboardService -> {
                    ModifyWalkTask modifyWalkTask = new ModifyWalkTask(wall.getPoints(), wall.name);
                    Task task = new Task(TaskType.WallTask, modifyWalkTask);
                     return blackboardService.addTask(task)
                        .map(a -> (ModifyWallAnswer)a.getAnswer())
                        .map(a -> new ModifyWallResponsePojo(a.getPoints(), a.getErrors()));
                });
    }

    @Override
    public Future<Void> addDoor() {
        // todo
        return Future.succeededFuture();
    }

    @Override
    public Future<Void> deleteDoor() {
        // todo
        return Future.succeededFuture();
    }

    @Override
    public Future<NumOfPeople> changeNumOfPeople(String sessionId, int amount) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    var amountPeopleTask = new ChangeAmountPeopleTask(amount);
                    var task = new Task(TaskType.WallTask, amountPeopleTask);
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (AmountPeopleAnswer)a)
                            .map(a -> new NumOfPeople(a.getAmount(), a.getRuleEvalErrors()));
                });
    }

    @Override
    public Future<NumOfPeople> getNumOfPeople(String sessionId) {
        return sessionService.getBlackboardForSession(sessionId)
                .flatMap(blackboardService -> {
                    var amountPeopleTask = new GetAmountPeopleTask();
                    var task = new Task(TaskType.WallTask, amountPeopleTask);
                    return blackboardService.addTask(task)
                            .flatMap(Answer::unpack)
                            .map(a -> (AmountPeopleAnswer)a)
                            .map(a -> new NumOfPeople(a.getAmount(), a.getRuleEvalErrors()));
                });
    }
}
