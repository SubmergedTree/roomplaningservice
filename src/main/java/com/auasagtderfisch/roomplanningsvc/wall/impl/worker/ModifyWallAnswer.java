/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.ArrayList;
import java.util.List;

public class ModifyWallAnswer {
    private final List<Point> points;
    private final List<RuleEvalError> errors;

    public ModifyWallAnswer(List<Point> points, List<RuleEvalError> errors) {
        this.points = points;
        this.errors = errors;
    }
    public ModifyWallAnswer(List<Point> points, RuleEvalError error) {
        this.points = points;
        this.errors = new ArrayList<>();
        this.errors.add(error);
    }

    public List<Point> getPoints() {
        return points;
    }

    public List<RuleEvalError> getErrors() {
        return errors;
    }
}
