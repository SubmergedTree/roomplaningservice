/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;


public class WallTask {
    public enum WallTaskType {
        MODIFY_WALL,
        GET_WALL,
        CHANGE_AMOUNT_PEOPLE,
        GET_AMOUNT_PEOPLE
    }

    private final WallTaskType type;

    public WallTask(WallTaskType wallTaskType) {
        this.type = wallTaskType;
    }

    @SuppressWarnings("unchecked")
    public <T extends WallTask> T get() {
        return (T) this;
    }

    public WallTaskType getType() {
        return type;
    }
}
