/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

public class GetWallTask extends WallTask {
    public GetWallTask() {
        super(WallTaskType.GET_WALL);
    }
}
