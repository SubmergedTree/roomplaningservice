/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Wall;

import java.util.Arrays;
import java.util.List;

public class GetWallAnswer {
    private final Wall wall;

    public GetWallAnswer(Wall wall) {
        this.wall = wall;
    }

    public List<Point> getPoints() {
        List<Point> points = Arrays.asList(wall.getVertices());
        return points;
    }
}
