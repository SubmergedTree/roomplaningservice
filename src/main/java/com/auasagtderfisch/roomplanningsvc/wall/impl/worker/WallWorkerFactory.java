/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractWorker;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.WallFactory;
import com.auasagtderfisch.roomplanningsvc.util.function.Function2;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WallWorkerFactory {
    private final String callCmd = "#WallWorker";

    private final CAD2DObjectFactory cad2dObjectFactory;
    private final WallFactory wallFactory;
    private final LocalizationService localizationService;

    @Autowired
    public WallWorkerFactory(CAD2DObjectFactory cad2dObjectFactory,
                             WallFactory wallFactory,
                             LocalizationService localizationService) {
        this.cad2dObjectFactory = cad2dObjectFactory;
        this.wallFactory = wallFactory;
        this.localizationService = localizationService;
    }

    @Bean
    public Function2<String, Blackboard, WallWorker> wallWorker() {
        return (String modelUUID, Blackboard store) ->
                new WallWorker(AbstractWorker.getListenTo(modelUUID, callCmd), store,
                        cad2dObjectFactory, wallFactory, localizationService);
    }
}
