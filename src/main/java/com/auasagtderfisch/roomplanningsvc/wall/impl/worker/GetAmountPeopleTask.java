/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

public class GetAmountPeopleTask extends WallTask {
    public GetAmountPeopleTask() {
        super(WallTaskType.GET_AMOUNT_PEOPLE);
    }
}
