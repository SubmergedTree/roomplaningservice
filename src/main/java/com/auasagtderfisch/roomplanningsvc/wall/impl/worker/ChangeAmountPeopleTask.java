/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

public class ChangeAmountPeopleTask extends WallTask {

    private final int newAmount;

    public ChangeAmountPeopleTask(int newAmount) {
        super(WallTaskType.CHANGE_AMOUNT_PEOPLE);
        this.newAmount = newAmount;
    }

    public int getNewAmount() {
        return newAmount;
    }
}
