/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractWorker;
import com.auasagtderfisch.roomplanningsvc.blackboard.Blackboard;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Answer;
import com.auasagtderfisch.roomplanningsvc.blackboard.communication.Task;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.ErrorType;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Room;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Wall;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.WallException;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.WallFactory;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;


public class WallWorker extends AbstractWorker {

    private final WallFactory wallFactory;
    private final Logger logger = LoggerFactory.getLogger(WallWorker.class);

    public WallWorker(String listenTo, Blackboard store, CAD2DObjectFactory cad2dObjectFactory,
                      WallFactory wallFactory, LocalizationService localizationService) {
        super(store, cad2dObjectFactory,localizationService, listenTo);
        this.wallFactory = wallFactory;
    }

    @Override
    protected Optional<Answer> onTaskReceived(Task task) {
        WallTask wallTask = (WallTask) task.getTask();

        switch (wallTask.getType()) {
            case GET_WALL:
                GetWallAnswer getWallAnswer = getWall();
                return Optional.of(new Answer(getWallAnswer));
            case MODIFY_WALL:
                ModifyWallAnswer modifyWallAnswer = addWall(wallTask.get());
                return Optional.of(new Answer(modifyWallAnswer));
            case CHANGE_AMOUNT_PEOPLE:
                return Optional.of(changeAmountOfPeople(wallTask.get()));
            case GET_AMOUNT_PEOPLE:
                return Optional.of(getAmountOfPeople());
            default:
                return Optional.empty();
        }
    }

    private GetWallAnswer getWall() {
        Room room = store.get();
        return new GetWallAnswer(room.getWall());
    }

    private ModifyWallAnswer addWall(ModifyWalkTask modifyWalkTask) {
        Room room = store.get();
        List<Point> points = modifyWalkTask.getPoints();
        try {
            Wall w = wallFactory.createWall(points);
            room.changeWall(w);
            var error = room.checkErrors();
            return new ModifyWallAnswer(points, error);
        } catch (WallException e) {
            var msg = localizationService.getForKey("notEnoughWalls");
            var error = new RuleEvalError(msg, ErrorType.MALFORMED_WALL);
            return new ModifyWallAnswer(points, error);
        }
    }

    private Answer changeAmountOfPeople(ChangeAmountPeopleTask changeAmountPeopleTask) {
        Room room = store.get();
        int newAmount = changeAmountPeopleTask.getNewAmount();
        room.setAmountPersons(newAmount);
        var errors = room.checkErrors();
        return new Answer(new AmountPeopleAnswer(newAmount, errors));
    }

    private Answer getAmountOfPeople() {
        Room room = store.get();
        int amountPersons = room.getAmountPersons();
        var errors = room.checkErrors();
        return new Answer(new AmountPeopleAnswer(amountPersons, errors));
    }

}