/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

import java.util.List;

public class ModifyWalkTask extends WallTask {
    private List<Point> points;
    private String name;

    public ModifyWalkTask(List<Point> points, String name) {
        super(WallTaskType.MODIFY_WALL);
        this.points = points;
        this.name = name;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
