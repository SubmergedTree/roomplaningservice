/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl.worker;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class AmountPeopleAnswer {
    private final int amount;
    private final List<RuleEvalError> ruleEvalErrors;

    public AmountPeopleAnswer(int amount, List<RuleEvalError> ruleEvalErrors) {
        this.amount = amount;
        this.ruleEvalErrors = ruleEvalErrors;
    }

    public int getAmount() {
        return amount;
    }

    public List<RuleEvalError> getRuleEvalErrors() {
        return ruleEvalErrors;
    }
}
