/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.impl;

import com.auasagtderfisch.roomplanningsvc.util.validation.ValidationService;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.RestUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import com.auasagtderfisch.roomplanningsvc.wall.WallService;
import com.auasagtderfisch.roomplanningsvc.wall.pojo.ModifyWallPojo;
import com.auasagtderfisch.roomplanningsvc.util.vertx.json.JsonUtil;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WallRestController implements VertxRestController {
    private final static String SESSION_ID_KEY = "sessionId";
    private final static String NUMBER_OF_PEOPLE = "numOfPeople";

    private final Logger logger = LoggerFactory.getLogger(WallRestController.class);

    private final WallService wallService;
    private final ValidationService validator;

    @Autowired
    public WallRestController(WallService wallService, ValidationService validator) {
        this.wallService = wallService;
        this.validator = validator;
    }

    public Router setupRoutes(Vertx vertx) {
        Router subRouter = Router.router(vertx);
        subRouter.route("/getWall*").handler(BodyHandler.create());
        subRouter.get("/getWall").handler(this::getWall);

        subRouter.route("/modifyWall*").handler(BodyHandler.create());
        subRouter.post("/modifyWall").handler(this::modifyWall);

        subRouter.route("/getAmountOfPeople*").handler(BodyHandler.create());
        subRouter.get("/getAmountOfPeople").handler(this::getAmountOfPeople);

        subRouter.route("/changeAmountOfPeople*").handler(BodyHandler.create());
        subRouter.put("/changeAmountOfPeople").handler(this::changeNumOfPersons);

        return subRouter;
    }

    @Override
    public String getSubRoute() {
        return "/wall";
    }

    private void getWall(RoutingContext routingContext) {
        var response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
            sessionId.flatMap(wallService::getWall)
                .onSuccess(getWallResponse -> {
                    response.setStatusCode(200).end(JsonUtil.encodeObject(getWallResponse));
                })
                .onFailure(err -> {
                    response.setStatusCode(400).end(JsonUtil.encodeExceptionToBuffer(err, 400));
                });
    }

    private void modifyWall(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var modifyWallBody = JsonUtil.serializeRequest(routingContext, ModifyWallPojo.class);
        var modifyWall = sessionId.compose(id -> modifyWallBody.map(a -> a.setSessionId(id)));
        modifyWall.flatMap(validator::validate)
            .flatMap(wallService::modifyWall)
                .onSuccess(addWallResponsePojo -> {
                    response.setStatusCode(200).end(JsonUtil.encodeObject(addWallResponsePojo));
                })
                .onFailure(err -> {
                    response.setStatusCode(400).end(JsonUtil.encodeExceptionToBuffer(err, 400));
                });
    }

    private void changeNumOfPersons(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        var newNumber = RestUtil.extractParamInt(routingContext.request(), NUMBER_OF_PEOPLE);
        CompositeFuture.all(sessionId, newNumber)
                .flatMap(cf -> wallService.changeNumOfPeople(sessionId.result(), newNumber.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }

    private void getAmountOfPeople(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        var sessionId = RestUtil.extractParamString(routingContext.request(), SESSION_ID_KEY);
        sessionId.flatMap(cf -> wallService.getNumOfPeople(sessionId.result()))
                .onComplete(h -> RestUtil.genericResponseHandler(h, response));
    }
}
