/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.pojo;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class NumOfPeople {
    private final int amountOfPeople;
    private final List<RuleEvalError> errors;

    public NumOfPeople(int newAmountOfPeople, List<RuleEvalError> errors) {
        this.amountOfPeople = newAmountOfPeople;
        this.errors = errors;
    }

    public int getAmountOfPeople() {
        return amountOfPeople;
    }

    public List<RuleEvalError> getErrors() {
        return errors;
    }
}
