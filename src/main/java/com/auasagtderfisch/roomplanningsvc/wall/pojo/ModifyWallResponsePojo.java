/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.pojo;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.RuleEvalError;

import java.util.List;

public class ModifyWallResponsePojo {
    private List<Point> points;
    private List<RuleEvalError> errors;

    public ModifyWallResponsePojo() {
    }

    public ModifyWallResponsePojo(List<Point> points, List<RuleEvalError> errors) {
        this.points = points;
        this.errors = errors;
    }

    public List<Point> getPoints() {
        return points;
    }

    public List<RuleEvalError> getErrors() {
        return errors;
    }
}
