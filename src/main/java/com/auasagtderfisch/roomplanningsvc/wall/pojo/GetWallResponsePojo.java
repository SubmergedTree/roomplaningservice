/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.pojo;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;

import java.util.List;

public class GetWallResponsePojo {
    public List<Point> points;

    public GetWallResponsePojo() {
    }

    public GetWallResponsePojo(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }
}
