/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.wall.pojo;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ModifyWallPojo {
    @NotNull
    List<Point> points;
    @NotNull
    @NotBlank(message = "name must not be empty")
    public String name;

    @JsonIgnore
    @NotNull
    @NotBlank(message = "sessionId must not be empty")
    public String sessionId;

    public ModifyWallPojo() {

    }

    public ModifyWallPojo(List<Point> points, String name, String sessionId) {
        this.points = points;
        this.name = name;
        this.sessionId = sessionId;
    }

    public List<Point> getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ModifyWallPojo setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }
}
