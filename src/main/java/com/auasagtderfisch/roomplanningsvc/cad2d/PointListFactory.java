package com.auasagtderfisch.roomplanningsvc.cad2d;

import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.List;

public interface PointListFactory {
    List<Point> createRectangle(Point ol, Point or, Point ul, Point ur);
    List<Point> createRectangleFromDimension(Point dimension);
    List<Point> createRectangleWithInitialPoints(Point ol, Point or, Point dimension);
    List<Point> createRectangleWithDepthPoints(Point ol, Point or, Point dimension);
}
