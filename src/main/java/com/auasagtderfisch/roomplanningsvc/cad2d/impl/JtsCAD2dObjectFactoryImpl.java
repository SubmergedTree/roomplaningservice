/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JtsCAD2dObjectFactoryImpl implements CAD2DObjectFactory {
    public JtsCAD2dObject create(List<Point> points) {
        return new JtsCAD2dObject(createCoordinateArraySequence(points), JtsCAD2dObject.InitialPosition.ZERO);
    }

    public JtsCAD2dObject createWithLocalCentroid(List<Point> points) {
        return new JtsCAD2dObject(createCoordinateArraySequence(points), JtsCAD2dObject.InitialPosition.LOCAL_CENTROID);
    }

    public JtsCAD2dObject create(List<Point> points, Point globalPosition, double rotation) {
        return new JtsCAD2dObject(createCoordinateArraySequence(points), globalPosition, rotation);
    }

    private CoordinateArraySequence createCoordinateArraySequence(List<Point> points) {
        Coordinate[] coordinates = points
                .stream()
                .map(point -> new Coordinate(point.getX(), point.getY()))
                .toArray(Coordinate[]::new);
        return new CoordinateArraySequence(coordinates);
    }

}
