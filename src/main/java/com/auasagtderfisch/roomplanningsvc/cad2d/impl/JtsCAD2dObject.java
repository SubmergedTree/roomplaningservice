/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.locationtech.jts.algorithm.MinimumDiameter;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;

//Documentation of JTS says that it is preferred to threat a Geometry instance as immutable.
public class JtsCAD2dObject implements CAD2DObject, Serializable {

    private final Logger logger = LoggerFactory.getLogger(JtsCAD2dObject.class);


    public enum InitialPosition {
        ZERO,
        LOCAL_CENTROID
    }

    private Geometry globalGeometry;
    private Point globalPosition;
    private double globalRotation;
    private final CoordinateSequence localCoordinates;

    private final GeometryFactory geometryFactory;

    public JtsCAD2dObject(CoordinateSequence localCoordinates, InitialPosition initialPosition) {
        this.geometryFactory = new GeometryFactory();
        CoordinateSequence coordinateSequence = new CoordinateArraySequence(localCoordinates);
        this.localCoordinates = localCoordinates;
        this.globalGeometry = geometryFactory.createPolygon(coordinateSequence);
        if (initialPosition == InitialPosition.LOCAL_CENTROID) {
            org.locationtech.jts.geom.Point centroid = globalGeometry.getCentroid();
            this.globalPosition = new Point(centroid.getX(), centroid.getY());
        } else if (initialPosition == InitialPosition.ZERO) {
            this.globalPosition = new Point(0,0);
        }
        this.globalRotation = 0;
        setPosition(this.globalPosition);

        // rotate(this.globalRotation); //SetPosition does an implicit rotation
    }



    public JtsCAD2dObject(CoordinateSequence localCoordinates, Point globalPosition, double globalRotation) {
        this.geometryFactory = new GeometryFactory();
        CoordinateSequence coordinateSequence = new CoordinateArraySequence(localCoordinates);
        this.localCoordinates = localCoordinates;
        this.globalPosition = globalPosition;
        this.globalRotation = globalRotation;
        globalGeometry = geometryFactory.createPolygon(coordinateSequence);
        setPosition(this.globalPosition);
        // rotate(this.globalRotation); //SetPosition does an implicit rotation
    }

    @Override
    public void setPosition(Point position) {
        Coordinate[] newCoordinates = new Coordinate[localCoordinates.size()];
        Geometry local = geometryFactory.createPolygon(localCoordinates);
        org.locationtech.jts.geom.Point oldCentroid = local.getCentroid();
       // org.locationtech.jts.geom.Point oldCentroid = globalGeometry.getCentroid();
        Point toMove = new Point(position.getX() - oldCentroid.getX(), position.getY() - oldCentroid.getY());

        for (int i = 0; i < localCoordinates.size(); i++) {
            double newX = localCoordinates.getX(i) + toMove.getX();
            double newY = localCoordinates.getY(i) + toMove.getY();
            newCoordinates[i] = new Coordinate(newX, newY);
        }
        CoordinateSequence coordinateSequence = new CoordinateArraySequence(newCoordinates);
        globalGeometry = geometryFactory.createPolygon(coordinateSequence);
        adjustPosition(globalGeometry);

        rotate(this.globalRotation);
    }

    @Override
    public void move(Point distanceToMove) {
        Coordinate[] movedCoordinates = new Coordinate[localCoordinates.size()];
        Coordinate[] oldCoordinates = globalGeometry.getCoordinates();

        for (int i = 0; i < localCoordinates.size(); i++) {
            double newX = oldCoordinates[i].getX() + distanceToMove.getX();
            double newY = oldCoordinates[i].getY() + distanceToMove.getY();
            movedCoordinates[i] = new Coordinate(newX, newY);
        }

        CoordinateSequence coordinateSequence = new CoordinateArraySequence(movedCoordinates);
        globalGeometry = geometryFactory.createPolygon(coordinateSequence);
        adjustPosition(globalGeometry);

        rotate(this.globalRotation);
    }

    @Override
    public void rotate(double angle) {
        rotateAround(angle, globalPosition);
    }

    @Override
    public void rotateAround(double angle, Point around) {
        // see: https://math.stackexchange.com/questions/1917449/rotate-polygon-around-center-and-get-the-coordinates/1917485
        Coordinate[] oldCoordinates = globalGeometry.getCoordinates();
        final int n = oldCoordinates.length;
        RealMatrix p = twoXNMatrix(n, oldCoordinates);
        RealMatrix c =  centroidMatrix(n, around);
        RealMatrix r = calculateRotationMatrix(angle);

        //RealMatrix subtracted = p.subtract(c);
        //RealMatrix multiplied =  r.multiply(subtracted);
        //RealMatrix rotated = multiplied.add(c);
        RealMatrix rotated = r.multiply((p.subtract(c))).add(c);

         Coordinate[] newCoordinates = new Coordinate[n];
        for (int i = 0; i < n; i++) {
            double[] column = rotated.getColumn(i);
            newCoordinates[i] = new Coordinate(column[0], column[1]);
        }
        CoordinateSequence coordinateSequence = new CoordinateArraySequence(newCoordinates);
        globalGeometry = geometryFactory.createPolygon(coordinateSequence);
        this.globalRotation = angle;
        adjustPosition(globalGeometry); // unnecessary but good validity check for tests

    }

    private RealMatrix calculateRotationMatrix(double angle) {
        double[][] rData = { {Math.cos(Math.toRadians(angle)), -Math.sin(Math.toRadians(angle)) },
                {Math.sin(Math.toRadians(angle)), Math.cos(Math.toRadians(angle))} };
        return MatrixUtils.createRealMatrix(rData);
    }

    private RealMatrix twoXNMatrix(int n, Coordinate[] coords) {
        double[][] pData = new double[2][n]; // {x,x,x,x,x} {y,y,y,y,y} 2 rows 5 columns
        double[] pRow1 = new double[n];
        double[] pRow2 = new double[n];
        for (int i = 0; i < n; i++) {
            pRow1[i] = coords[i].getX();
            pRow2[i] = coords[i].getY();
        }
        pData[0] = pRow1;
        pData[1] = pRow2;

        return MatrixUtils.createRealMatrix(pData);
    }

    private RealMatrix centroidMatrix(int n, Point centroid) {
        double[][] cData = new double[2][n]; //centroid matrix
        double[] cRow1 = new double[n];
        double[] cRow2 = new double[n];

        for (int i = 0; i < n; i++) {
            cRow1[i] = centroid.getX();
            cRow2[i] = centroid.getY();
        }
        cData[0] = cRow1;
        cData[1] = cRow2;
        return MatrixUtils.createRealMatrix(cData);
    }

    @Override
    public Point getPosition() {
        return globalPosition;
    }

    @Override
    public double getRotation() {
        return globalRotation;
    }

    @Override
    public boolean intersectWith(CAD2DObject other) {
        if (other instanceof JtsCAD2dObject) {
            try {
                return globalGeometry.intersects(((JtsCAD2dObject) other).globalGeometry);
            } catch (Exception e) {
                logger.warn("intersectWith jts error", e);
                return true;
            }
        } else
            throw new RuntimeException("CAD2DObject other is not an JtsCAD2dObject instance");
    }

    @Override
    public boolean isInside(CAD2DObject other) {
        if (other instanceof JtsCAD2dObject) {
            try {
                return globalGeometry.contains(((JtsCAD2dObject) other).globalGeometry); // or geometry.covers(...) ?
            } catch (Exception e) {
                logger.warn("isInside jts error", e);
                return true;
            }
        }
        else
            throw new RuntimeException("CAD2DObject other is not an JtsCAD2dObject instance");
    }

    @Override
    public double distanceTo(CAD2DObject other) {
        if (other instanceof JtsCAD2dObject) {
            try {
                return globalGeometry.distance(((JtsCAD2dObject) other).globalGeometry);
            } catch (Exception e) {
                logger.warn("distanceTo jts error", e);
                return 0.0;
            }
        } else
            throw new RuntimeException("CAD2DObject other is not an JtsCAD2dObject instance");
    }

    @Override
    public Point[] getVertices() {
        Coordinate[] coordinates = globalGeometry.getCoordinates();
        return Arrays.stream(coordinates)
                .map(c -> new Point(c.getX(), c.getY()))
                .toArray(Point[]::new);
    }

    @Override
    public double getSize() {
        return this.globalGeometry.getArea();
    }

    private void adjustPosition(Geometry newGeometry) {
        org.locationtech.jts.geom.Point p = globalGeometry.getCentroid();
        globalPosition = new Point(p.getX(), p.getY());
    }

    @Override
    public Point getDimension() {
     //   Geometry geometry = MinimumDiameter.getMinimumRectangle(globalGeometry);
  //      Coordinate[] coordinates = localCoordinates.toCoordinateArray();
        Geometry geometry = geometryFactory.createPolygon(this.localCoordinates);
        Geometry envelope = geometry.getEnvelope();

        Coordinate[] coordinates = envelope.getCoordinates();

            if((coordinates.length>2)) {
                double x = coordinates[2].getX()-coordinates[0].getX();
                double y = coordinates[2].getY()-coordinates[0].getY();
                return new Point(x,y);
            } else if (coordinates.length<3) {
                double x = coordinates[1].getX()-coordinates[0].getX();
                double y = coordinates[1].getY()-coordinates[0].getY();
                return new Point(x,y);
            }

        return new Point(0,0);
    }
}
