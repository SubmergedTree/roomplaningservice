package com.auasagtderfisch.roomplanningsvc.cad2d.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PointListFactoryImpl implements PointListFactory {
    public List<Point> createRectangle(Point ol, Point or, Point ul, Point ur) {
        List<Point> points = new ArrayList<>();
        points.add(ul);
        points.add(ur);
        points.add(or);
        points.add(ol);
        points.add(ul);
        return points;
    }
    public List<Point> createRectangleFromDimension(Point dimension) {
        List<Point> points = new ArrayList<>();
        points.add(new Point(0, 0));
        points.add(new Point(dimension.getX(), 0));
        points.add(new Point(dimension.getX(),dimension.getY()));
        points.add(new Point(0, dimension.getY()));
        points.add(new Point(0, 0));
        return points;
    }

    public List<Point> createRectangleWithInitialPoints(Point ol, Point or, Point dimension) {
        List<Point> points = new ArrayList<>();
        points.add(new Point(ol.getX()-dimension.getX(), ol.getY()-dimension.getY()));
        points.add(new Point(or.getX()-dimension.getX(), ol.getY()-dimension.getY()));
        points.add(or);
        points.add(ol);
        points.add(new Point(ol.getX()-dimension.getX(), ol.getY()-dimension.getY()));
        return points;
    }

    public List<Point> createRectangleWithDepthPoints(Point ol, Point or, Point dimension) {
        List<Point> points = new ArrayList<>();
        points.add(new Point(ol.getX(), ol.getY()-dimension.getY()));
        points.add(new Point(or.getX(), ol.getY()-dimension.getY()));
        points.add(or);
        points.add(ol);
        points.add(new Point(ol.getX(), ol.getY()-dimension.getY()));
        return points;
    }
}
