/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d;

public interface CAD2DObject {
    void setPosition(Point position);
    void move(Point distanceToMove);
    void rotate(double angle);
    void rotateAround(double angle, Point around);

    // returns centroid
    Point getPosition();
    double getRotation();

    boolean intersectWith(CAD2DObject other);
    boolean isInside(CAD2DObject other);
    double distanceTo(CAD2DObject other);

    Point[] getVertices();
    double getSize();
    Point getDimension();
}
