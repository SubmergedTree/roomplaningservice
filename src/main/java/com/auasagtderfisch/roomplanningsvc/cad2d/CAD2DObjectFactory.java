/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.cad2d;

import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.List;

public interface CAD2DObjectFactory {
    JtsCAD2dObject create(List<Point> points);
    JtsCAD2dObject createWithLocalCentroid(List<Point> points);
    JtsCAD2dObject create(List<Point> points, Point globalPosition, double rotation);
}
