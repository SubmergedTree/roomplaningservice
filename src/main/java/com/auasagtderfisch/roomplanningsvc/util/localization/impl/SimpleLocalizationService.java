/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.localization.impl;

import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.ResourceBundle;

@Service
public class SimpleLocalizationService implements LocalizationService {
    private final static String LANGUAGE = "de";
    private final static String COUNTRY = "DE";
    private final static String TRANSLATION_KEY = "translation";

    private final ResourceBundle ressources;

    public SimpleLocalizationService(){
        Locale locale = new Locale(LANGUAGE, COUNTRY);
        this.ressources = ResourceBundle.getBundle(TRANSLATION_KEY, locale);
    }

    @Override
    public String getForKey(String key) {
        return ressources.getString(key);
    }

    @Override
    public String getForKey(String key, Object... placeholder) {
        var msg = ressources.getString(key);
        return String.format(msg, placeholder);
    }
}
