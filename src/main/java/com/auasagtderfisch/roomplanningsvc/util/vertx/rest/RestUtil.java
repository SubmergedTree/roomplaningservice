/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.rest;

import com.auasagtderfisch.roomplanningsvc.util.vertx.json.JsonUtil;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

public class RestUtil {
    public static Future<String> extractParamString(HttpServerRequest request, String key) {
        String param = request.getParam(key);
        if (param != null) {
            return Future.succeededFuture(param);
        }
        return Future.failedFuture(new Exception("HTTP parameter " + key + " is required"));
    }

    public static Future<Integer> extractParamInt(HttpServerRequest request, String key) {
        String param = request.getParam(key);
        if (param != null) {
            try {
                Integer paramInt = Integer.valueOf(param);
                return Future.succeededFuture(paramInt);
            } catch (NumberFormatException e) {
                return Future.failedFuture(new Exception("HTTP parameter " + key + " is not an integer"));
            }
        }
        return Future.failedFuture(new Exception("HTTP parameter " + key + " is required"));
    }

    public static <T> void genericResponseHandler(AsyncResult<T> result, HttpServerResponse response) {
        if (result.succeeded()) {
            response.setStatusCode(200).end(JsonUtil.encodeObject(result.result()));
        } else {
            response.setStatusCode(400).end(JsonUtil.encodeExceptionToBuffer(result.cause(), 400));
        }
    }
}
