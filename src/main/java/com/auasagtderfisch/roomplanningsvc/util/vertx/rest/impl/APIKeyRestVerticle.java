/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.rest.impl;

import com.auasagtderfisch.roomplanningsvc.util.vertx.json.JsonUtil;
import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component("APIKeyRestVerticle")
public class APIKeyRestVerticle extends RestVerticle {
    private final static String API_KEY = "API-KEY";
    private final Logger logger = LoggerFactory.getLogger(StdRestVerticle.class);
    private final String apiKeyValue;
    private final int port;

    private final List<VertxRestController> vertxRestControllers;
    private HttpServer httpServer;

    @Autowired
    public APIKeyRestVerticle(List<VertxRestController> vertxRestControllers, Environment environment) {
        this.vertxRestControllers = vertxRestControllers;
        this.apiKeyValue = environment.getProperty("RP_SVC_API_KEY");
        Integer port = environment.getProperty("RP_SVC_PORT", Integer.class);
        this.port = Objects.requireNonNullElse(port, 8080);
        System.out.println(apiKeyValue);
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        this.httpServer = vertx.createHttpServer();
        Router router = Router.router(vertx);
        enableCORS(router);
        evaluateAPIKey(router);
        setupRoutes(router);
        this.httpServer.requestHandler(router).listen(this.port);
        logger.info("Rest controller started. API Key required");
        startPromise.complete();
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        httpServer.close();
        stopPromise.complete();
    }

    private void setupRoutes(Router router) {
        vertxRestControllers.forEach( vrc -> {
            router.mountSubRouter(vrc.getSubRoute(), vrc.setupRoutes(vertx));
        });
    }

    private void enableCORS(Router router) {
        Set<String> allowedHeaders = new HashSet<>();
        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("API-KEY");
        allowedHeaders.add("X-PINGARUNER");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        router.route().handler(CorsHandler
                .create("*")
                .allowedHeaders(allowedHeaders)
                .allowedMethods(allowedMethods));
    }

    private void evaluateAPIKey(Router router) {
        router.route().handler(ctx -> {
            String apiKey = ctx.request().getHeader(API_KEY);
            if (apiKey == null) {
                ctx.response()
                        .setStatusCode(404)
                        .end(JsonUtil.encodeExceptionToBuffer(new Exception("API key required"), 404));
            } else if (apiKey.equals(apiKeyValue)) {
                ctx.next();
            } else {
                ctx.response()
                        .setStatusCode(404)
                        .end(JsonUtil.encodeExceptionToBuffer(new Exception("API key invalid"), 404));
            }
        }) ;
    }
}
