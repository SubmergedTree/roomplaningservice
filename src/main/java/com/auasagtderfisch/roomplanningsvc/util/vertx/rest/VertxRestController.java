/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.rest;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;

public interface VertxRestController {
    Router setupRoutes(Vertx vertx);
    String getSubRoute();
}
