/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.rest.impl;

import com.auasagtderfisch.roomplanningsvc.util.vertx.rest.VertxRestController;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("StandardRestVerticle")
public class StdRestVerticle extends RestVerticle {

    private final Logger logger = LoggerFactory.getLogger(StdRestVerticle.class);

    private final List<VertxRestController> vertxRestControllers;
    private HttpServer httpServer;

    @Autowired
    public StdRestVerticle(List<VertxRestController> vertxRestControllers) {
        this.vertxRestControllers = vertxRestControllers;
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        this.httpServer = vertx.createHttpServer();
        Router router = Router.router(vertx);
        setupRoutes(router);
        this.httpServer.requestHandler(router).listen(8080);
        logger.info("Rest controller started");
        startPromise.complete();
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        httpServer.close();
        stopPromise.complete();
    }

    private void setupRoutes(Router router) {
        vertxRestControllers.forEach( vrc -> {
            router.mountSubRouter(vrc.getSubRoute(), vrc.setupRoutes(vertx));
        });
    }
}
