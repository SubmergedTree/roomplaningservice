/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.json;

import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class JsonUtil {
    // misuse future as either
    public static <T> Future<T> serializeRequest(RoutingContext routingContext, Class<T> toMap) {
        try {
            JsonObject jsonObject = routingContext.getBodyAsJson();
            if (jsonObject == null) {
                return Future.failedFuture(new Exception("No body found"));
            }
            T result = jsonObject.mapTo(toMap);
            return Future.succeededFuture(result);
        } catch (Exception e) {
            return Future.failedFuture(e);
        }
    }

    public static Buffer encodeExceptionToBuffer(Throwable e, int statusCode) {
        return Json.encodeToBuffer(new BaseResultPojo(e.getMessage(), statusCode));
    }

    public static Buffer encodeStatus(int statusCode) {
        return Json.encodeToBuffer(new BaseResultPojo("", statusCode));
    }

    public static <T> Buffer encodeObject(T object) {
        return Json.encodeToBuffer(object);
    }
}
