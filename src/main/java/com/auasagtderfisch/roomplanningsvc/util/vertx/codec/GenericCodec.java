/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.codec;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

public class GenericCodec<T> implements MessageCodec<T, T> {
    private final Class<T> classToEncode;

    public static <R> void registerDefault(Vertx vertx, Class<R> classToEncode) {
        vertx.eventBus().registerDefaultCodec(classToEncode, new GenericCodec<>(classToEncode));
    }

    public GenericCodec(Class<T> classToEncode) {
        super();
        this.classToEncode = classToEncode;
    }

    @Override
    public void encodeToWire(Buffer buffer, T t) {
        // intentionally left blank
    }

    @Override
    public T decodeFromWire(int pos, Buffer buffer) {
        // intentionally left blank
        return null;
    }

    @Override
    public T transform(T t) {
        return t;
    }

    @Override
    public String name() {
        return classToEncode.getSimpleName() + "Codec";
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
