/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.vertx.rest.impl;

import io.vertx.core.AbstractVerticle;

public abstract class RestVerticle extends AbstractVerticle {
    // intentionally left blank
}
