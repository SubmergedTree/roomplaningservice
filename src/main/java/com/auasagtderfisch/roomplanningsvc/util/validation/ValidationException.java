/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.validation;

import java.util.Arrays;

public class ValidationException extends RuntimeException {

    String[] validationErrors;

    public ValidationException(String[] validationErrors) {
        this.validationErrors = validationErrors;
    }

    @Override
    public String getMessage() {
        return pretty();
    }

    private String pretty() {
        return Arrays.stream(validationErrors)
                .reduce("", (acc, err) -> {
                    if (acc.equals(""))
                        return err;
                    return acc + " \n " + err;
                });
    }
}
