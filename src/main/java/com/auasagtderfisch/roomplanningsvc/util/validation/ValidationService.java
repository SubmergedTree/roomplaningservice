/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.util.validation;

import io.vertx.core.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
public class ValidationService {

    private final Validator validator;

    @Autowired
    public ValidationService(Validator validator) {
        this.validator = validator;
    }

    public <T> Future<T> validate(T toValidate) {
        Set<ConstraintViolation<T>> violations = validator.validate(toValidate);
        if (violations.isEmpty()) {
            return Future.succeededFuture(toValidate);
        }
        String[] violationMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                .toArray(String[]::new);
        return Future.failedFuture(new ValidationException(violationMessages));
    }
}
