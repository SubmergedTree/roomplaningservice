/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Wall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WallFactory {

    private final CAD2DObjectFactory cad2DObjectFactory;
    private final Logger logger = LoggerFactory.getLogger(WallFactory.class);

    @Autowired
    public WallFactory(CAD2DObjectFactory cad2DObjectFactory) {
        this.cad2DObjectFactory = cad2DObjectFactory;
    }

    public Wall createWall(List<Point> points) {
        if (points.size() < 4) {
            // better: use sth like Either
            throw new WallException();
        }
        try {
            CAD2DObject raw = cad2DObjectFactory.createWithLocalCentroid(points);
            return new Wall(raw);
        } catch (Exception e) {
            logger.info(e.getMessage());
            return new Wall();
        }
    }

    public Wall createDefaultWall() {
        return new Wall(createDefaultWallBoundaries());
    }

    private CAD2DObject createDefaultWallBoundaries() {
        // Room with 10x10 m^2
        Point ol = new Point(0, 1000);
        Point or = new Point(1000, 1000);
        Point ul = new Point(0, 0);
        Point ur = new Point(1000, 0);

        Point[] p =  new Point[]{ul, ur, or, ol, ul};
        return cad2DObjectFactory.createWithLocalCentroid(List.of(p));
    }
}
