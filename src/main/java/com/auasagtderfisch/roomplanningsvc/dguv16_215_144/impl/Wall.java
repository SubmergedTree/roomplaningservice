/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;


public class Wall {
    private final CAD2DObject wallCAD;
    private final boolean isMalformed;

    public Wall(CAD2DObject wallCAD) {
        this.wallCAD = wallCAD;
        isMalformed = false;
    }

    public Wall() {
        this.wallCAD = null;
        isMalformed = true;
    }

    public boolean isFurnitureInside(AbstractFurniture abstractFurniture) {
        if (isMalformed || wallCAD == null)
            return false;
        return wallCAD.isInside(abstractFurniture.getCAD2dObject());
    }

    public boolean isAreaInside(Area area) {
        if (isMalformed || wallCAD == null)
            return false;
        return wallCAD.isInside(area.getAreaObject());
    }

    public double getFloorSize() {
        if (isMalformed || wallCAD == null)
            return 0.0;
        return wallCAD.getSize();
    }

    public Point[] getVertices() {
        if (isMalformed || wallCAD == null)
            return new Point[0];
        return wallCAD.getVertices();
    }

    public boolean isMalformed() {
        return isMalformed;
    }
}
