/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.CabinetShelf;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Chair;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.ConferenceDesk;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Desk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FurnitureFactory {

    private CAD2DObjectFactory cad2DObjectFactory;
    private PointListFactory pointListFactory;

    @Autowired
    public FurnitureFactory(CAD2DObjectFactory dimensionFactory, PointListFactory pointsFactory) {
        this.cad2DObjectFactory = dimensionFactory;
        this.pointListFactory = pointsFactory;
    }

    public Desk createDesk(List<Point> points, Point globalPosition, double rotation) {
        return new Desk(points, globalPosition, rotation, cad2DObjectFactory, pointListFactory);
    }

    public CabinetShelf createCabinetShelf(boolean isSeldomInUse, boolean isOpen,  Point points, Point globalPosition, double rotation, double longestDrawer) {
        return new CabinetShelf(isSeldomInUse, isOpen, points, globalPosition, rotation, cad2DObjectFactory, pointListFactory, longestDrawer);
    }


    public Chair createChair(Point dimension, Point globalPosition, double rotation) {
        return new Chair(dimension, globalPosition, rotation, cad2DObjectFactory, pointListFactory);
    }

    public ConferenceDesk createConferenceDesk(boolean backsideIsEmpty, Point dimension, Point globalPosition, double rotation) {
        return new ConferenceDesk(backsideIsEmpty, dimension, globalPosition, rotation, cad2DObjectFactory, pointListFactory);
    }

    public Desk createDesk(Point dimension, Point globalPosition, double rotation) {
        List<Point> points = pointListFactory.createRectangleFromDimension(dimension);
        return this.createDesk(points, globalPosition, rotation);
    }

}
