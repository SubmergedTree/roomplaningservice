/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.*;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.LinkedList;
import java.util.List;

import static com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType.MovementArea;

public class ConferenceDesk extends AbstractFurniture {

    //private boolean footprintSize;
    private boolean backsideIsEmpty;
    private Point[] unrotatedVertices;

    public ConferenceDesk(boolean backsideIsEmpty, Point dimension, Point globalPosition, double rotation, CAD2DObjectFactory dimensionFactory, PointListFactory pointsFactory) {
        this.dimensionFactory = dimensionFactory;
        this.pointsFactory = pointsFactory;
        this.cad2dObject = dimensionFactory.create(pointsFactory.createRectangleFromDimension(dimension), globalPosition, 0.0);
        unrotatedVertices = cad2dObject.getVertices();
        cad2dObject.rotate(rotation);
        this.backsideIsEmpty = backsideIsEmpty;
        initializeAreas();
        updateAreas();
    }

    private void initializeAreas() {
        this.areas = new LinkedList<>();
        Area area1 = new Area();
        area1.setAreaType(AreaType.UserArea);
       /* Point[] vertices = this.cad2dObject.getVertices();
        JtsCAD2dObject areaDimension = dimensionFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(vertices[2], vertices[3], new Point(area1.getLowestDepth(), area1.getLowestWidth())));
        areaDimension.rotateAround(cad2dObject.getRotation(), cad2dObject.getPosition());
        area1.setAreaObjectWithRotation(areaDimension);*/
        this.areas.add(area1);

        for (Area a : this.areas) {
            switch (a.getAreaType()) {
                case UserArea:
                    a.setLowestDepthRotation(this.cad2dObject,80.0, unrotatedVertices[0], unrotatedVertices[1]);
                    break;
                case MovementArea:
                    break;
                case Footprint:
                    break;
                case FunctionalArea:
                    break;
            }

        }
    }

   /* public boolean isFootprintSize() {
        return footprintSize;
    }

    public void setFootprintSize(boolean footprintSize) {
        this.footprintSize = footprintSize;
    }
*/

    public boolean isBacksideIsEmpty() {
        return backsideIsEmpty;
    }

    public void setBacksideIsEmpty(boolean backsideIsEmpty) {
        this.backsideIsEmpty = backsideIsEmpty;
        updateAreas();
    }

    private void updateAreas() {
        if (backsideIsEmpty) {
            List<Point> points = this.pointsFactory.createRectangle(new Point(0, 60), new Point(60, 60), new Point(0, 0), new Point(60, 0) );
            JtsCAD2dObject dimension = this.dimensionFactory.create(points);
            this.setArea(MovementArea, dimension);
        }
    }

    private void setArea(AreaType areaType, JtsCAD2dObject dimension) {
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setAreaObjectWithRotation(dimension);
            }
        }
    }

    private void setArea(AreaType areaType, double lowestDepth) {
        Point[] vertices = this.cad2dObject.getVertices();
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setLowestDepthRotation(cad2dObject, lowestDepth, unrotatedVertices[0], unrotatedVertices[1]);
            }
        }
    }
}
