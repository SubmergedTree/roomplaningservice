/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import java.util.UUID;

public class RuleEvalError {
    private final UUID furnitureID1;
    private final String furniture1AffectedArea;
    private final UUID furnitureID2;
    private final String furniture2AffectedArea;
    private final String msg;
    private final ErrorType errorType;

    public RuleEvalError(UUID furnitureID1, String furniture1AffectedArea,
                         UUID furnitureID2, String furniture2AffectedArea,
                         String msg, ErrorType errorType) {
        this.furnitureID1 = furnitureID1;
        this.furniture1AffectedArea = furniture1AffectedArea;
        this.furnitureID2 = furnitureID2;
        this.furniture2AffectedArea = furniture2AffectedArea;
        this.msg = msg;
        this.errorType = errorType;
    }

    public RuleEvalError(UUID furnitureID1, UUID furnitureID2, String msg, ErrorType errorType) {
        this.furnitureID1 = furnitureID1;
        this.furniture1AffectedArea = null;
        this.furnitureID2 = furnitureID2;
        this.furniture2AffectedArea = null;
        this.msg = msg;
        this.errorType = errorType;
    }

    public RuleEvalError(String msg, ErrorType errorType) {
        this.furnitureID1 = null;
        this.furniture1AffectedArea = null;
        this.furnitureID2 = null;
        this.furniture2AffectedArea = null;
        this.msg = msg;
        this.errorType = errorType;
    }

    public UUID getFurnitureID1() {
        return furnitureID1;
    }

    public UUID getFurnitureID2() {
        return furnitureID2;
    }

    public String getFurniture1AffectedArea() {
        return furniture1AffectedArea;
    }

    public String getFurniture2AffectedArea() {
        return furniture2AffectedArea;
    }

    public String getMsg() {
        return msg;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}
