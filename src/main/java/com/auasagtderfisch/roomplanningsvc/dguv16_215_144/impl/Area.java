/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.*;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObjectFactoryImpl;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.PointListFactoryImpl;

import java.util.Arrays;
import java.util.List;

// TODO can overlap with Area/Furniture arraylist...
public class Area {
    private AreaType areaType;
    private double lowestDepth;
    private double lowestHeight;
    private double lowestWidth;
    private CAD2DObject areaObject;
    private CAD2DObjectFactory cad2dFactory;
    private PointListFactory pointsFactory;

    public Area() {
        this.cad2dFactory = new JtsCAD2dObjectFactoryImpl();
        this.pointsFactory = new PointListFactoryImpl();
        List<Point> points = pointsFactory.createRectangle(new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0) );
        Point globalPosition = new Point(0,0);
        this.areaObject = cad2dFactory.create(points, globalPosition, 0.0);
        areaObject.rotate(0);
    }

    public CAD2DObject getAreaObject() {
        return areaObject;
    }

    public void setAreaObject(CAD2DObject areaObject) {
        this.areaObject = cad2dFactory.createWithLocalCentroid(Arrays.asList(areaObject.getVertices()));
       // this.areaObject = cad2dFactory.create(Arrays.asList(areaObject.getVertices()), areaObject.getPosition(), areaObject.getRotation());
      //  this.areaObject = cad2dFactory.create(pointsFactory.createRectangleFromDimension(areaObject.getDimension()));
    }

    public void setAreaObjectWithRotation(CAD2DObject areaObject) {
        this.areaObject = cad2dFactory.create(Arrays.asList(areaObject.getVertices()), areaObject.getPosition(), areaObject.getRotation());
        //this.areaObject = cad2dFactory.create(pointsFactory.createRectangleFromDimension(areaObject.getDimension()));
       // this.areaObject = cad2dFactory.createWithLocalCentroid(Arrays.asList(areaObject.getVertices()));
        //setAreaObject(areaObject);
    }

    public void setPosition(Point position) {
        this.areaObject.setPosition(position);
    }

    public Point getPosition() {
        return this.areaObject.getPosition();
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public double getLowestDepth() {
        return lowestDepth;
    }

    public void setLowestDepth(double lowestDepth, Point ul, Point ur, double rotation) {
        this.lowestDepth = lowestDepth;
        Point new_dimension = new Point(0,this.lowestDepth);
        CAD2DObject areaDimension = cad2dFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(ul, ur, new_dimension));
        areaDimension.rotateAround(rotation, areaDimension.getPosition());
        setAreaObjectWithRotation(areaDimension);
    }

    public void setLowestDepthRotation(JtsCAD2dObject cad2dObject, double lowestDepth, Point ul, Point ur) {
        this.lowestDepth = lowestDepth;
   /*     JtsCAD2dObject localObject = cad2dFactory.create(Arrays.asList(cad2dObject.getVertices()), cad2dObject.getPosition(), cad2dObject.getRotation());
        double rotation_1 = 0.0;
        rotation_1-= cad2dObject.getRotation();
        localObject.rotateAround(rotation_1, cad2dObject.getPosition());
        Point[] vertices = localObject.getVertices();
*/
        Point new_dimension = new Point(0,this.lowestDepth);
        CAD2DObject areaDimension = cad2dFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(ul, ur, new_dimension));
        //areaDimension.rotate(cad2dObject.getRotation());
        areaDimension.rotateAround(cad2dObject.getRotation(), cad2dObject.getPosition());
        setAreaObject(areaDimension);
    }

    public double getLowestHeight() {
        return lowestHeight;
    }

    public void setLowestHeight(double lowestHeight) {
        this.lowestHeight = lowestHeight;
    }

    public double getLowestWidth() {
        return lowestWidth;
    }

    public void setLowestWidth(JtsCAD2dObject cad2dObject, double lowestWidth, Point ul, Point ur) {
        this.lowestWidth = lowestWidth;
        Point new_dimension = new Point(this.lowestWidth,this.lowestDepth);
        CAD2DObject areaDimension = cad2dFactory.createWithLocalCentroid(pointsFactory.createRectangleWithDepthPoints(ul, ur, new_dimension));
        areaDimension.rotateAround(cad2dObject.getRotation(), cad2dObject.getPosition());
        setAreaObject(areaDimension);
    }

    public double getAngle() {
        return areaObject.getRotation();
    }

    public void setAngle(double angle, Point position) {
        this.areaObject.rotateAround(angle, position);
    }
}
