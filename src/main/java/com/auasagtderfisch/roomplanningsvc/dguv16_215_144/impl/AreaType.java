/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

public enum AreaType {
    UserArea, //Benutzerfläche
    Footprint, //Stellfläche
    MovementArea, //Bewegungsfläche
    FunctionalArea
}
