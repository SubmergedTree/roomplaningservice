/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

public enum ErrorType {
    OUTSIDE_OFFICE,
    MALFORMED_WALL,
    OFFICE_TO_SMALL,
    AREAS_COLLIDING
}
