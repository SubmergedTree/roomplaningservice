/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.List;
import java.util.UUID;

public abstract class AbstractFurniture {
    protected String name;
    protected UUID furnitureUUID;
    protected JtsCAD2dObject cad2dObject;
    protected List<Area> areas;
    protected CAD2DObjectFactory dimensionFactory;
    protected PointListFactory pointsFactory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return furnitureUUID;
    }

    public void setId(UUID id) {
        this.furnitureUUID = id;
    }

    public JtsCAD2dObject getCAD2dObject() {
        return cad2dObject;
    }

    public void setCad2dObject(JtsCAD2dObject cad2dObject) {
        this.cad2dObject = cad2dObject;
    }

    public double getRotation() {
        return cad2dObject.getRotation();
    }

    public void rotate(double angle) {
        this.cad2dObject.rotate(angle);
    }

    public Point getPosition() {
        return cad2dObject.getPosition();
    }

    public void setPosition(Point position) {
        this.cad2dObject.setPosition(position);
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }
}
