/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.LinkedList;

import static com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType.FunctionalArea;
import static com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType.UserArea;


public class CabinetShelf extends AbstractFurniture {
    private double longestDrawer = 0.0;
    private boolean isSeldomInUse;
    private boolean isOpen;
    private Point[] unrotatedVertices;

    public CabinetShelf(boolean isSeldomInUse, boolean isOpen, Point dimension, Point globalPosition, double rotation, CAD2DObjectFactory dimensionFactory, PointListFactory pointsFactory, double longestDrawer) {
        this.isSeldomInUse = isSeldomInUse;
        this.longestDrawer = longestDrawer;
        this.isOpen = isOpen;
        this.dimensionFactory = dimensionFactory;
        this.pointsFactory = pointsFactory;
        this.cad2dObject = dimensionFactory.create(pointsFactory.createRectangleFromDimension(dimension), globalPosition, 0.0);
        unrotatedVertices = cad2dObject.getVertices();
        cad2dObject.rotate(rotation);
        initializeAreas();
        updateAreas();
    }

    private void initializeAreas() {
        this.areas = new LinkedList<>();
        Area area1 = new Area();
        area1.setAreaType(AreaType.UserArea);

        Area area2 = new Area();
        area2.setAreaType(AreaType.FunctionalArea);
       /* Point[] vertices = this.cad2dObject.getVertices();
        JtsCAD2dObject areaDimension = dimensionFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(vertices[2], vertices[3], new Point(area1.getLowestDepth(), area1.getLowestWidth())));

        area1.setAreaObjectWithRotation(areaDimension);
        area2.setAreaObjectWithRotation(areaDimension);
*/
        this.areas.add(area1);
        this.areas.add(area2);
        for (Area a : this.areas) {
            switch (a.getAreaType()) {
                case UserArea:
                    a.setLowestDepthRotation(cad2dObject, 80.0, unrotatedVertices[0], unrotatedVertices[1]);
                    break;
                case MovementArea:
                    break;
                case Footprint:
                    break;
                case FunctionalArea:
                    a.setLowestDepthRotation(cad2dObject, longestDrawer, unrotatedVertices[0], unrotatedVertices[1]);
                    break;
            }
        }
    }

    public double getLongestDrawer() {
        return longestDrawer;
    }

    public void setLongestDrawer(double size) {
        this.longestDrawer = size;
    }

    public boolean getIsSeldomInUse() {
        return this.isSeldomInUse;
    }

    public void setIsSeldomInUse(boolean isSeldomInUse) {
        this.isSeldomInUse = isSeldomInUse;
        updateAreas();
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
        updateAreas();
    }
    // TODO ist doch nichts dynamisches, kann nicht vom benutzer verändert werden ?
    // Interfaces sind nur dynamisch, rest über konstruktor/factory ?
    private void updateAreas() {
        // funktionsfläche = auszugstiefe +
        if(isOpen) {
            setArea(FunctionalArea, 50.0+this.longestDrawer);
            setArea(UserArea, 50.0+this.longestDrawer);
        }
    }

    private void setArea(AreaType areaType, JtsCAD2dObject dimension) {
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setAreaObjectWithRotation(dimension);
            }
        }
    }

    private void setArea(AreaType areaType, double lowestDepth) {
        Point[] vertices = this.cad2dObject.getVertices();
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setLowestDepthRotation(cad2dObject,lowestDepth, unrotatedVertices[0], unrotatedVertices[1]);
            }
        }
    }

}
