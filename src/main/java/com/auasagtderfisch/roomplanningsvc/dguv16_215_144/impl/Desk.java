/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.impl.JtsCAD2dObject;

import java.util.LinkedList;
import java.util.List;

import static com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.AreaType.*;

public class Desk extends AbstractFurniture {
    private boolean wheelchairCompatible;
    private boolean wheelchairAccessible; // unterfahrbar
    private boolean standingNotUpright;
    private double rotation;
    private Point[] unrotatedVertices;


    public Desk (List<Point> points, Point globalPosition, double rotation, CAD2DObjectFactory dimensionFactory, PointListFactory pointsFactory){
        this.dimensionFactory = dimensionFactory;
        this.pointsFactory = pointsFactory;
        this.cad2dObject = dimensionFactory.create(points, globalPosition, 0.0);
        unrotatedVertices = cad2dObject.getVertices();
        cad2dObject.rotate(rotation);
        initializeAreas();
    }

    private void initializeAreas() {
        this.areas = new LinkedList<>();
        Area area1 = new Area();
        area1.setAreaType(AreaType.UserArea);

        Area area2 = new Area();
        area2.setAreaType(MovementArea);
     /*   Point[] vertices = this.cad2dObject.getVertices();
        JtsCAD2dObject areaDimension = dimensionFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(vertices[0], vertices[1], new Point(area1.getLowestDepth(), area1.getLowestWidth())));
        areaDimension.rotateAround(rotation, cad2dObject.getPosition());
        area1.setAreaObjectWithRotation(areaDimension);
       // area1.setAngle(cad2dObject.getRotation(), cad2dObject.getPosition());
        area2.setAreaObjectWithRotation(areaDimension);
       // area2.setAngle(cad2dObject.getRotation(), cad2dObject.getPosition());
     */
        this.areas.add(area1);
        this.areas.add(area2);
        for (Area a : this.areas) {
            switch(a.getAreaType()) {
                case UserArea:
                   // a.setLowestDepth(1200.0f);
                    a.setLowestDepthRotation(this.cad2dObject, 100.0, unrotatedVertices[0], unrotatedVertices[1]);
                    break;
                case MovementArea:
                 /*   List<Point> new_points = this.pointsFactory.createRectangle(new Point(0, 150), new Point(150, 150), new Point(0, 0), new Point(150, 0) );
                    JtsCAD2dObject cad2dObject = this.dimensionFactory.create(new_points);*/
                //    JtsCAD2dObject new_area_object = dimensionFactory.createWithLocalCentroid(pointsFactory.createRectangleWithInitialPoints(unrotatedVertices[0], unrotatedVertices[1], new Point(150, 150)));
                //    a.setAreaObject(new_area_object);
                    a.setLowestDepthRotation(this.cad2dObject, 150.0, unrotatedVertices[0], unrotatedVertices[1]);
                    a.setLowestWidth(this.cad2dObject,150.0, unrotatedVertices[0], unrotatedVertices[1]);
                  //  a.setLowestDepth(1000.0f);
                   // a.setLowestWidth(this.cad2dObject,100.0f, unrotatedVertices[0], unrotatedVertices[1]);
                    break;
                case Footprint:
                    break;
                case FunctionalArea:
                    break;
            }

        }
    }
    public boolean isWheelchairCompatible() {
        return wheelchairCompatible;
    }

    public void setWheelchairCompatible(boolean wheelchairCompatible) {

        this.wheelchairCompatible = wheelchairCompatible;
        updateAreas();
    }

    public boolean isWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(boolean wheelchairAccessible) {
        if (wheelchairAccessible) {
            this.wheelchairCompatible = true;
            this.wheelchairAccessible = true;
        } else {
            this.wheelchairAccessible = false;
        }
        updateAreas();
    }

    public boolean isStandingNotUpright() {
        return standingNotUpright;
    }

    public void setStandingNotUpright(boolean standingNotUpright) {
        this.standingNotUpright = standingNotUpright;
        updateAreas();
    }


    private void updateAreas() {
        if (this.wheelchairCompatible && !this.wheelchairAccessible) {
            List<Point> new_points = this.pointsFactory.createRectangle(new Point(0, 150), new Point(150, 150), new Point(0, 0), new Point(150, 0) );
            JtsCAD2dObject cad2dObject = this.dimensionFactory.createWithLocalCentroid(new_points);
            this.setArea(MovementArea, cad2dObject);
        } else if(this.wheelchairCompatible && this.wheelchairAccessible) {
            this.setArea(MovementArea, 120.0f);
        }
        if (this.isStandingNotUpright()) {
            this.setArea(MovementArea, 120.0f);
        }
    }

    private void setArea(AreaType areaType, JtsCAD2dObject cad2dObject) {
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setAreaObjectWithRotation(cad2dObject);
            }
        }
    }

    private void setArea(AreaType areaType, double lowestDepth) {
        Point[] vertices = this.cad2dObject.getVertices();
        for (Area a : this.areas) {
            if (a.getAreaType() == areaType) {
                a.setLowestDepthRotation(cad2dObject, lowestDepth, unrotatedVertices[0], unrotatedVertices[1]);
            }
        }
    }
}
