/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.loader;

import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Room;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.Wall;
import com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl.factory.WallFactory;
import com.auasagtderfisch.roomplanningsvc.persistence.LoaderService;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import io.vertx.core.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Service
public class DGUV16InMemoryLoaderService implements LoaderService {

    private final Map<String, Room> savedData = new LinkedHashMap<>();
    private final WallFactory wallFactory;
    private final LocalizationService localizationService;

    @Autowired
    public DGUV16InMemoryLoaderService(WallFactory wallFactory, LocalizationService localizationService) {
        this.wallFactory = wallFactory;
        this.localizationService = localizationService;
    }

    @Override
    public Future<Object> loadNew(String name) {
        Wall wall = this.wallFactory.createDefaultWall();
        Room room = new Room(3, wall, localizationService);
        savedData.put(name, room);
        return Future.succeededFuture(room);
    }

    @Override
    public Future<Object> load(String name) {
        Room room = savedData.get(name);
        if(room == null) {
            return Future.failedFuture("Not found");
        }
        return Future.succeededFuture(room);
    }

    @Override
    public Future<Void> safe(String name) {
        return Future.succeededFuture();
    }

    // Delete
    @Override
    public Future<Void> unload(String name) {
        return Future.succeededFuture();
    }

    @Override
    public Future<Set<String>> getStoredProjectNames() {
        return Future.succeededFuture(savedData.keySet());
    }

    @Override
    public String forWhat() {
        return "DGUV16";
    }
}
