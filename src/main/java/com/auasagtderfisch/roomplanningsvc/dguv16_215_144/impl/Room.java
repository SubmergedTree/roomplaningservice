/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.blackboard.AbstractData;
import com.auasagtderfisch.roomplanningsvc.util.localization.LocalizationService;
import org.apache.commons.math3.util.Pair;

import java.util.*;

public class Room implements AbstractData {
    private final List<AbstractFurniture> furnitures;
   // private final Map<String, List<String>> groupIdFurnitureIdMap;
    private final Map<String, Group> groupIdFurnitureIdMap;
    private Wall wall;
    private int amountPersons;
    private final LocalizationService localizationService;

    public Room(int defaultAmountPerson, Wall defaultWall, LocalizationService localizationService) {
        this.amountPersons = defaultAmountPerson;
        this.furnitures = new ArrayList<>();
        this.wall = defaultWall;
        this.groupIdFurnitureIdMap = new HashMap<>();
        this.localizationService = localizationService;
    }

    public int getAmountPersons() {
        return amountPersons;
    }

    public void setAmountPersons(int amountPersons) {
        this.amountPersons = amountPersons;
    }

    public void addFurniture(AbstractFurniture abstractFurniture) {
        furnitures.add(abstractFurniture);
    }

    public void changeWall(Wall wall) {
        this.wall = wall;
    }

    public String createGroup(String groupName) {
        UUID groupId = UUID.randomUUID();
        this.groupIdFurnitureIdMap.put(groupId.toString(),
                new Group(groupName, localizationService.getForKey("unknownFurniture")));
        return groupId.toString();
    }

    public boolean deleteGroup(String groupId) {
        if (groupIdFurnitureIdMap.containsKey(groupId)) {
            groupIdFurnitureIdMap.remove(groupId);
            return true;
        }
        return false;
    }

    public Throwable addToGroup(String groupId, String furnitureId) {
        if (groupIdFurnitureIdMap.containsKey(groupId)) {
            if(getFurniture(UUID.fromString(furnitureId)).isPresent()) {
                groupIdFurnitureIdMap.get(groupId).addFurnitureId(furnitureId);
                return null;
            }
            return new FurnitureNotFoundException(localizationService.getForKey("unknownFurniture"));
        }
        return new GroupNotFoundException(localizationService.getForKey("unknownGroup"));
    }

    public Throwable deleteFromGroup(String groupId, String furnitureId) {
        if (groupIdFurnitureIdMap.containsKey(groupId)) {
            var group = groupIdFurnitureIdMap.get(groupId);
            return group.deleteFurnitureId(furnitureId);
        }
        return new GroupNotFoundException(localizationService.getForKey("unknownGroup"));
    }

    public Map<String, Group> getGroupIdFurnitureIdMap() {
        return groupIdFurnitureIdMap;
    }

    public Optional<AbstractFurniture> getFurniture(UUID furnitureId) {
        return furnitures.stream()
                .filter(f -> f.getId().equals(furnitureId))
                .findFirst();
    }

    public <T extends AbstractFurniture> Optional<T> getFurnitureByType(UUID furnitureId, Class<T> clazz) {
        List<T> byType = getAllByType(clazz);
        return byType.stream()
                .filter(f -> f.getId().equals(furnitureId))
                .findFirst();
    }

    // TODO also delete from group
    public boolean deleteFurniture(UUID furnitureId) {
        deleteFromAllGroups(furnitureId.toString());
        return furnitures.removeIf( f -> f.getId().equals(furnitureId));
    }

    private void deleteFromAllGroups(String furnitureId) {
        for (var group : groupIdFurnitureIdMap.entrySet()) {
            group.getValue().deleteFurnitureId(furnitureId);
        }
    }

    public List<RuleEvalError> checkErrors() {
        var errors = this.checkIfFurnituresAreInsideOffice();
        if (wall.isMalformed())
            errors.add(new RuleEvalError(localizationService.getForKey("malformedWall"), ErrorType.MALFORMED_WALL));
        errors.addAll(this.areAreasColliding());
        var toSmall = this.isOfficeLargeEnough();
        if (toSmall != null)
            errors.add(toSmall);
        return errors;
    }

    public List<AbstractFurniture> getFurnitures() {
        return furnitures;
    }

    @SuppressWarnings("unchecked")
    public <T extends AbstractFurniture> List<T> getAllByType(Class<T> clazz) {
        List<T> byType = new ArrayList<>();
        for (var f : furnitures) {
            if (clazz.isInstance(f)) {
                byType.add((T)f);
            }
        }
        return byType;
    }

    private List<RuleEvalError> checkIfFurnituresAreInsideOffice() {
        var errorMessages = new ArrayList<RuleEvalError>();
        for (var furniture : furnitures) {
            if (!isMainSpaceInsideOffice(furniture) || !areAreasSpaceInsideOffice(furniture)) {
                var msg = localizationService.getForKey("furnitureOutOfOffice", furniture.getName());
                errorMessages.add(new RuleEvalError(furniture.furnitureUUID, null, msg, ErrorType.OUTSIDE_OFFICE));
            }
        }
        return errorMessages;
    }

    private RuleEvalError isOfficeLargeEnough() {
        if (wall.getFloorSize() >= calcMinOfficeSize(amountPersons))
            return null;
        var msg = localizationService.getForKey("officeToSmall", amountPersons);
        return new RuleEvalError(msg, ErrorType.OFFICE_TO_SMALL);
    }

    private List<RuleEvalError> areAreasColliding() {
        var duplicatedList = new ArrayList<Pair<UUID, UUID>>();
        var errors = new ArrayList<RuleEvalError>();
        for (var f1 : furnitures)
            for (var f2 : furnitures) {
                if (f1.getId().equals(f2.getId()))
                    continue;
                if (inDuplicatedList(duplicatedList, f1.furnitureUUID, f2.furnitureUUID))
                    continue;
                duplicatedList.add(new Pair<>(f1.furnitureUUID, f2.furnitureUUID));
                areAreasColliding(f1, f2, errors);
            }
        return errors;
    }

    private boolean inDuplicatedList(List<Pair<UUID, UUID>> dupList, UUID f1, UUID f2) {
        return dupList.stream()
                .anyMatch(dup -> dup.getFirst().equals(f1) && dup.getSecond().equals(f2));
    }

    public Wall getWall() {
        return wall;
    }

    private void areAreasColliding(AbstractFurniture f1, AbstractFurniture f2, List<RuleEvalError> errors) {
        intersects(f1, f2, errors);
        for (var area1 : f1.getAreas())
            for (var area2 : f2.getAreas()) {
                intersects(area1, area2, f1, f2,errors);
            }
    }

    private void intersects(AbstractFurniture f1, AbstractFurniture f2, List<RuleEvalError> errors) {
        if (f1.getCAD2dObject().intersectWith(f2.getCAD2dObject())) {
            var msg = localizationService.getForKey("mainSpaceColliding", f1.getName(), f2.getName());
            errors.add(new RuleEvalError(f1.furnitureUUID, "Main Space",f2.furnitureUUID,
                    "Main Space",msg, ErrorType.AREAS_COLLIDING));
        }
    }

    private void intersects(Area area1, Area area2, AbstractFurniture furniture1Name, AbstractFurniture furniture2Name, List<RuleEvalError> errors) {
        var area1Name = localizationService.getForKey(area1.getAreaType().toString());
        var area2Name = localizationService.getForKey(area2.getAreaType().toString());
        if (area1.getAreaObject().intersectWith(area2.getAreaObject())) {
            var msg = localizationService.getForKey("areaColliding",
                    area1Name, furniture1Name.getName(), area2Name, furniture2Name.getName());
            errors.add(new RuleEvalError(furniture1Name.furnitureUUID, area1Name,
                    furniture2Name.furnitureUUID, area2Name, msg, ErrorType.AREAS_COLLIDING));
        }
    }

    private boolean isMainSpaceInsideOffice(AbstractFurniture furniture) {
        return wall.isFurnitureInside(furniture);
    }

    private boolean areAreasSpaceInsideOffice(AbstractFurniture furniture) {
        for (var area : furniture.getAreas()) {
            if (!wall.isAreaInside(area)) return false;
        }
        return true;
    }

    /**
     *
     * @param amountPersons max workplaces
     * @return min space in cm^2
     */
    private double calcMinOfficeSize(int amountPersons) {
        double size = 8;
        if (amountPersons > 1)
            size += (amountPersons - 1) * 6;
        return size / 0.00010000;
    }
}
