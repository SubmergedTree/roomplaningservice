/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import com.auasagtderfisch.roomplanningsvc.cad2d.CAD2DObjectFactory;
import com.auasagtderfisch.roomplanningsvc.cad2d.Point;
import com.auasagtderfisch.roomplanningsvc.cad2d.PointListFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Chair extends AbstractFurniture{

    public Chair(Point dimension, Point globalPosition, double rotation, CAD2DObjectFactory dimensionFactory, PointListFactory pointsFactory) {
        this.dimensionFactory = dimensionFactory;
        this.pointsFactory = pointsFactory;
        this.cad2dObject = dimensionFactory.create(pointsFactory.createRectangleFromDimension(dimension), globalPosition, rotation);
        this.areas = new ArrayList<>();
    }
}
