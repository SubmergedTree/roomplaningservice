/*
 * Copyright (c) Johannes Hummel, Jannik Seemann 2020 - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.auasagtderfisch.roomplanningsvc.dguv16_215_144.impl;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private final List<String> furnitureIds;
    private final String name;
    private final String unkownFurniture;

    public Group(String name, String unkownFurniture) {
        furnitureIds = new ArrayList<>();
        this.name = name;
        this.unkownFurniture = unkownFurniture;
    }

    public void addFurnitureId(String furnitureId) {
        if (furnitureIds.contains(furnitureId)) return;
        furnitureIds.add(furnitureId);
    }

    public Throwable deleteFurnitureId(String furnitureId) {
        if (furnitureIds.removeIf(u ->u.equals(furnitureId))) {
            return null;
        }
        return new FurnitureNotFoundException(unkownFurniture);
    }

    public List<String> getFurnitureIds() {
        return furnitureIds;
    }

    public String getName() {
        return name;
    }
}
